FROM ubuntu:16.04
MAINTAINER Marian Neagul <marian@info.uvt.ro>
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update --fix-missing && \
	apt-get -y install software-properties-common && \
	/usr/bin/add-apt-repository ppa:ubuntugis/ppa && \
	apt-get -y update
RUN apt-get -y install python-virtualenv
RUN apt-get -y install python-pip
RUN mkdir /opt/ewps
RUN pip install --upgrade pip
RUN pip install --upgrade virtualenv
#RUN sed -i 's/\x0//g' /usr/lib/python3/dist-packages/pkg_resources/__init__.py
RUN virtualenv --system-site-packages /opt/ewps-venv/
COPY requirements.txt /tmp/requirements.txt
RUN /opt/ewps-venv/bin/pip install -r /tmp/requirements.txt
RUN apt-get -y install htcondor htcondor-dev
RUN /opt/ewps-venv/bin/pip install htcondor Pillow
RUN /opt/ewps-venv/bin/pip install scikit-image
COPY . /opt/ewps
RUN cd /opt/ewps && \
	/opt/ewps-venv/bin/python setup.py develop
#RUN /opt/ewps-venv/bin/ewps 
RUN mkdir -p /eodata /srv/eo4see
ENTRYPOINT /opt/ewps/entrypoint.sh
