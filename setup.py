__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """

import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="ewps",
    version="0.1.0",
    author="UVT EO4SEE Project Contributors",
    author_email="eo4see@info.uvt.ro",
    description="An WPS 2.0 Server implementation",
    license="APL",
    long_description=read('README.md'),
    package_dir={'': 'src'},
    packages=find_packages(where="./src/", exclude=['contrib', 'docs', '_tests']),
    extras_require={
        'MONGO': ["pymongo", ],
        'REDIS': ["redis", ]
    },
    install_requires=['Flask>=0.10.0', "lxml>=3.5.0", 'PyYaml>=3.8.0', 'enum34'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            "ewps = ewps.app:main",
            "ewps-wrapper = ewps.execution.docker_wrapper:main"
        ],
        'ewps.plugin.processing.handlers': [

        ],
        'ewps.plugin.chain.handlers': [

        ]
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: Apache Software License",
        "Intended Audience :: Developers",
        "Topic :: Scientific/Engineering :: GIS",
        "Intended Audience :: Science/Research",
        "Environment :: Web Environment"
    ]
)
