## Introduction

EWPS is an outcome of the ESA EO4SEE project, being a standalone **WPS 2.0 server implementation**. 

Main features:

* Based on native, extensible Python 2 and Python 3 implementation

* Two way process definition and description:
    * User-friendly WPS 2.0 concept mapping using an YAML specification
    * Native Python API (using decorators) process definition and description
* Multiple job execution environments:
    * Local: based on Python multiprocessing 
    * Remote: Cook Job Scheduler and Mesos

## How To

### Initial configuration

EWPS basic configuration is specified using the `--configfile` command line flag. An example is in in the `config.yaml` file under `src/ewps/_data/etc`, in the source distribution. The configuration for the execution environments is under the **execution_managers** field. The Cook Scheduler requires an additional ***docker_image*** paramater set, that is the path to the EWPS docker image. The corresponding Dockerfile for building the EWPS image is in the root of this project. The process definitions are organised in **registries**, where each registry should have ONLY one ***execution_manager*** name, correlated to the ones defined under the **execution_managers** field. Each registry should have a **type**, describing the method of process description:

* fs - YAML based processings
* discovered-processes-ng - decorator based processings

### Process definition and implementation

YAML based mapping enables a user-friendly description of the WPS concepts, supporting the WPS data types: LiteralData, ComplexData and BoundingBoxData. An example of such a description is `examples/processes/demo_ndvi.yaml`.

We provide a two-way decorator based mechanism for process definition:

* for WPS experienced users, that allows a more in depth configuration of WPS process parameters ( `src/ewps/processes/process_defition/decorator_example_process1.py`)
* for pure process developers, simplifying the WPS concepts, setting default values (`src/src/ewps/processes/process_defition/decorator_example_process2.py`)

Regarding the process definition method, the actual process implementation, must include *@redirectOutput()* decorator, to allow EWPS to collect error and output from the processing. If the processing creates additional files, it must also include *@createOutputFiles()* decorator. Then, EWPS create a specific folder for the ouput files, that can be obtained from inside the function as follows:
```python
   this = function_name
   output_directory = this.get_output_dir()

```

At the end of a processing, the results should be wrapped as a dictionary with mapped values of the result(output) ids and result values. Large results should be send by reference and their value should be the saved path to file.

## Start EWPS

Run `src/ewps/app.py` script. Make sure Cook scheduler is up and running and its parameters are correctly set in the `config.yaml` file before starting EWPS, if planning to use Cook as execution environment.

## WPS 2.0 Operations

### GetCapabilities

A request for GetCapabilities should be sent to: `http://localhost:5000/wps?service=WPS&version=2.0.0&request=GetCapabilities` (change localhost:5000 with your EWPS running URL). The response includes a description of the EWPS supported WPS operation. An example response is `examples/wps_request_response/GetCapabilitiesResponse.xml`.

### DescribeProcess

A request for DescribeProcess should be sent to: `http://localhost:5000/wps?service=WPS&version=2.0.0&request=DescribeProcess&identifier=ALL` (change localhost:5000 with your EWPS running URL). The response includes a description of the EWPS registered processes. An example response is `examples/wps_request_response/DescribeProcessALLResponse.xml`. EWPS also supports individual process description, by replacing the `identifier=ALL` with the process identifier. For example, a request to `http://localhost:5000/wps?service=WPS&version=2.0.0&request=DescribeProcess&identifier=computeNDVI` returnes a description of the process `computeNDVI` (`examples/wps_request_response/DescribeProcessComputeNDVIResponse.xml`).

### Execute

A request for Execute should be sent to: `http://localhost:5000/wps` (change localhost:5000 with your EWPS running URL). EWPS supports both **sync and async** process execution, as defined by WPS 2.0 standard. Also, EWPS supports inputs and outputs transmission by both **value and reference**. An example request for the ***computeNDVI*** process, in async mode can be found at `examples/wps_reques_response/ExecuteComputeNDVIRequest.xml` and its corresponding response (StatusInfo document for async requests) is `examples/wps_request_response/ExecuteComputeNDVIAsyncResponse.xml`. To execute a process in sync mode, change the **mode** attribute to **sync** and receive a Result Document (`examples/wps_request_response/ExecuteComputeNDVISyncResponse.xml`).

### GetStatus

A request for GetStatus should be sent to: `http://localhost:5000/wps?service=WPS&version=2.0.0&request=GetStatus&JobID=job-id` (change localhost:5000 with your EWPS running URL). The ***job-id*** parameter must be replaced with the actual job id returned as a reponse to the corresponding ***Execute*** request, under **JobID** element (`examples/wps_request_response/GetStatusResponse.xml`).

### GetResult

A request for GetResult should be sent to: `http://localhost:5000/wps?service=WPS&version=2.0.0&request=GetResult&JobID=job-id` (change localhost:5000 with your EWPS running URL). The ***job-id*** parameter must be replaced with the actual job id returned as a reponse to the corresponding ***Execute*** request, under **JobID** element (`examples/wps_request_response/GetResultResponse.xml`).