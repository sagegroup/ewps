__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import logging

EWPS_PROCESS_REGISTRY = []

from functools import wraps, partial
from enum import Enum
import sys

logger = logging.getLogger(__name__)


class EwpsTypes(Enum):
    string = 'literalData'
    int = 'literalData'
    boolean = 'literalData'
    double = 'literalData'
    float = 'literalData'


# ToDo: this class should move outside of the decorator classes
class LiteralDataType(object):
    def __init__(self):
        self._definition = {
            'formats': {
                "utf8_text_plain": {
                    "mimeType": "text/plain",
                    "encoding": "utf8"
                }
            },
            "default": 'utf8_text_plain',
            "dataDomains": [
                {
                    "possibleLiteralValues": "AnyValue",
                    "dataType": None,
                    "default": True
                }
            ]
        }

        self._definition["dataDomains"][0]["dataType"] = self.type_name

    def to_definition(self):
        return self._definition


class ComplexDataType(object):
    def __init__(self, formats={}, default=None, any=None):
        self._definition = {

            'formats': {

            },
            "default": None
        }
        if len(formats.keys()) < 1:
            # add default value
            self._definition['formats']['octet_stream'] = {
                'mimeType': 'application/octet-stream',
                'encoding': 'raw',
                'schema': 'http://quakeml.org/schema/xsd/QuakeML-1.2.xsd'
            }
            self._definition['default'] = 'octet_stream'

        else:
            self._definition['formats'] = formats
            if default:
                self._definition['default'] = default
            else:
                # set default as first entry
                self._definition['default'] = self._definition['formats'].keys()[0]
        if any:
            self._definition['any'] = any

    def to_definition(self):
        return self._definition


class BoundingBoxDataType(object):
    def __init__(self, crs):
        self._definition = {
            'formats': {
                "utf8_text_plain": {
                    "mimeType": "text/plain",
                    "encoding": "utf8"
                }
            },
            "default": 'utf8_text_plain',
            "supportedCRS": []

        }
        self._definition["supportedCRS"].append({})
        self._definition["supportedCRS"][0]["CRS"] = crs
        self._definition["supportedCRS"][0]["default"] = True

    def to_definition(self):
        return self._definition


class String(LiteralDataType): type_name = "String"


class Integer(LiteralDataType): type_name = "Integer"


class Boolean(LiteralDataType): type_name = "Boolean"


class Double(LiteralDataType): type_name = "Double"


class Float(LiteralDataType): type_name = "Float"


class BoundingBox(BoundingBoxDataType): type_name = "BoundingBox"


class ComplexData(ComplexDataType): type_name = "ComplexData"


_DECORATED_FUNCTION_ATTRIBUTE = "__ewps_processing"


def is_processing(obj):
    return hasattr(obj, _DECORATED_FUNCTION_ATTRIBUTE)


def get_processing_attributes(obj, default=None):
    return getattr(obj, _DECORATED_FUNCTION_ATTRIBUTE, default)


def get_process_offering(obj, default=None):
    offering_params = getattr(obj, _DECORATED_FUNCTION_ATTRIBUTE, default).get("processOferring", None)
    if offering_params is None:
        offering_params = {
            'jobControlOptions': "async-execute sync-execute",
            'outputTransmission': "value",
            'processModel': "native"

        }
    return offering_params


def prepare_function_options(f):
    if is_processing(f):
        options = get_processing_attributes(f)
    else:
        options = {}
        setattr(f, _DECORATED_FUNCTION_ATTRIBUTE, options)
    return options


# Utility decorator to attach a function as an attribute of obj
def attach_wrapper(obj, func=None):
    if func is None:
        return partial(attach_wrapper, obj)
    setattr(obj, func.__name__, func)
    return func


def processDescription(name, title, executionManager, metadata=None, abstract=None, keywords=None):
    def decorate(f):
        options = prepare_function_options(f)

        options.update({
            'name': name,
            'title': title,
            'metadata': metadata,
            'keywords': keywords,
            'abstract': abstract,
            'entrypoint': "%s:%s" % (f.__module__, f.__name__),
            'executionManager': executionManager
        })

        @wraps(f)
        def __wrapper_func(*args, **kwargs):
            return f(*args, **kwargs)

        @attach_wrapper(__wrapper_func)
        def set_output(output_file):
            options["outputFile"] = output_file

        @attach_wrapper(__wrapper_func)
        def set_error(error_file):
            options["errorFile"] = error_file

        return __wrapper_func

    return decorate


def processInput(identifier, title, abstract, minOccurs=1, maxOccurs=1, type=EwpsTypes.string, metadata=None,
                 keywords=None):
    def decorate(f):
        options = prepare_function_options(f)
        input = {
            'title': title,
            'abstract': abstract,
            'minOccurs': minOccurs,
            'maxOccurs': maxOccurs,
            'type': type,
            'metadata': metadata,
            'keywords': keywords
        }

        if isinstance(type, LiteralDataType):
            input["literalData"] = type.to_definition()
        elif isinstance(type, BoundingBoxDataType):
            input["boundingBoxData"] = type.to_definition()
        elif isinstance(type, ComplexDataType):
            input['complexData'] = type.to_definition()
        else:
            raise NotImplementedError("We support literal,complex and bounding box data types")

        if 'inputs' in options:
            inputs = options.get('inputs')
        else:
            inputs = {}
            options['inputs'] = inputs
        inputs[identifier] = input

        @wraps(f)
        def wrapped_f(*args, **kwargs):
            return f(*args, **kwargs)

        return wrapped_f

    return decorate


def processOutput(identifier, title, abstract, type=EwpsTypes.string):
    def decorate(f):
        options = prepare_function_options(f)
        output = {
            'title': title,
            'abstract': abstract,
            'type': type
        }

        if isinstance(type, LiteralDataType):
            output["literalData"] = type.to_definition()
        elif isinstance(type, BoundingBoxDataType):
            output["boundingBoxData"] = type.to_definition()
        elif isinstance(type, ComplexDataType):
            output['complexData'] = type.to_definition()
        else:
            raise NotImplementedError("We support literal,complex and bounding box data types")

        if 'outputs' in options:
            outputs = options.get('outputs')
        else:
            outputs = {}
            options['outputs'] = outputs
        outputs[identifier] = output

        @wraps(f)
        def wrapped_f(*args, **kwargs):
            return f(*args, **kwargs)

        return wrapped_f

    return decorate


def processOffering(jobControlOptions='async-execute sync-execute', outputTransmission='value', processModel='native'):
    def decorate(f):
        options = prepare_function_options(f)
        processOffering = {
            'jobControlOptions': jobControlOptions,
            'outputTransmission': outputTransmission,
            'processModel': processModel

        }
        if 'processOffering' in options:
            processOfferings = options.get('processOffering')
            processOfferings.update(processOffering)
        else:
            options['processOferring'] = processOffering

        @wraps(f)
        def wrapped_f(*args, **kwargs):
            return f(*args, **kwargs)

        return wrapped_f

    return decorate


def processing(**kwargs):
    def decorate(f):
        options = prepare_function_options(f)

        name = kwargs.get("name", f.__name__)
        title = kwargs.get("title", "%s processing" % name)
        metadata = kwargs.get("metadata", None)
        keywords = kwargs.get("keywords", None)
        abstract = kwargs.get("abstract", None)

        options.update({
            'name': name,
            'title': title,
            'metadata': metadata,
            'keywords': keywords,
            'abstract': abstract,
            'entrypoint': "%s:%s" % (f.__module__, f.__name__)
        })
        if 'inputs' not in options:
            options["inputs"] = {}
        if 'outputs' not in options:
            options["outputs"] = {}

        @wraps(f)
        def __wrapper_func(*args, **kwargs):
            return f(*args, **kwargs)

        @attach_wrapper(__wrapper_func)
        def set_output(output_file):
            options["outputFile"] = output_file

        @attach_wrapper(__wrapper_func)
        def set_error(error_file):
            options["errorFile"] = error_file

        return __wrapper_func

    return decorate


def output(**kwargs):
    def decorate(f):
        identifier = kwargs.get('id', kwargs.get('identifier'))
        title = kwargs.get('title', identifier)
        min_occurs = kwargs.get('min_occurs', 1)
        max_occurs = kwargs.get('max_occurs', 1)
        output_definition = {
            'id': identifier,
            'title': title,
            'abstract': 'Abstract of %s' % title,
        }

        output_type = kwargs.get('type', String())
        if isinstance(output_type, LiteralDataType):
            output_definition["literalData"] = output_type.to_definition()
        elif isinstance(output_type, BoundingBoxDataType):
            output_definition["boundingBoxData"] = output_type.to_definition()
        elif isinstance(output_type, ComplexDataType):
            output_definition['complexData'] = output_type.to_definition()
        else:
            raise NotImplementedError("We support literal,complex and bounding box data types")
        if is_processing(f):
            options = get_processing_attributes(f)
        else:
            options = {}
            setattr(f, _DECORATED_FUNCTION_ATTRIBUTE, options)
        if 'outputs' in options:
            outputs = options.get('outputs')
        else:
            outputs = {}
            options['outputs'] = outputs
        outputs[identifier] = output_definition

        @wraps(f)
        def __wrapper_func(*args, **kwargs):
            return f(*args, **kwargs)

        return __wrapper_func

    return decorate


def argument(**kwargs):
    def decorate(f):
        identifier = kwargs.get('id', kwargs.get('identifier'))
        title = kwargs.get('title', identifier)

        min_occurs = kwargs.get('min_occurs', 1)
        max_occurs = kwargs.get('max_occurs', 1)
        metadata = kwargs.get('metadata', None)
        keywords = kwargs.get('keywords', None)
        input = {
            'id': identifier,
            'title': title,
            'abstract': 'Abstract of %s' % title,
            'minOccurs': min_occurs,
            'maxOccurs': max_occurs,
            'metadata': metadata,
            'keywords': keywords

        }

        input_type = kwargs.get('type', String())
        if isinstance(input_type, LiteralDataType):
            input["literalData"] = input_type.to_definition()
        elif isinstance(input_type, BoundingBoxDataType):
            input["boundingBoxData"] = input_type.to_definition()
        elif isinstance(type, ComplexDataType):
            input['complexData'] = type.to_definition()
        else:
            raise NotImplementedError("Currently we support only literal and bounding box data types")

        if is_processing(f):
            options = get_processing_attributes(f)
        else:
            options = {}
            setattr(f, _DECORATED_FUNCTION_ATTRIBUTE, options)
        if 'inputs' in options:
            inputs = options.get('inputs')
        else:
            inputs = {}
            options['inputs'] = inputs
        inputs[identifier] = input

        @wraps(f)
        def __wrapper_func(*args, **kwargs):
            return f(*args, **kwargs)

        return __wrapper_func

    return decorate


# Decorator needed for LocalProcessExecution
def redirectOutput(outputFile=None):
    def decorate(func):
        d = {"outputFile": None, "errorFile": None}

        @wraps(func)
        def wrapper(*args, **kwargs):
            sys.stdout = open(d['outputFile'], "w")
            sys.stderr = open(d['errorFile'], "w")

            return func(*args, **kwargs)

        @attach_wrapper(wrapper)
        def set_output(newOutputFile):
            d['outputFile'] = newOutputFile

        @attach_wrapper(wrapper)
        def set_error(newErrorFile):
            d['errorFile'] = newErrorFile

        return wrapper

    return decorate


# Decorator to create specific process output files in job
def createOutputFiles(jobId=None, outputDir=None):
    def decorate(func):
        d = {"jobId": None, "outputDir": None}
        func._createOutputFiles = True

        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        @attach_wrapper(wrapper)
        def set_output_info(job_id, output_dir):
            d['jobId'] = job_id
            d['outputDir'] = output_dir

        @attach_wrapper(wrapper)
        def get_output_dir():
            return d['outputDir']

        @attach_wrapper(wrapper)
        def get_job_id():
            return d['jobId']

        return wrapper

    return decorate
