__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import importlib
from pkg_resources import resource_stream

from ewps.registry import LocalPythonRegistry

try:
    from urllib import quote as urlquote
except:  # Python 3
    from urllib.parse import quote as urlquote

import os


def initialize_ewps(config, config_file, logging_config=None, internal_chains_config=None):
    #### Initialize logging
    if logging_config is None:
        import yaml
        logging_config = yaml.load(resource_stream(__name__, "_data/etc/internal-logging-config.yaml"))
    config_base_dir = os.path.dirname(config_file)
    workdir = config.get("global", {}).get("workdir", ".")
    if not os.path.isabs(workdir):
        workdir = os.path.join(config_base_dir, workdir)
        config.get("global", {})["workdir"] = workdir
    os.chdir(os.path.abspath(workdir))
    internal_logging_config = logging_config
    logging_config = config.get("logging", internal_logging_config)
    from logging.config import dictConfig
    from logging import getLogger
    import yaml
    dictConfig(logging_config)
    log = getLogger(__name__)
    log.info("logging system initialized")

    #### Initialize chain subsystem
    from ewps.chains import ChainExecutor, HandlerChain, Task, lookup_task, populate_constructor_from_dict
    executor = ChainExecutor(global_config=config)
    log.info("loading internal chains")
    if internal_chains_config is None:
        internal_chains_config = yaml.load(resource_stream(__name__, "_data/etc/internal-chains.yaml"))
    populate_constructor_from_dict(executor, internal_chains_config)
    log.info('Loading config supplied chains')
    config_chains = config.get('chains', {})
    if config_chains:
        populate_constructor_from_dict(executor, config_chains)
    else:
        log.warning("No chain specification in configuration")

    # Loading execution managers
    execution_managers = {}
    execution_managers_config = config.get('execution-managers', {})
    for execution_manager_name, execution_manager_config in execution_managers_config.items():
        module_name, class_name = execution_manager_config["provider"].split(":")
        exectution_module = importlib.import_module(module_name)
        # ToDO add options
        execution_module_options = execution_manager_config.get("options", {})
        execution_managers[execution_manager_name] = getattr(exectution_module, class_name)(execution_module_options,
                                                                                            config)

    executor.execution_managers = execution_managers

    # Loading registries
    from ewps.registry import MetaWPSProcessRegistry, LocalFSRegistry, LocalPSRegistry
    registries = {}
    registries_config = config.get('registries', {})
    log.debug("Registry config: %s", registries_config)
    for registry_definition in registries_config:
        registry_name = registry_definition["name"]
        registry_type = registry_definition["type"]
        registry_options = registry_definition.get("options", {})

        meta_repo = MetaWPSProcessRegistry()
        if registry_name in registries:
            raise KeyError("Registry already defined!")
        registries[registry_name] = meta_repo

        execution_manager_name = registry_options.get("execution-manager", None)
        execution_manager = execution_managers.get(execution_manager_name, None)

        if registry_type == "fs":
            if execution_manager is None:
                raise KeyError("Referenced execution manager (`%s`) is not defined", execution_manager_name)
            paths = registry_options.get("locations", [])
            for repo_path in paths:
                meta_repo.add_registry(LocalFSRegistry(repo_path, execution_manager, execution_managers))
        elif registry_type == "discovered-processes":
            if execution_manager is None:
                raise KeyError("Referenced execution manager (`%s`) is not defined", execution_manager_name)
            packages = registry_options.get("packages", [])
            for package_definition_config in packages:
                package_definition_file_path = os.path.abspath(os.path.join(config_base_dir, package_definition_config))
                meta_repo.add_registry(
                    LocalPSRegistry(package_definition_file_path, execution_manager, execution_managers))
        elif registry_type == "discovered-processes-ng":
            if execution_manager is None:
                raise KeyError("Referenced execution manager (`%s`) is not defined", execution_manager_name)
            modules = registry_options.get("modules", [])
            for module_specification in modules:
                meta_repo.add_registry(LocalPythonRegistry(module_specification, execution_manager, execution_managers))
        elif registry_type == "meta":
            if execution_manager is not None:
                raise KeyError("Execution managers are not allowed for meta registries")
            referenced_registries = registry_options.get("registries", [])
            for registry_reference_name in referenced_registries:
                if registry_reference_name not in registries:
                    raise KeyError("Referenced registry `%s` does not exist!" % registry_reference_name)
                meta_repo.add_registry(registries[registry_reference_name])
        else:
            raise NotImplementedError("Unsupported registry type: %s" % registry_type)

        log.debug("Registry definition %s", repr(registry_definition))

    executor.registries = registries

    from flask import Flask
    log.info("Initializing Flask")
    flaskApp = Flask("ewps")
    #### Register the service endpoints
    from ewps.webapp import WPSHTTPHandler
    from ewps.webapp import serve_outputs, receive_result_output

    endpoints_config = config.get('endpoints', {})
    for endpoint_name, endpoint_config in endpoints_config.items():
        endpoint_chain = endpoints_config.get('chain', 'main')  # if not specified we use the `main` chain

        flaskApp.add_url_rule("/%s" % urlquote(endpoint_name),
                              view_func=WPSHTTPHandler.as_view('wps_service_%s' % endpoint_name, executor,
                                                               endpoint_chain))
        log.info("Endpoint `%s` registered and handled by chain `%s`", endpoint_name, endpoint_chain)

    def serve_logs():
        from flask import Response
        return Response(open("ewps-output.log").read(), mimetype="text/plain")

    flaskApp.add_url_rule("/result/<path:filename>", view_func=serve_outputs)
    flaskApp.add_url_rule("/logs", view_func=serve_logs)
    flaskApp.add_url_rule("/result-outputs/<string:jobid>", view_func=receive_result_output, methods=['POST'])

    return flaskApp


def main():
    import argparse
    import yaml
    parser = argparse.ArgumentParser(description='Provide WPS Services', prog="ewps")
    parser.add_argument("--configfile", type=argparse.FileType('r'))
    args = parser.parse_args()
    configfd = args.configfile
    if configfd is None:  # No config provided. Using the default one
        configfd = resource_stream(__name__, "_data/etc/config.yaml")

    config = yaml.load(configfd)
    flaskApp = initialize_ewps(config=config,
                               config_file=configfd.name,
                               logging_config=None)

    from logging import getLogger
    log = getLogger(__name__)

    serverConfig = config["server"]

    ### Start flask
    server_bind_address = serverConfig.get("host", "127.0.0.1")
    server_bind_port = serverConfig.get("port", "5000")
    # flaskApp.config['SERVER_NAME'] = "localhost:5000"
    log.debug("Starting flask on %s:%s", server_bind_address, server_bind_port)
    flaskApp.run(host=server_bind_address, port=server_bind_port, threaded=True)


if __name__ == "__main__":
    main()
