__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import logging
import lxml.etree as etree

from ewps.exceptions import WPSProtocolError

log = logging.getLogger(__name__)


class Response(object):
    def __init__(self, response, headers=None):
        self.response = response
        self.headers = {} if headers is not None else headers


class RequestHandler(object):
    _is_safe = False

    def handle_request(self, request, context):
        return self.handle(request, context)

    def handle(self, request, context):
        raise NotImplementedError("This method is meant to be implemented by subclasses")

    def handle_response(self, response, context):
        return response


class SafeRequestHandler(RequestHandler):
    """
    This class is similar to the above, except the fact that if you inherit from it the engine will keep only one instance of it
    """
    _is_safe = True


class VersionMapper(SafeRequestHandler):
    log = log.getChild('VersionMapper')

    def handle_request(self, request, context):
        request_version = request.attrib["version"]
        options = context.task.options
        if request_version not in options:
            raise WPSProtocolError("WPS Protocol not supported in mapper")
        destination_chain = options[request_version]
        self.log.debug("routing WPS %s request to chain `%s`", request_version, destination_chain)
        return context.executor.handle_in_chain(request, destination_chain, context)


class NOOP(RequestHandler):
    """
    Dummy handler doing nothing
    """

    def handle(self, request, context):
        log.debug("In NOOP")


noop = NOOP


class GoTo(RequestHandler):
    def handle(self, request, context):
        log.debug("in GOTO")
        destination_chain = context.task.options.get("destination")
        return context.executor.handle_in_chain(request, destination_chain, context)


goto = GoTo


class Error(RequestHandler):
    """
    Service always generating an error
    """

    def handle(self, request, context):
        1 / 0
