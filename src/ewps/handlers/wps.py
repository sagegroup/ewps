__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import copy
import logging
import lxml.etree as etree

from ewps.execution.core import ExecutionManger
from ewps.execution.local import LocalExecutionManager
from ewps.exceptions import WPSProtocolError, NoSuchProcess

from ewps.handlers.core import RequestHandler
from ewps.handlers.core import Response
from pkg_resources import resource_filename, resource_stream
import time
import os
from flask import Flask
from flask import request as flask_request

logger = logging.getLogger(__name__)

app = Flask(__name__)


class WPSHandler(RequestHandler):
    log = logger.getChild("WPSHandler")

    def handle_request(self, request, context):
        raise NotImplementedError()


class WPS2Handler(WPSHandler):
    log = logger.getChild("WPS2Handler")

    WPS_NS = "http://www.opengis.net/wps/2.0"
    OWS_NS = "http://www.opengis.net/ows/2.0"
    XSI_NS = 'http://www.w3.org/2001/XMLSchema-instance'
    XLINK_NS = "http://www.w3.org/1999/xlink"
    XML_WPS = "{%s}" % WPS_NS
    XML_OWS = "{%s}" % OWS_NS
    XML_XLINK = "{%s}" % XLINK_NS
    NSMAP = {
        'wps': WPS_NS,
        'xsi': XSI_NS,
        'ows': OWS_NS,
        'xlink': XLINK_NS
    }

    def __init__(self):
        super(WPS2Handler, self).__init__()
        self._execution_manager = None
        self._execution_info = None

    def handle_request(self, request_document, context):
        self.log.debug("handling wps2 request")

        request_name = etree.QName(request_document.tag).localname
        method_name = "handle_%s" % request_name
        hndl_func = getattr(self, method_name, None)
        if hndl_func is None:
            self.log.error("Method %s is not supported!", method_name)
            raise WPSProtocolError("Unsupported method: %s" % method_name)
        response_document = hndl_func(request_document, context)
        return Response(response_document)

    def get_meta_registry(self, context):
        task_registry = context.task.options.get('task-registry')
        meta_registry = context.executor.registries.get(task_registry, None)
        if meta_registry is None:
            raise WPSProtocolError("Invalid registry %s", task_registry)
        return meta_registry

    def handle_GetCapabilities(self, wps_request, context):
        # get all process from the registry
        meta_registry = self.get_meta_registry(context)
        processes = meta_registry.get_processes()

        # load GetCapabilities template
        tree = etree.parse(resource_stream(__name__, "../_data/templates/wps/getCapabilities_template.xml"))

        expr = "//*[local-name() = $name]"

        # add service url
        service_urls = tree.xpath(expr + "/*", name="HTTP")
        base_path = flask_request.headers["Host"]
        for service_url in service_urls:
            service_url.set(self.XML_XLINK + "href", base_path)

        processSummary = tree.xpath(expr, name="ProcessSummary")
        parent = processSummary[0].getparent()
        parent.remove(processSummary[0])
        processSummary_string = etree.tostring(processSummary[0])

        for process in processes:
            new_processSummary = copy.deepcopy(processSummary[0])

            # add title one (mandatory)
            title = new_processSummary.xpath(expr, name="Title")[0]
            title.text = process.title

            # add metadata zero or more (optional)
            # only max one supported
            # TODO add more metadata support
            metadata = new_processSummary.xpath(expr, name="Metadata")[0]
            if process.metadata is not None and process.metadata.get('href', None) is not None:
                metadata.set(self.XML_XLINK + "href", process.metadata['href'])
            else:
                new_processSummary.remove(metadata)

            # add identifier one (mandatory)
            identifier = new_processSummary.xpath(expr, name="Identifier")[0]
            identifier.text = process.identifier

            # add abstract zero or more (optional)
            # only max one supported
            # TODO add more abstracts support
            abstract = new_processSummary.xpath(expr, name="Abstract")[0]
            if process.abstract is not None:
                abstract.text = process.abstract
            else:
                new_processSummary.remove(abstract)

            # add keywords zero or more (optional)
            keywords = new_processSummary.xpath(expr, name="Keywords")[0]
            if process.keywords is not None and len(process.keywords) > 0:
                for keyword in process.keywords:
                    keyword_node = etree.Element(self.XML_OWS + "Keyword", nsmap=self.NSMAP)
                    keyword_node.text = keyword
                    keywords.append(keyword_node)


            else:
                new_processSummary.remove(keywords)

            new_processSummary.set('jobControlOptions', process.get_jobControlOptions())
            new_processSummary.set('outputTransmission', process.get_output_transmission())
            parent.append(new_processSummary)

        return tree

    def handle_DescribeProcess(self, wps_request, context):
        root = etree.Element("{http://www.opengis.net/wps/2.0}ProcessOfferings", nsmap=self.NSMAP)
        root.set('{%s}schemaLocation' % self.XSI_NS,
                 'http://www.opengis.net/wps/2.0 http://schemas.opengis.net/wps/2.0/wps.xsd')

        self.log.info("Handle DescribeProcess request")
        meta_registry = self.get_meta_registry(context)
        processes = meta_registry.get_processes()
        expr = "//*[local-name() = $name]"
        identifier_nodes = wps_request.xpath(expr, name="Identifier")

        for identifier in identifier_nodes:
            if identifier.text == 'ALL':
                for process in processes:
                    process_offering_node = etree.Element(self.XML_WPS + "ProcessOffering", nsmap=self.NSMAP)
                    root.append(process_offering_node)
                    process_offering_node.set('jobControlOptions', process.get_jobControlOptions())
                    process_offering_node.set('outputTransmission', process.get_output_transmission())
                    process_offering_node.set('processModel', process.get_process_model())
                    process_offering_node.append(process.get_process_definition().to_xml())
            else:
                process_summary = meta_registry.get_process_by_identifier(identifier.text)
                if process_summary is not None:
                    process_ofering_node = etree.Element(self.XML_WPS + "ProcessOffering", nsmap=self.NSMAP)
                    root.append(process_ofering_node)
                    process_ofering_node.set('jobControlOptions', process_summary.get_jobControlOptions())
                    process_ofering_node.set('outputTransmission', process_summary.get_output_transmission())
                    process_ofering_node.set('processModel', process_summary.get_process_model())
                    process_ofering_node.append(process_summary.get_process_definition().to_xml())
        return root

    def handle_Execute(self, wps_request, context):
        meta_registry = self.get_meta_registry(context)
        expr = "//*[local-name() = $name]"

        # get process by process identifier
        process_identifier = wps_request.xpath(expr, name="Identifier")[0]
        processing_name = process_identifier.text
        process_summary = meta_registry.get_process_by_identifier(processing_name)

        self.log.debug("Received Execute request for %s process", processing_name)

        # attach a proper ExecutionManager to WPSHandler
        execution_manager = process_summary.get_execution_manager()
        if execution_manager is None:
            # ToDO: maybe treat different exeption
            raise KeyError("Execution Manager not implemented")

        self.log.debug("Handle Execute Request with %s execution manager", execution_manager)

        # get process execution mode
        process_mode = wps_request.xpath(expr, name="Execute")[0].get("mode")

        # get inputs and outputs
        process_definition = process_summary.get_process_definition()
        process_inputs_values = process_definition.extract_input_values(wps_request)
        process_output_values = process_definition.extract_output_values(wps_request,
                                                                         process_summary.get_output_transmission())

        process_uuid = execution_manager.generate_process_uuid()

        self.log.debug("Process inputs " + str(process_inputs_values))
        self.log.debug("Process outputs " + str(process_output_values))
        self.log.debug("Prepare to start job with uuid %s", process_uuid)

        # send process to execution
        execution_manager.set_db_connector(getattr(context, context.db_connector_type))
        self.log.debug("Get process info from %s", execution_manager._db_connector)
        job_id = execution_manager.start(process_uuid, process_inputs_values, process_summary, process_output_values)

        self.log.debug("Started job %s" % process_uuid)

        # ToDo treat case if mode not supported by process
        # ToDo treat case if mode is not valid
        # ToDo analyze auto mode
        if process_mode == 'async' or process_mode == 'auto':
            # return a StatusInfo document
            # load Execute(Status Info) template
            tree = etree.parse(resource_stream(__name__, "../_data/templates/wps/execute_statusinfo_template.xml"))
            jobId = tree.xpath(expr, name="JobID")[0]
            jobId.text = process_uuid
            jobStatus = tree.xpath(expr, name="Status")[0]
            # get status
            status = execution_manager.get_status(process_uuid)
            jobStatus.text = status
        elif process_mode == 'sync':
            # return a result document
            # prepare a ResultInfo document
            tree = etree.Element("{http://www.opengis.net/wps/2.0}Result", nsmap=self.NSMAP)
            tree.set('{%s}schemaLocation' % self.XSI_NS,
                     'http://www.opengis.net/wps/2.0 http://schemas.opengis.net/wps/2.0/wps.xsd')
            jobid_node = etree.Element(self.XML_WPS + "JobID", nsmap=self.NSMAP)
            jobid_node.text = process_uuid
            tree.append(jobid_node)
            # wait until process is finished, query every 15 seconds
            finished = False
            finished = execution_manager.is_finished(process_uuid)
            while not finished:
                finished = execution_manager.is_finished(process_uuid)
                time.sleep(2)
            # the job has finished, prepare result document
            # ToDO handle output documents not created due to failed

            output_data = execution_manager.get_result(process_uuid)
            process_identifier = execution_manager.get_process_identifier(process_uuid)
            process_summary = meta_registry.get_process_by_identifier(process_identifier)
            output_nodes = process_summary.get_process_definition().get_output_result_to_xml(output_data, process_uuid)
            for output_node in output_nodes:
                tree.append(output_node)

        return tree

    def handle_GetStatus(self, wps_request, context):

        expr = "//*[local-name() = $name]"
        job_uuid = (wps_request.xpath(expr, name="JobID")[0]).text

        execution_manager = ExecutionManger()
        execution_manager.set_db_connector(getattr(context, context.db_connector_type))

        # get process identifier from uuid
        meta_registry = self.get_meta_registry(context)
        process_identifier = execution_manager.get_process_identifier(job_uuid)
        process_summary = meta_registry.get_process_by_identifier(process_identifier)

        # Todo handle error if process identifier is none

        self.log.debug("Get status for job with uuid %s and process identifier %s " % (job_uuid, process_identifier))

        # get the actual execution_manager of a process
        execution_manager = process_summary.get_execution_manager()
        if execution_manager is None:
            # ToDO: maybe treat different exeption
            raise KeyError("Execution Manager not implemented")

        tree = etree.parse(resource_stream(__name__, "../_data/templates/wps/execute_statusinfo_template.xml"))

        jobId = tree.xpath(expr, name="JobID")[0]
        jobId.text = job_uuid
        jobStatus = tree.xpath(expr, name="Status")[0]
        jobStatus.text = execution_manager.get_status(job_uuid)

        return tree

    def handle_GetResult(self, wps_request, context):
        self.log.info("Handling GetResult request")
        meta_registry = self.get_meta_registry(context)
        root = etree.Element("{http://www.opengis.net/wps/2.0}Result", nsmap=self.NSMAP)
        root.set('{%s}schemaLocation' % self.XSI_NS,
                 'http://www.opengis.net/wps/2.0 http://schemas.opengis.net/wps/2.0/wps.xsd')

        jobid_node = etree.Element(self.XML_WPS + "JobID", nsmap=self.NSMAP)
        expr = "//*[local-name() = $name]"
        job_uuid = (wps_request.xpath(expr, name="JobID")[0]).text
        jobid_node.text = job_uuid
        root.append(jobid_node)

        execution_manager = ExecutionManger()
        execution_manager.set_db_connector(getattr(context, context.db_connector_type))

        process_identifier = execution_manager.get_process_identifier(job_uuid)
        process_summary = meta_registry.get_process_by_identifier(process_identifier)
        # get the actual execution_manager of a process
        execution_manager = process_summary.get_execution_manager()
        output_data = execution_manager.get_result(job_uuid)
        output_nodes = process_summary.get_process_definition().get_output_result_to_xml(output_data, job_uuid)
        for output_node in output_nodes:
            root.append(output_node)
        return root

    def handle_Dismiss(self, wps_request, context):
        pass
