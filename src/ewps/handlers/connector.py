__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
from ewps.handlers.core import SafeRequestHandler
from ewps.utils import expand_variables


class LocalEnricher(SafeRequestHandler):
    def handle_request(self, request, context):
        if hasattr(context, "local"):  # We have already setup local env
            return
        else:
            from ewps.database import LocalConnector
            # prepare local connector
            db_path = expand_variables(context.task.options["path"], context.config)
            table_name = expand_variables(context.task.options["tabel_name"], context.config)
            db_connector = LocalConnector(path=db_path, table_name=table_name)
            db_connector.prepare_connection()
            context.local = db_connector
            context.db_connector_type = 'local'
            # TODO clean up old database and old job file directories
