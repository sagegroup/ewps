__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import lxml.etree as ET

WPS_NS = "http://www.opengis.net/wps/2.0"
OWS_NS = "http://www.opengis.net/ows/2.0"
XSI_NS = 'http://www.w3.org/2001/XMLSchema-instance'
XLINK_NS = "http://www.w3.org/1999/xlink"
XML_WPS = "{%s}" % WPS_NS
XML_OWS = "{%s}" % OWS_NS
XML_XLINK = "{%s}" % XLINK_NS
NSMAP = {
    'wps': WPS_NS,
    'xsi': XSI_NS,
    'ows': OWS_NS
}


class _WPSException(Exception):
    def __init__(self, message, code, http_code, locator=None):
        super(_WPSException, self).__init__(message)
        self.code = code
        self.locator = locator
        self.http_code = http_code
        self.message = message

    def to_xml(self):
        pass


class WPSException(_WPSException):
    def __init__(self, message, code=None, http_code=None, locator=None):
        super(WPSException, self).__init__(message, self.__class__.__name__ if code is None else code,
                                           self.http_code if http_code is None else http_code, locator)


class WPSProtocolError(WPSException):
    def __init__(self, message=None):
        super(WPSProtocolError, self).__init__(message, self.__class__.__name__,
                                               500, locator=None)


class RuntimeError(WPSException):
    def __init__(self, message=None):
        super(RuntimeError, self).__init__(message, self.__class__.__name__,
                                           500, locator=None)


class WPSDatabaseException(WPSException):
    def __init__(self, *args, **argv):
        super(WPSDatabaseException, self).__init__(*args, **argv)


class ExceptionReport(Exception):
    def __init__(self, *exceptions):
        self._exceptions = exceptions[:]

    def to_xml(self):
        raise NotImplementedError("This should be implemented by subclasses")


class WPS2ExceptionReport(ExceptionReport):
    def __init__(self, *args, **kws):
        super(WPS2ExceptionReport, self).__init__(*args, **kws)

    def to_xml(self):
        xml_response_document = ET.Element(XML_OWS + "ExceptionReport", nsmap=NSMAP)
        for exception in self._exceptions:
            exception_node = ET.Element(XML_OWS + "Exception", nsmap=NSMAP)
            if isinstance(exception, WPSException):
                exception_code = getattr(exception, "code", None)
                if exception_code is not None:
                    exception_node.attrib["exceptionCode"] = exception_code
                exception_message = getattr(exception, "message", None)
                if exception_message is not None:
                    exception_node.attrib["exceptionText"] = exception_message
            else:
                exception_node.attrib["code"] = "ZZZ"
            xml_response_document.append(exception_node)
        return xml_response_document

    def get_http_code(self):
        if not self._exceptions:
            return 500
        else:
            return getattr(self._exceptions[0], "http_code", 500)


class OperationNotSupported(WPSException):
    message = "Not Implemented"

    def __init__(self, message=None, locator=None):
        super(OperationNotSupported, self).__init__(message,
                                                    self.__class__.__name__,
                                                    501, locator)


class MissingParameterValue(WPSException):
    message = "Bad request"

    def __init__(self, message=None, locator=None):
        super(MissingParameterValue, self).__init__(message,
                                                    self.__class__.__name__,
                                                    400, locator)


class NoSuchProcess(WPSException):
    message = "One of the identifiers passed does not match with any of the processes offered by this server."

    def __init__(self, message=None, locator=None):
        super(NoSuchProcess, self).__init__(message,
                                            self.__class__.__name__,
                                            400, locator)


class InvalidParameterValue(WPSException):
    message = "Bad request"

    def __init__(self, message=None, locator=None):
        super(InvalidParameterValue, self).__init__(message,
                                                    self.__class__.__name__,
                                                    400, locator)


class VersionNegotiationFailed(WPSException):
    message = "Bad request"

    def __init__(self, message=None, locator=None):
        super(VersionNegotiationFailed, self).__init__(message,
                                                       self.__class__.__name__,
                                                       400, locator)


class InvalidUpdateSequence(WPSException):
    message = "Bad request"

    def __init__(self, message=None, locator=None):
        super(InvalidUpdateSequence, self).__init__(message,
                                                    self.__class__.__name__,
                                                    400, locator)


class OptionNotSupported(WPSException):
    message = "Not Implemented"

    def __init__(self, message=None, locator=None):
        super(OptionNotSupported, self).__init__(message,
                                                 self.__class__.__name__,
                                                 501, locator)
