__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
from string import Template


class __CustomTemplate(Template):
    idpattern = r'[_a-z][_a-z0-9.]*'

    def __init__(self, in_string):
        Template.__init__(self, in_string)


def __generate_substitutions_from_dict(dictionary, prefix="", result={}, depth=0):
    if depth > 42:
        raise KeyError("Recursion depth exceeded")
    for key in dictionary:
        if isinstance(dictionary[key], dict):
            __generate_substitutions_from_dict(dictionary[key], result=result, prefix="%s." % key, depth=depth + 1)
        else:
            result[prefix + key] = dictionary[key]
    return result


def expand_variables(in_string, backend_dictionary):
    substitutions = __generate_substitutions_from_dict(backend_dictionary)

    s = __CustomTemplate(in_string)
    return s.substitute(substitutions)
