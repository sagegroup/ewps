angular.module('wpsFormControl', [ 'wpsProperties', 'wpsExecuteInput', 'wpsExecuteOutput']);

/**
 * a common serviceInstance that holds all needed properties for a WPS service.
 * 
 * This service represents a shared object ´which is used across the different
 * application tabs/components like Setup, Capabilities, Execute etc.
 * 
 * This way, one single service instance can be used to easily share values and
 * parameters for each WPS operation represented by different Angular components
 */
angular.module('wpsFormControl').service('wpsFormControlService', 
		[ '$rootScope', 'wpsPropertiesService', 'wpsExecuteInputService', 'wpsExecuteOutputService', 
		  function($rootScope, wpsPropertiesService, wpsExecuteInputService, wpsExecuteOutputService) {

			this.wpsPropertiesServiceInstance = wpsPropertiesService;
			this.wpsExecuteInputServiceInstance = wpsExecuteInputService;
			this.wpsExecuteOutputServiceInstance = wpsExecuteOutputService;
			
			/**
			 * initialize as disabled and not clickable
			 */
			this.capabilitiesTab_classAttribute = 'disabled';
			this.capabilitiesTab_dataToggleAttribute = '';

			this.processesTab_classAttribute = 'disabled';
			this.processesTab_dataToggleAttribute = '';

			this.executeTab_classAttribute = 'disabled';
			this.executeTab_dataToggleAttribute = '';
			
			/*
			 * removeWPS button
			 */
			this.removeWpsServiceButton_classAttribute = 'disabled';
			
			/*
			 * remove already defined input/output for execute button
			 */
			this.isRemoveInputButtonDisabled = true;
			this.isRemoveOutputButtonDisabled = true;
			
			/*
			 * WPS capabilities error
			 */
			this.capabilitiesFailed_classAttribute = 'hidden';
			this.capabilitiesSuccess_classAttribute = 'hidden';
			
			this.capabilitiesFailed_errorThrown = '';
			
			/*
			 * describeProcess error
			 */
			this.describeProcessFailed_classAttribute = 'hidden';
			this.describeProcessSuccess_classAttribute = 'hidden';
			
			this.describeProcessFailed_errorThrown = '';
			
			/*
			 * execute error
			 */
			this.executeFailed_classAttribute = 'hidden';
			this.executeSuccess_classAttribute = 'hidden';
			
			this.executeFailed_errorThrown = '';
			
			/*
			 * indicates whether a request to fetch a reference output failed
			 */
			this.fetchingReferenceOutputFailed = false;
			this.fetchingReferenceOutputSuccess = false;
			
			/*
			 * tab management
			 */
			this.enableTabs = function(){
				/*
				 * enable tabs
				 */
				this.capabilitiesTab_classAttribute = 'enabled';
				this.capabilitiesTab_dataToggleAttribute = 'tab';

				this.processesTab_classAttribute = 'enabled';
				this.processesTab_dataToggleAttribute = 'tab';

				
				this.removeWpsServiceButton_classAttribute = 'enabled';
			};
			
			this.disableTabs = function(){
				/*
				 * disable all
				 */
				this.capabilitiesTab_classAttribute = 'disabled';
				this.capabilitiesTab_dataToggleAttribute = '';

				this.processesTab_classAttribute = 'disabled';
				this.processesTab_dataToggleAttribute = '';

				this.executeTab_classAttribute = 'disabled';
				this.executeTab_dataToggleAttribute = '';

			};
			
			this.resetTabContents = function(){
				
				this.wpsExecuteInputServiceInstance.reset();
				this.wpsExecuteOutputServiceInstance.reset();
				
				this.wpsPropertiesServiceInstance.resetExecuteContents();
				
				this.resetErrorMessages();
				this.resetExecute();
			};
			
			this.resetErrorMessages = function(){
				/*
				 * WPS capabilities error
				 */
				this.capabilitiesFailed_classAttribute = 'hidden';
				this.capabilitiesSuccess_classAttribute = 'hidden';
				
				this.capabilitiesFailed_errorThrown = '';
				
				/*
				 * describeProcess error
				 */
				this.describeProcessFailed_classAttribute = 'hidden';
				this.describeProcessSuccess_classAttribute = 'hidden';
				
				this.describeProcessFailed_errorThrown = '';
				
				/*
				 * execute error
				 */
				this.executeFailed_classAttribute = 'hidden';
				this.executeSuccess_classAttribute = 'hidden';
				
				this.executeFailed_errorThrown = '';
			};
			
			this.resetExecute = function(){
				/*
				 * reset execute form
				 */
				this.wpsPropertiesServiceInstance.resetResponseDocuments();
				this.resetErrorMessages();
				this.wpsPropertiesServiceInstance.resetExecuteContents();
				
				// reset all input forms!
				$rootScope.$broadcast('reset-all-input-forms', {});
				
//				// remove all overlays from map!
//				$rootScope.$broadcast('reset-map-overlays', {});
				
				this.fetchingReferenceOutputFailed = false;
				this.fetchingReferenceOutputSuccess = false;
			};

		} ]);
angular.module('wpsExecuteInput', []);

/**
 * a common serviceInstance that holds attributes and methods needed for
 * configuring execute inputs
 */
angular.module('wpsExecuteInput').service('wpsExecuteInputService',  function () {

    /*
     * execute lists
     */
    this.unconfiguredExecuteInputs;
    this.alreadyConfiguredExecuteInputs = [];

    this.selectedExecuteInput;

    /*
     * literalData
     */
    this.literalInputValue;

    /*
     * complexInput
     */
    this.selectedExecuteInputFormat;
    this.asReference = false;
    this.asFile = false;
    this.asDrawing = false;
    this.asComplex = false;
    this.complexPayload;

    /*
     * bbox input
     */
    this.selectedExecuteInputCrs;
    this.bboxLowerCorner;
    this.bboxUpperCorner;
    this.bboxAsGeoJSON;

    this.markInputAsConfigured = function (input) {
        /*
         * move the input with the fitting identifier from unconfigured input
         * list to configured.
         */
        /*
         * remove input
         */
        var index = undefined;
        var alreadyExists = false;

        for (var i = 0; i < this.unconfiguredExecuteInputs.length; i++) {
            var currentInput = this.unconfiguredExecuteInputs[i];

            if (currentInput.identifier === input.identifier) {
                index = i;
                alreadyExists = true;
                break;
            }
        }

        if (alreadyExists)
            this.unconfiguredExecuteInputs.splice(index, 1);

        /*
         * add input
         */
        index = undefined;
        alreadyExists = false;

        if (!this.alreadyConfiguredExecuteInputs)
            this.alreadyConfiguredExecuteInputs = [];

        for (var i = 0; i < this.alreadyConfiguredExecuteInputs.length; i++) {
            var currentInput = this.alreadyConfiguredExecuteInputs[i];

            if (currentInput.identifier === input.identifier) {
                index = i;
                alreadyExists = true;
                break;
            }
        }

        if (alreadyExists)
            this.alreadyConfiguredExecuteInputs.splice(index, 1);

        this.alreadyConfiguredExecuteInputs.push(input);
        
        /*
         * set selection to undefined as visual feedback (and prevent that the same 
         * input view is still shown)
         */
        this.selectedExecuteInput = undefined;
    };

    

    this.removeInputFromAlreadyDefinedInputs = function (input) {
        var index = undefined;

        for (var i = 0; i < this.alreadyConfiguredExecuteInputs.length; i++) {
            var currentInput = this.alreadyConfiguredExecuteInputs[i];

            if (currentInput.identifier === input.identifier) {
                index = i;
                break;
            }
        }

        this.alreadyConfiguredExecuteInputs.splice(index, 1);
    };

    this.addInputToUnconfiguredExecuteInputs = function (currentInput) {
        this.unconfiguredExecuteInputs.push(currentInput);
    };

    this.reset = function () {
        this.unconfiguredExecuteInputs = [];
        this.alreadyConfiguredExecuteInputs = [];

        this.selectedExecuteInput = undefined;

        /*
         * literalData
         */
        this.literalInputValue = undefined;

        /*
         * complexInput
         */
        this.selectedExecuteInputFormat = undefined;
        this.asReference = false;
        this.complexPayload = undefined;

        /*
         * bbox input
         */
        this.selectedExecuteInputCrs = undefined;
        this.bboxLowerCorner = undefined;
        this.bboxUpperCorner = undefined;
    };

});
angular.module('wpsExecuteOutput', []);

/**
 * a common serviceInstance that holds attributes and methods needed for
 * configuring execute outputs
 */
angular.module('wpsExecuteOutput').service('wpsExecuteOutputService', function() {

	/*
	 * execute lists
	 */
	this.unconfiguredExecuteOutputs = [];
	this.alreadyConfiguredExecuteOutputs = [];

	this.selectedExecuteOutput;

	this.selectedTransmissionMode;
	
	/*
	 * complexOutput
	 */
	this.selectedExecuteOutputFormat;

	this.markOutputAsConfigured = function(output) {
		/*
		 * move the output with the fitting identifier from unconfigured output
		 * list to configured.
		 */
		/*
		 * remove output
		 */
		var index = undefined;
		var alreadyExists = false;

		for (var i = 0; i < this.unconfiguredExecuteOutputs.length; i++) {
			var currentOutput = this.unconfiguredExecuteOutputs[i];

			if (currentOutput.identifier === output.identifier) {
				index = i;
				alreadyExists = true;
				break;
			}
		}

		if(alreadyExists)
			this.unconfiguredExecuteOutputs.splice(index, 1);

		/*
		 * add output
		 */
		index = undefined;
		alreadyExists = false;
		
		if(! this.alreadyConfiguredExecuteOutputs)
			this.alreadyConfiguredExecuteOutputs = [];
		
		for (var i = 0; i < this.alreadyConfiguredExecuteOutputs.length; i++) {
			var currentOutput = this.alreadyConfiguredExecuteOutputs[i];

			if (currentOutput.identifier === output.identifier) {
				index = i;
				alreadyExists = true;
				break;
			}
		}
		
		if(alreadyExists)
			this.alreadyConfiguredExecuteOutputs.splice(index, 1);
		
		this.alreadyConfiguredExecuteOutputs.push(output);
		
		/*
		 * set selection to undefined as visual feedback (and prevent that the same 
		 * output view is still shown)
		 */
		this.selectedExecuteOutput = undefined;
		this.selectedTransmissionMode = undefined;
	};
	
	this.removeOutputFromAlreadyDefinedOutputs = function(output){
		var index = undefined;
		
		for(var i=0; i<this.alreadyConfiguredExecuteOutputs.length; i++){
			var currentOutput = this.alreadyConfiguredExecuteOutputs[i];
			
			if(currentOutput.identifier === output.identifier){
				index = i;
				break;
			}
		}

			this.alreadyConfiguredExecuteOutputs.splice(index, 1);
	};
	
	this.addOutputToUnconfiguredExecuteOutputs = function(currentOutput){
		this.unconfiguredExecuteOutputs.push(currentOutput);
	};
	
	this.reset = function(){
		this.unconfiguredExecuteOutputs = [];
		this.alreadyConfiguredExecuteOutputs = [];

		this.selectedExecuteOutput = undefined;
		
		this.selectedTransmissionMode = undefined;
		
		/*
		 * complexOutput
		 */
		this.selectedExecuteOutputFormat = undefined;
	};

});
angular.module('wpsProperties', ['wpsExecuteInput', 'wpsExecuteOutput', 'wpsGeometricOutput', 'wpsMap']);

/**
 * a common serviceInstance that holds all needed properties for a WPS service.
 * 
 * This service represents a shared object ´which is used across the different
 * application tabs/components like Setup, Capabilities, Execute etc.
 * 
 * This way, one single service instance can be used to easily share values and
 * parameters for each WPS operation represented by different Angular components
 */
angular
		.module('wpsProperties')
		.service(
				'wpsPropertiesService', ['$rootScope', 'wpsExecuteInputService', 'wpsExecuteOutputService', 
				                         'wpsGeometricOutputService', 'wpsMapService',
				function($rootScope, wpsExecuteInputService, wpsExecuteOutputService, wpsGeometricOutputService,
						wpsMapService) {
					
					this.wpsExecuteInputServiceInstance = wpsExecuteInputService;
					this.wpsExecuteOutputServiceInstance = wpsExecuteOutputService;
					this.wpsGeometricOutputServiceInstance = wpsGeometricOutputService;
					this.wpsMapServiceInstance = wpsMapService;

					/*
					 * this property represents the WpsService object of wps-js
					 * library that encapsulates the functionality to
					 * communicate with WPS
					 */
					this.wpsServiceLibrary;

					this.availableWpsServices = [
							"http://localhost:5000/wps" ];

					this.serviceVersion = '2.0.0';
					this.selectedServiceUrl = 'http://localhost:5000/wps';
					
					this.responseFormats = [ 'document', 'raw' ];
					
					this.selectedProcess = '';

					this.capabilities;

					this.processDescription;

					this.executeRequest = {};

					/*
					 * this property is used to store execute result document from WPS 2.0.0
					 */
					this.resultDocument_wps_2_0;
					
					/*
					 * this property is used to store execute response document from WPS 1.0.0
					 */
					this.responseDocument_wps_1_0;
					
					/*
					 * only used for WPS 2.0 status info document
					 */
					this.statusInfoDocument_wps_2_0;
					
					/*
					 * used if wps responds with raw output
					 */
					this.rawOutput;
					
					this.inputGenerator = new InputGenerator();
					this.outputGenerator = new OutputGenerator();

					this.initializeWpsLibrary = function() {
						this.wpsServiceLibrary = new WpsService({
							url : this.selectedServiceUrl,
							version : this.serviceVersion
						});
					};

					this.getCapabilities = function(callbackFunction) {
						/*
						 * take currently selected URL and version and execute
						 * an getCapabilitiesRequest
						 */
						this.wpsServiceLibrary.getCapabilities_GET(callbackFunction);
					};
					
					
					
					this.onCapabilitiesChange = function(capabilitiesObject){
						this.capabilities = capabilitiesObject;
						
						/*
						 * modify list of available processes from capabilities!
						 */
					};

					this.describeProcess = function(callbackFunction) {
						/*
						 * take currently selected process and execute
						 * an describeProcessRequest
						 */
						this.wpsServiceLibrary.describeProcess_GET(callbackFunction, this.selectedProcess.identifier);
					};
					
					this.onProcessDescriptionChange = function(processDescription){
						this.processDescription = processDescription;
						
						/*
						 * set all inputs and outputs as non configured for executeRequest!
						 */
						this.resetExecuteContents();
					};
					
					this.resetProcessDescription = function(){
						/*
						 * remove currently selected process
						 */
						this.selectedProcess = undefined;
						this.processDescription = undefined;
					};
					
					this.resetExecuteContents = function(){
						this.executeRequest = {};
						
						this.wpsExecuteInputServiceInstance.unconfiguredExecuteInputs = [];
						this.wpsExecuteOutputServiceInstance.unconfiguredExecuteOutputs = [];
						this.wpsExecuteInputServiceInstance.alreadyConfiguredExecuteInputs = [];
						this.wpsExecuteOutputServiceInstance.alreadyConfiguredExecuteOutputs = [];
						
						this.executeRequest.responseFormat = undefined;
						this.executeRequest.executionMode = undefined;
						
						if(this.processDescription){
							this.wpsExecuteInputServiceInstance.unconfiguredExecuteInputs.push.apply(this.wpsExecuteInputServiceInstance.unconfiguredExecuteInputs, this.processDescription.process.inputs);
							this.wpsExecuteOutputServiceInstance.unconfiguredExecuteOutputs.push.apply(this.wpsExecuteOutputServiceInstance.unconfiguredExecuteOutputs, this.processDescription.process.outputs);
							this.executeRequest.executionMode = this.processDescription.jobControlOptions[0];
							this.executeRequest.responseFormat = this.responseFormats[0];
						}							
							
					};
					
					this.addLiteralInput = function(literalInput){
						if(! this.executeRequest.inputs)
							this.executeRequest.inputs = [];

						this.removeAlreadyExistingInputWithSameIdentifier(literalInput);
						
						/*
						 * use InputGenerator of wps-js-lib library!
						 * 
						 * createLiteralDataInput_wps_1_0_and_2_0(identifier, dataType,
								uom, value) <-- only identifier and value are mandatory
						 */
						var newInput = this.inputGenerator.createLiteralDataInput_wps_1_0_and_2_0(literalInput.identifier, undefined,
								undefined, this.wpsExecuteInputServiceInstance.literalInputValue);
						
						this.executeRequest.inputs.push(newInput);
					};
					
					this.addLiteralOutput = function(literalOutput){
						if(! this.executeRequest.outputs)
							this.executeRequest.outputs = [];

						this.removeAlreadyExistingOutputWithSameIdentifier(literalOutput);
						
						/*
						 * use OutputGenerator of wps-js-lib library!
						 * 
						 * depends on service version!
						 * 
						 * createLiteralOutput_WPS_1_0 : function(identifier, asReference)
						 * 
						 * createLiteralOutput_WPS_2_0 : function(identifier, transmission)
						 */
						
						var asReference = false;
						if(this.wpsExecuteOutputServiceInstance.selectedTransmissionMode === 'reference')
							asReference = true;
						
						var newOutput;
						if(this.serviceVersion === '1.0.0')
							newOutput = this.outputGenerator.createLiteralOutput_WPS_1_0(literalOutput.identifier,
									asReference);
						else
							newOutput = this.outputGenerator.createLiteralOutput_WPS_2_0(literalOutput.identifier,
									this.wpsExecuteOutputServiceInstance.selectedTransmissionMode);
						
						this.executeRequest.outputs.push(newOutput);
					};
					
					this.addComplexInput = function(complexInput){
						if(! this.executeRequest.inputs)
							this.executeRequest.inputs = [];

						this.removeAlreadyExistingInputWithSameIdentifier(complexInput);
						
						/*
						 * use InputGenerator of wps-js-lib library!
						 * 
						 * createComplexDataInput_wps_1_0_and_2_0 : function(identifier,
							mimeType, schema, encoding, asReference, complexPayload) 
							<-- only identifier and complexPayload are mandatory
						 */
						
						var format = this.wpsExecuteInputServiceInstance.selectedExecuteInputFormat;
						
						var newInput = this.inputGenerator.createComplexDataInput_wps_1_0_and_2_0(complexInput.identifier, 
								format.mimeType, format.schema, format.encoding, 
								this.wpsExecuteInputServiceInstance.asReference, 
								this.wpsExecuteInputServiceInstance.complexPayload);
						
						var inputLayerPropertName = this.wpsMapServiceInstance.generateUniqueInputLayerPropertyName(complexInput.identifier);
						
                        $rootScope.$broadcast('add-input-layer', {'geojson':newInput.complexPayload,'name':complexInput.identifier, 'layerPropertyName':inputLayerPropertName});

						this.executeRequest.inputs.push(newInput);
					};
					
					this.addComplexOutput = function(complexOutput){
						if(! this.executeRequest.outputs)
							this.executeRequest.outputs = [];

						this.removeAlreadyExistingOutputWithSameIdentifier(complexOutput);
						
						/*
						 * use OutputGenerator of wps-js-lib library!
						 * 
						 * depends on service version!
						 * 
						 * createComplexOutput_WPS_1_0 : function(identifier, mimeType, schema,
								encoding, uom, asReference, title, abstractValue)
								
							createComplexOutput_WPS_2_0 : function(identifier, mimeType, schema,
								encoding, transmission)
						 */
						
						var format = this.wpsExecuteOutputServiceInstance.selectedExecuteOutputFormat;
						
						var asReference = false;
						if(this.wpsExecuteOutputServiceInstance.selectedTransmissionMode === 'reference')
							asReference = true;
						
						var newOutput;
						if(this.serviceVersion === '1.0.0')
							newOutput = this.outputGenerator.createComplexOutput_WPS_1_0(complexOutput.identifier,
									format.mimeType, format.schema, format.encoding, undefined, asReference, undefined, undefined);
						else
							newOutput = this.outputGenerator.createComplexOutput_WPS_2_0(complexOutput.identifier,
									format.mimeType, format.schema, format.encoding, 
									this.wpsExecuteOutputServiceInstance.selectedTransmissionMode);
						
						this.executeRequest.outputs.push(newOutput);
					};
					
					this.addBoundingBoxInput = function(bboxInput){
						if(! this.executeRequest.inputs)
							this.executeRequest.inputs = [];

						this.removeAlreadyExistingInputWithSameIdentifier(bboxInput);
						
						/*
						 * use InputGenerator of wps-js-lib library!
						 * 
						 * createBboxDataInput_wps_1_0_and_2_0 : function(identifier, crs,
					dimension, lowerCorner, upperCorner)  
							<-- only dimension is not mandatory
						 */
						
						var newInput = this.inputGenerator.createBboxDataInput_wps_1_0_and_2_0(bboxInput.identifier, 
								this.wpsExecuteInputServiceInstance.selectedExecuteInputCrs, undefined,
								this.wpsExecuteInputServiceInstance.bboxLowerCorner, 
								this.wpsExecuteInputServiceInstance.bboxUpperCorner);
						
						var bboxAsGeoJSON_String = JSON.stringify(this.wpsExecuteInputServiceInstance.bboxAsGeoJSON);
                        
                        var inputLayerPropertName = this.wpsMapServiceInstance.generateUniqueInputLayerPropertyName(bboxInput.identifier);
						
                        $rootScope.$broadcast('add-input-layer', {'geojson':bboxAsGeoJSON_String,'name':bboxInput.identifier, 'layerPropertyName':inputLayerPropertName});
						
						this.executeRequest.inputs.push(newInput);
					};
					
					this.addBoundingBoxOutput = function(bboxOutput){
						this.addLiteralOutput(bboxOutput);
					};
					
					this.removeAlreadyExistingInputWithSameIdentifier = function(input){
						var index = undefined;
						var isALreadyDefined = false;
						
						for(var i=0; i<this.executeRequest.inputs.length; i++){
							var currentInput = this.executeRequest.inputs[i];
							
							if(currentInput.identifier === input.identifier){
								index = i;
								isALreadyDefined = true;
								break;
							}
						}
						
						if(isALreadyDefined)
							this.executeRequest.inputs.splice(index, 1);
					};
					
					this.removeAlreadyExistingOutputWithSameIdentifier = function(output){
						var index = undefined;
						var isALreadyDefined = false;
						
						for(var i=0; i<this.executeRequest.outputs.length; i++){
							var currentOutput = this.executeRequest.outputs[i];
							
							if(currentOutput.identifier === output.identifier){
								index = i;
								isALreadyDefined = true;
								break;
							}
						}
						
						if(isALreadyDefined)
							this.executeRequest.outputs.splice(index, 1);
					};

					this.execute = function(callbackFunction) {
						
						this.resetResponseDocuments();
						
						/*
						 * collect execute request information and
						 * perfrom execute request
						 * 
						 * API interface from wps-js-lib
						 * execute : function(callbackFunction, processIdentifier, responseFormat,
								executionMode, lineage, inputs, outputs)
						 */
						
						var processIdentifier = this.selectedProcess.identifier;
						var responseFormat = this.executeRequest.responseFormat;
						/*
						 * this.executeRequest.executionMode has value "sync-execute" or "async-execute"
						 * 
						 * but execute request expects either "sync" or "async".
						 * 
						 * Hence we split the value!
						 */
						var executionMode = this.executeRequest.executionMode.split("-")[0];
						var lineage = false; /* only applicable for wps 1.0; we just leave it out */
						var inputs = this.executeRequest.inputs;
						var outputs = this.executeRequest.outputs;
														
						this.wpsServiceLibrary.execute(
										callbackFunction, processIdentifier,
										responseFormat, executionMode, lineage,
										inputs, outputs);
						
						/*
						 * reset execute contents
						 */
						this.resetExecuteContents();
					};
					
					this.resetResponseDocuments = function(){
						this.resultDocument_wps_2_0 = undefined;
						this.responseDocument_wps_1_0 = undefined;
						this.statusInfoDocument_wps_2_0 = undefined;
						this.rawOutput = undefined;
					};
					
					this.onExecuteResponseChange = function(executeResponse){

						/*
						 * based on the type of the response, a different concrete 
						 * property is instantiated
						 */

						/*
						 * as we intend to visualize geometric outputs on the map
						 * the response documents should be inspected for geometric outputs
						 * 
						 * then each geometric output should be visualized an an individual 
						 * output layer on the map using its identifier as layer name.
						 * 
						 * as first step we want to visualize GeoJSON data.
						 * Hence we have to detect, whether an output is GeoJSON format.
						 * IF NOT, a future task could be to contact a transformation WPS, 
						 * which transforms other geometric formats to GeoJSON
						 */
						
						/*
						 * approach: extract outputs and then have a method to process all outputs!
						 */
						
						switch (executeResponse.type){
						case "responseDocument":
							this.responseDocument_wps_1_0 = executeResponse.responseDocument;
							
							/*
							 * extract outputs array
							 */
							
							var allOutputs = executeResponse.responseDocument.outputs;
							
							/*
							 * call method to clone array and reduce content to geometric outputs
							 * 
							 * TODO what about outputs that are given as URL????????
							 */
							
							var geometricOutputs = this.wpsGeometricOutputServiceInstance.getGeometricOutputs(allOutputs);
							
							/*
							 * call visualization method for geometric outputs
							 */
							this.wpsMapServiceInstance.visualizeGeometricOutputs(geometricOutputs);
							
							break;
						
						case "resultDocument":
							this.resultDocument_wps_2_0 = executeResponse.responseDocument;
							
							/*
							 * extract outputs array
							 */
							
							var allOutputs = executeResponse.responseDocument.outputs;
							
							/*
							 * call method to clone array and reduce content to geometric outputs
							 * 
							 * TODO what about outputs that are given as URL????????
							 */
							
							var geometricOutputs = this.wpsGeometricOutputServiceInstance.getGeometricOutputs(allOutputs);
							
							/*
							 * call visualization method for geometric outputs
							 */
							this.wpsMapServiceInstance.visualizeGeometricOutputs(geometricOutputs);
							
							break;
							
						case "statusInfoDocument":
							this.statusInfoDocument_wps_2_0 = executeResponse.responseDocument;
							break;
							
						case "rawOutput":
							this.rawOutput = executeResponse.responseDocument;
							
							/*
							 * TODO implement
							 * 
							 * TODO what about outputs that are given as URL????????
							 */
							
							/*
							 * detect whether output is geometric
							 */
							
							/*
							 * call visualization method for geometric output
							 */
							
							break;
						}
						
					};

					this.getStatus = function(callbackFunction, jobId) {
						/*
						 * clear previous response documents
						 * 
						 * and perform getStatus request
						 */
						this.resetResponseDocuments();
						
						this.wpsServiceLibrary.getStatus_WPS_2_0(callbackFunction, jobId);
						
					};

					this.getResult = function(callbackFunction, jobId) {
						/*
						 * clear response documents
						 * 
						 * and perform getResult request
						 */
						this.resetResponseDocuments();
						
						
						this.wpsServiceLibrary.getResult_WPS_2_0(callbackFunction, jobId);
					};
					
					this.fetchUpdatedResponseDocument_wps_1_0 = function(
							callbackFunction, documentLocation){
						/*
						 * clear response documents
						 * 
						 * and fetch new response document for WPS 1.0
						 */
						this.resetResponseDocuments();
						
						this.wpsServiceLibrary.parseStoredExecuteResponse_WPS_1_0(callbackFunction, 
								documentLocation);
					};

					this.addNewWpsServiceUrl = function(url) {
						if (url.startsWith('http')) {
							this.availableWpsServices.push(url);
						}
					};

					this.removeWpsServiceUrl = function() {
						for (var int = 0; int < this.availableWpsServices.length; int++) {
							if (this.availableWpsServices[int] == this.selectedServiceUrl) {
								this.availableWpsServices.splice(int, 1);

								this.selectedServiceUrl = '';
							}
						}
					};
				

				}]);

angular.module('wpsInputOutputFilter', []);

/**
 * provides methods to filter input lists for their type (literalData,
 * complexData, boundingBoxData)
 */
angular.module('wpsInputOutputFilter').service('wpsInputOutputFilterService', function() {

	this.hasLiteralData = function(object) {
		if (object.literalData)
			return true;

		return false;
	},

	this.hasComplexData = function(object) {
		if (object.complexData)
			return true;

		return false;
	},

	this.hasBoundingBoxData = function(object) {
		if (object.boundingBoxData)
			return true;

		return false;
	}

});
angular.module('wpsGeometricOutput', [ ]);

/**
 * a common serviceInstance that holds all needed properties for a WPS service.
 * 
 * This service represents a shared object ´which is used across the different
 * application tabs/components like Setup, Capabilities, Execute etc.
 * 
 * This way, one single service instance can be used to easily share values and
 * parameters for each WPS operation represented by different Angular components
 */
angular.module('wpsGeometricOutput').service('wpsGeometricOutputService', 
		[ function() {

			this.getGeometricOutputs = function(allOutputs){
				
				/*
				 * inspect array of outputs for geometric outputs only
				 * 
				 * first concentrate on GeoJSON output
				 * 
				 * TODO future task: transform other geometric formats to GeoJSON
				 * using a special WPS 
				 */
				
				/*
				 * create empty new array "geometricOutputs"
				 */
				
				var geometricOutputs = new Array();
				
				/*
				 * only geometric outputs shall be put into array "geometricOutputs"
				 */
				
				for (i=0; i < allOutputs.length; i++){
					var currentOutput = allOutputs[i];

					// is geometric output?
					if(this.isGeometricFormat(currentOutput)){
						
						/*
						 * outputs may be "as reference" or "as value"
						 * hence, an output may have different properties
						 */
						if(currentOutput.reference){
							/*
							 * output is as reference
							 */
							
							// is GeoJSON
							if(this.isGeoJSON(currentOutput))
								geometricOutputs.push(currentOutput);
							
							// is WMS (will be a complexOutput with mimeType = application/WMS)
							else if(this.isWMS(currentOutput))
								geometricOutputs.push(currentOutput);
							
							// is not GeoJSON but can be converted to GeoJSON								
							else if(this.canBeTransformedToGeoJSON(currentOutput)){
								
							}
							
						} else{
							/*
							 * output is as value! hence has .data property
							 */
							
							/*
							 * bounding box outputs will be transformed to GeoJSON by wps-map.module.js
							 */
							
							if(currentOutput.data.boundingBoxData)
								geometricOutputs.push(currentOutput);
							
							// is GeoJSON
							else if(this.isGeoJSON(currentOutput))
								geometricOutputs.push(currentOutput);
							
							else if(this.isWMS(currentOutput))
								geometricOutputs.push(currentOutput);
							
							// is not GeoJSON but can be converted to GeoJSON								
							else if(this.canBeTransformedToGeoJSON(currentOutput)){
								/*
								 * TODO transform to GeoJSOn using special WPS instance
								 * 
								 * and add to "geometricOutputs" array
								 */
							}
						}
						
					}
				}
				
				/*
				 * new all geometric outputs have been found.
				 * 
				 * return array "geometricOutputs"
				 */
				return geometricOutputs;
			};
			
			this.isGeometricFormat = function(currentOutput){
				/*
				 * check if mimeType / format of output is supported
				 */
				
				/*
				 * output might be stored as Reference or as value
				 */
				
				if(currentOutput.reference){
					/*
					 * output is given as reference
					 */
					
					return this.isGeometricFormat_reference(currentOutput);
					
				} else{
					
					/*
					 * output is given as value
					 */
				
					if(currentOutput.data.complexData)
						return this.isGeometricFormat_complexData(currentOutput);
					
					//bounding box is always geometric
					else if(currentOutput.data.boundingBoxData)
						return true;
					
					else 
						return false;
				
				}
			};
			
			this.isGeometricFormat_reference = function(currentOutput){
				var reference = currentOutput.reference;
				var format = reference.format;
				
				if (this.isFormatSupported(format))
					return true;
				
				else
					return false;
			};
			
			this.isGeometricFormat_complexData = function(currentOutput){
				var format = currentOutput.data.complexData.mimeType;
				
				if (this.isFormatSupported(format))
					return true;
				
				else
					return false;
			};
			
			this.isFormatSupported = function(format){
				/*
				 * TODO implement better way using an external file of supported formats
				 */
				
				if (format === 'application/vnd.geo+json')
					return true;
				else if (format === 'application/WMS')
					return true;
			};
			
			this.isGeoJSON = function(currentOutput){
				/*
				 * output can be as reference or as value
				 *  properties differ!
				 */
				
				var format;
				
				if(currentOutput.reference){
					/*
					 * as reference
					 */
					format = currentOutput.reference.format;
				}
				else{
					/*
					 * has .data property and will be a complexOutput!
					 */
					format = currentOutput.data.complexData.mimeType;
				}
				
				if (this.isGeoJSON_mimeType(format))
					return true;
				
				return false;
			};
			
			this.isGeoJSON_mimeType = function(mimeType){
				
				if (mimeType === 'application/vnd.geo+json')
					return true;
				
				return false;
			};
			
			this.isWMS = function(currentOutput){
				/*
				 * output can be as reference or as value
				 *  properties differ!
				 */
				
				var format;
				
				if(currentOutput.reference){
					/*
					 * as reference
					 */
					format = currentOutput.reference.format;
				}
				else{
					/*
					 * has .data property and will be a complexOutput!
					 */
					format = currentOutput.data.complexData.mimeType;
				}
				
				if (format === 'application/WMS')
					return true;
			};
			
			this.canBeTransformedToGeoJSON = function(currentOutput){
				/*
				 * TODO implement
				 */
				
				/*
				 * if output is a bounding box, than return true,
				 * as it can always be transformed to GeoJSON!
				 */
				if (currentOutput.data.boundingBoxData)
					return true;
			};
			
			this.transformToGeoJSON = function(currentOutput){
				/*
				 * TODO implement using a special WPS that transforms 
				 * the output to GeoJSON
				 */
			};

		} ]);
angular.module('wpsControls', [ 'wpsSetup', 'wpsCapabilities', 'wpsProcesses',
		'wpsExecute', 'wpsFormControl' ]);
angular.module('wpsControls').component(
		'wpsControls',
		{
			templateUrl : "components/wpsUserInterface/wpsControls/wps-controls.template.html",
			/*
			 * controller is injected with two module-values from module
			 * wpsSetup
			 */
			controller : [ 'wpsFormControlService',
					function ControlsController(wpsFormControlService) {

						this.formControlServiceInstance = wpsFormControlService;

					} ]
		});
angular.module('wpsSetup', ['wpsProperties', 'wpsFormControl', 'wpsAddServiceModal', 'wpsChangeLanguage']);
angular
		.module('wpsSetup')
		.component(
				'wpsSetup',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsSetup/wps-setup.template.html",
					/*
					 * injected with a modules service method that manages
					 * enabled tabs
					 */
					controller : [
							'wpsPropertiesService', 'wpsFormControlService', '$scope', 
							function WpsSetupController(wpsPropertiesService, wpsFormControlService, $scope) {
								/*
								 * references to wpsPropertiesService and wpsFormControl instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								this.wpsPropertiesServiceInstance.selectedServiceUrl = '';
								
								this.wpsFormControlServiceInstance = wpsFormControlService;
								
								this.isRemoveButtonDisabled = true;
								
								$scope.loadingData = false;

								this.changeVersion = function() {
									/*									
									 * reset capabilities divs
									 */
									wpsFormControlService.capabilitiesSuccess_classAttribute = 'hidden';
									wpsFormControlService.capabilitiesFailed_classAttribute = 'hidden';
									
									/*
									 * disable all tabs, will be enabled on capabilities response
									 */
									wpsPropertiesService.resetProcessDescription();
									wpsFormControlService.disableTabs();
									wpsFormControlService.resetTabContents();
									
									if (this.wpsPropertiesServiceInstance.selectedServiceUrl != '' && this.wpsPropertiesServiceInstance.selectedServiceUrl != undefined){
										wpsPropertiesService.initializeWpsLibrary();
										
										$scope.loadingData = true;
										
										wpsPropertiesService.getCapabilities(this.capabilitiesCallback);
									}
								};
								
								this.changeWpsUrl = function(){
									/*									
									 * reset capabilities divs
									 */
									wpsFormControlService.capabilitiesSuccess_classAttribute = 'hidden';
									wpsFormControlService.capabilitiesFailed_classAttribute = 'hidden';
									
									/*
									 * disable all tabs, will be enabled on capabilities response
									 */
									wpsPropertiesService.resetProcessDescription();
									wpsFormControlService.disableTabs();
									wpsFormControlService.resetTabContents();
									
									$scope.loadingData = true;
									
									wpsPropertiesService.initializeWpsLibrary();
									wpsPropertiesService.getCapabilities(this.capabilitiesCallback);
									
									this.isRemoveButtonDisabled = false;
									wpsFormControlService.removeWpsServiceButton_classAttribute = 'enabled';
								};
								
								this.removeSelectedWps = function(){
									wpsFormControlService.capabilitiesSuccess_classAttribute = 'hidden';
									wpsFormControlService.capabilitiesFailed_classAttribute = 'hidden';
									
									wpsPropertiesService.removeWpsServiceUrl();
									wpsFormControlService.disableTabs();
									
									this.isRemoveButtonDisabled = true;
									wpsFormControlService.removeWpsServiceButton_classAttribute = 'disabled';
								};
								
								this.capabilitiesCallback = function(capabilitiesResponse){
									
									$scope.loadingData = false;
									
									/*
									 * check received capObject for reasonable structure.
									 */
									if(capabilitiesResponse.capabilities){
										/*
										 * re-call wpsPropertiesService to actually modify it's capabilities object
										 */
										var capObject = capabilitiesResponse.capabilities;
										wpsPropertiesService.onCapabilitiesChange(capObject);
										
										wpsFormControlService.capabilitiesSuccess_classAttribute = '';
										
										/*
										 * enable tabs vor wps version!
										 */
										wpsFormControlService.enableTabs();
										
										
									}
									else{
										/*
										 * disable all tabs, since there is no valid data
										 */
										wpsFormControlService.disableTabs();
										/*
										 * error occurred!
										 * enable error message
										 */
										wpsFormControlService.capabilitiesFailed_errorThrown = capabilitiesResponse.errorThrown;
										wpsFormControlService.capabilitiesFailed_classAttribute = '';
									}
									
									/*
									 * call $apply manually to modify service references
									 */
									$scope.$apply();
								}
							} ]
				});
angular.module('wpsAddServiceModal', ['wpsProperties']);
angular
		.module('wpsAddServiceModal')
		.component(
				'wpsAddServiceModal',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsSetup/modal/wps-add-service-modal.template.html",

					controller : [
							'wpsPropertiesService',
							function WpsAddServiceModalController(
									wpsPropertiesService) {

								/*
								 * references to wpsPropertiesService and
								 * wpsFormControl instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;

								this.temporaryWpsURL;

								this.addNewWpsServiceButton_classAttribute = 'disabled';
								this.isDisabled = true;
								this.alertMessage_classAttribute = 'hidden';
								this.successMessage_classAttribute = 'hidden';
								this.inputFormColor_classAttribute = '';

								this.tryAddNewWpsServiceUrl = function() {
									/*
									 * check for http: or https: check for
									 * whitespaces check if already in list
									 */
									if (!(this.temporaryWpsURL.indexOf(' ') > -1)
											&& !(this.wpsPropertiesServiceInstance.availableWpsServices
													.indexOf('Sam') > -1)) {

										this.wpsPropertiesServiceInstance
												.addNewWpsServiceUrl(this.temporaryWpsURL);
										this.successMessage_classAttribute = '';

										this.inputFormColor_classAttribute = 'has-success';
										this.addNewWpsServiceButton_classAttribute = 'disabled';
										this.isDisabled = true;

									} else {

										this.inputFormColor_classAttribute = 'has-error';

										this.addNewWpsServiceButton_classAttribute = 'disabled';
										this.isDisabled = true;
										/*
										 * show alert message
										 */
										this.alertMessage_classAttribute = '';
									}
								};

								this.onChangeText = function() {
									this.alertMessage_classAttribute = 'hidden';
									this.successMessage_classAttribute = 'hidden';
									this.inputFormColor_classAttribute = '';

									if ((this.temporaryWpsURL
											.startsWith('http:') || this.temporaryWpsURL
											.startsWith('https:'))) {
										this.isDisabled = false;
										this.addNewWpsServiceButton_classAttribute = 'enabled';

									} else {
										this.isDisabled = true;
										this.addNewWpsServiceButton_classAttribute = 'disabled';

									}
								};
							} ]
				});
angular.module('wpsChangeLanguage', ['pascalprecht.translate']);
angular
		.module('wpsChangeLanguage')
		.component(
				'wpsChangeLanguage',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsSetup/wpsChangeLanguage/wps-change-language.template.html",
					/*
					 * injected with a modules service method that manages
					 * enabled tabs
					 */
					controller : ['$translate', function WpsChangeLanguageController($translate) {
						this.availableLanguages = [ {
							name : 'Deutsch',
							key : 'de'
						}, {
							name : 'English',
							key : 'en'
						} ];
						
						/*
						 * holds the selected language object
						 */
						this.selectedLanguage = '';
						
						this.changeWpsLanguage = function(){
							$translate.use(this.selectedLanguage.key);
						};
					}]
				});
angular.module('wpsCapabilities', ['wpsCapabilitiesServiceIdentification', 'wpsCapabilitiesServiceProvider', 'wpsCapabilitiesServiceOperations']);
angular
		.module('wpsCapabilities')
		.component(
				'wpsCapabilities',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsCapabilities/wps-capabilities.template.html",
					controller : ['wpsPropertiesService', function WpsCapabilitiesController(
							wpsPropertiesService) {

					}
				]});
angular.module('wpsCapabilitiesServiceIdentification', ['wpsProperties']);
angular
		.module('wpsCapabilitiesServiceIdentification')
		.component(
				'wpsCapabilitiesServiceIdentification',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsCapabilities/wpsCapabilitiesServiceIdentification/wps-capabilities-service-identification.template.html",

					controller : [
							'wpsPropertiesService',
							function WpsCapabilitiesServiceIdentificationController(
									wpsPropertiesService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;

							} ]
				});
angular.module('wpsCapabilitiesServiceProvider', ['wpsProperties']);
angular
		.module('wpsCapabilitiesServiceProvider')
		.component(
				'wpsCapabilitiesServiceProvider',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsCapabilities/wpsCapabilitiesServiceProvider/wps-capabilities-service-provider.template.html",

					controller : [
							'wpsPropertiesService',
							function WpsCapabilitiesServiceProviderController(
									wpsPropertiesService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;

							} ]
				});
angular.module('wpsCapabilitiesServiceOperations', ['wpsProperties']);
angular
		.module('wpsCapabilitiesServiceOperations')
		.component(
				'wpsCapabilitiesServiceOperations',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsCapabilities/wpsCapabilitiesServiceOperations/wps-capabilities-service-operations.template.html",

					controller : [
							'wpsPropertiesService',
							function WpsCapabilitiesServiceOperationsController(
									wpsPropertiesService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;

							} ]
				});
angular.module('wpsProcesses', ['wpsProperties', 'wpsFormControl', 'wpsProcessDescription']);
angular
		.module('wpsProcesses')
		.component(
				'wpsProcesses',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wps-processes.template.html",
					controller : ['wpsPropertiesService', 'wpsFormControlService', '$scope', function WpsProcessesController(
							wpsPropertiesService, wpsFormControlService, $scope) {

						this.wpsPropertiesServiceInstance = wpsPropertiesService;
						this.wpsFormControlServiceInstance = wpsFormControlService;
						
						$scope.loadingData = false;
						
						
						this.changeWpsProcess = function(){
							/*									
							 * reset error/success message divs
							 */
							wpsFormControlService.describeProcessSuccess_classAttribute = 'hidden';
							wpsFormControlService.describeProcessFailed_classAttribute = 'hidden';
							
							/*
							 * disable execute tab, in case an error occurs
							 */
							wpsFormControlService.executeTab_classAttribute = 'disabled';
							wpsFormControlService.executeTab_dataToggleAttribute = '';
							
							/*
							 * reset tab contents
							 */
							wpsFormControlService.resetTabContents();
							
							if(this.wpsPropertiesServiceInstance.selectedProcess){
								$scope.loadingData = true;
								wpsPropertiesService.describeProcess(this.describeProcessCallback);
							}							
								
						}
						
						this.describeProcessCallback = function(describeProcessResponse){
							/*
							 * TODO block execute once a process description could not be retrieved?
							 */
							
							$scope.loadingData = false;
							
							/*
							 * check received object for reasonable structure.
							 */
							if(describeProcessResponse.processOffering){
								/*
								 * re-call wpsPropertiesService to actually modify it's processDescription object
								 */
								var processDescrObject = describeProcessResponse.processOffering;
								wpsPropertiesService.onProcessDescriptionChange(processDescrObject);
								
								wpsFormControlService.describeProcessSuccess_classAttribute = '';

								/*
								 * enable execute tab
								 */
								wpsFormControlService.executeTab_classAttribute = 'enabled';
								wpsFormControlService.executeTab_dataToggleAttribute = 'tab';
	
							}
							else{

								// wpsFormControlService.disableTabs();
								/*
								 * error occurred!
								 * enable error message
								 */
								wpsFormControlService.describeProcessFailed_errorThrown = describeProcessResponse.errorThrown;
								wpsFormControlService.describeProcessFailed_classAttribute = '';
							}
							
							/*
							 * call $apply manually to modify service references
							 */
							$scope.$apply();
						};
						
					}]
				});
angular.module('wpsProcessDescription', [ 'wpsProperties',
		'wpsGeneralProcessInformation', 'wpsProcessInputs', 'wpsProcessOutputs' ]);
angular
		.module('wpsProcessDescription')
		.component(
				'wpsProcessDescription',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wps-process-description.template.html",
					controller : ['wpsPropertiesService', function WpsProcessDescriptionController(
							wpsPropertiesService, wpsFormControlService, $scope) {

						this.wpsPropertiesServiceInstance = wpsPropertiesService;
						
						
						this.changeWpsProcess = function(){
							/*									
							 * reset capabilities divs
							 */
							wpsFormControlService.describeProcessSuccess_classAttribute = 'hidden';
							wpsFormControlService.describeProcessFailed_classAttribute = 'hidden';
							
							wpsPropertiesService.describeProcess(this.describeProcessCallback);
						}
						
						this.describeProcessCallback = function(describeProcessResponse){
							/*
							 * TODO block execute once a process description could not be retrieved?
							 */
							
							
							/*
							 * check received object for reasonable structure.
							 */
							if(describeProcessResponse.processOffering){
								/*
								 * re-call wpsPropertiesService to actually modify it's processDescription object
								 */
								var processDescrObject = describeProcessResponse.processOffering;
								wpsPropertiesService.onProcessDescriptionChange(processDescrObject);
								
								wpsFormControlService.describeProcessSuccess_classAttribute = '';
								
								
								//wpsFormControlService.enableTabs();
								
								
							}
							else{

								// wpsFormControlService.disableTabs();
								/*
								 * error occurred!
								 * enable error message
								 */
								wpsFormControlService.describeProcessFailed_errorThrown = describeProcessResponse.errorThrown;
								wpsFormControlService.describeProcessFailed_classAttribute = '';
							}
							
							/*
							 * call $apply manually to modify service references
							 */
							$scope.$apply();
						}
						
					}]
				});
angular.module('wpsGeneralProcessInformation', ['wpsProperties']);
angular
		.module('wpsGeneralProcessInformation')
		.component(
				'wpsGeneralProcessInformation',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsGeneralProcessInformation/wps-general-process-information.template.html",

					controller : [
							'wpsPropertiesService',
							function WpsGeneralProcessInformationController(
									wpsPropertiesService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;

							} ]
				});
angular.module('wpsProcessInputs', [ 'wpsProperties', 'wpsLiteralInputs',
		'wpsComplexInputs', 'wpsBoundingBoxInputs', 'wpsInputOutputFilter' ]);
angular
		.module('wpsProcessInputs')
		.component(
				'wpsProcessInputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessInputs/wps-process-inputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsProcessInputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;

							} ]
				});
angular.module('wpsLiteralInputs', ['wpsProperties', 'wpsInputOutputFilter']);
angular
		.module('wpsLiteralInputs')
		.component(
				'wpsLiteralInputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessInputs/wpsLiteralInputs/wps-literal-inputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsLiteralInputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;


							} ]
				});
angular.module('wpsComplexInputs', ['wpsProperties', 'wpsInputOutputFilter']);
angular
		.module('wpsComplexInputs')
		.component(
				'wpsComplexInputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessInputs/wpsComplexInputs/wps-complex-inputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsComplexInputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;


							} ]
				});
angular.module('wpsBoundingBoxInputs', ['wpsProperties', 'wpsInputOutputFilter']);
angular
		.module('wpsBoundingBoxInputs')
		.component(
				'wpsBoundingBoxInputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessInputs/wpsBoundingBoxInputs/wps-bounding-box-inputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsBoundingBoxInputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;


							} ]
				});
angular.module('wpsProcessOutputs', [ 'wpsProperties', 'wpsLiteralOutputs',
		'wpsComplexOutputs', 'wpsBoundingBoxOutputs', 'wpsInputOutputFilter' ]);
angular
		.module('wpsProcessOutputs')
		.component(
				'wpsProcessOutputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessOutputs/wps-process-outputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsProcessOutputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;

							} ]
				});
angular.module('wpsLiteralOutputs', ['wpsProperties', 'wpsInputOutputFilter']);
angular
		.module('wpsLiteralOutputs')
		.component(
				'wpsLiteralOutputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessOutputs/wpsLiteralOutputs/wps-literal-outputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsLiteralOutputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * references to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;


							} ]
				});
angular.module('wpsComplexOutputs', ['wpsProperties', 'wpsInputOutputFilter']);
angular
		.module('wpsComplexOutputs')
		.component(
				'wpsComplexOutputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessOutputs/wpsComplexOutputs/wps-complex-outputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsComplexOutputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;


							} ]
				});
angular.module('wpsBoundingBoxOutputs', ['wpsProperties', 'wpsInputOutputFilter']);
angular
		.module('wpsBoundingBoxOutputs')
		.component(
				'wpsBoundingBoxOutputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsProcesses/wpsProcessDescription/wpsProcessOutputs/wpsBoundingBoxOutputs/wps-bounding-box-outputs.template.html",

					controller : [
							'wpsPropertiesService', 'wpsInputOutputFilterService',
							function WpsBoundingBoxOutputsController(
									wpsPropertiesService, wpsInputOutputFilterService) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsInputOutputFilterServiceInstance = wpsInputOutputFilterService;


							} ]
				});
angular.module('wpsExecute', [ 'wpsProperties', 'wpsFormControl',
		'wpsExecuteSetupRequest', 'wpsExecuteResultDocumentWps2',
		'wpsExecuteStatusInfoDocumentWps2', 'wpsExecuteResponseDocumentWps1',
		'wpsExecuteRawOutput' ]);
angular
		.module('wpsExecute')
		.component(
				'wpsExecute',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsExecute/wps-execute.template.html",
					/*
					 * injected with a modules service method that manages
					 * enabled tabs
					 */
					controller : [
							'wpsPropertiesService',
							'wpsFormControlService',
							function WpsExecuteController(wpsPropertiesService,
									wpsFormControlService) {
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								this.wpsFormControlServiceInstance = wpsFormControlService;
							} ]
				});
angular.module('wpsExecuteSetupRequest', [ 'wpsProperties', 'wpsFormControl',
		'wpsExecuteSetupInputs', 'wpsExecuteSetupOutputs',
		'wpsExecuteSetupParameters' ]);
angular
		.module('wpsExecuteSetupRequest')
		.component(
				'wpsExecuteSetupRequest',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteSetupRequest/wps-execute-setup-request.template.html",

					controller : [
							'wpsPropertiesService', 'wpsFormControlService', '$scope',
							function WpsExecuteSetupRequestController(
									wpsPropertiesService, wpsFormControlService, $scope) {
								/*
								 * references to wpsPropertiesService and wpsFormControl instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsFormControlServiceInstance = wpsFormControlService;
								
								this.executeFailed_classAttribute = 'hidden';
								this.executeSuccess_classAttribute = 'hidden';
								
								this.executeFailed_errorThrown = '';
								
								$scope.loadingData = false;
								
								this.performExecuteRequest = function() {
									
									this.executeFailed_classAttribute = 'hidden';
									this.executeSuccess_classAttribute = 'hidden';
									
									this.executeFailed_errorThrown = '';
									
									$scope.loadingData = true;
									
									this.wpsPropertiesServiceInstance.execute(this.executeCallback);
									
									this.wpsFormControlServiceInstance.resetTabContents();
								};
								
								this.executeCallback = function(executeResponseObj){
									
									$scope.loadingData = false;
									
									/*
									 * check received capObject for reasonable structure.
									 * 
									 * if successful, it will have a property named 'executeResponse'
									 */
									if(executeResponseObj.executeResponse){
										/*
										 * re-call wpsPropertiesService to actually modify it's capabilities object
										 */
										var executeResponse = executeResponseObj.executeResponse;
										wpsPropertiesService.onExecuteResponseChange(executeResponse);
										
										wpsFormControlService.executeSuccess_classAttribute = '';
										
										/*
										 * TODO enable GetStatus and GetResult Tabs
										 */
										
									}
									else{

										/*
										 * TODO block GetStatus and GetResult tabs
										 */
										
										/*
										 * error occurred!
										 * enable error message
										 */
										wpsFormControlService.executeFailed_errorThrown = executeResponseObj.errorThrown;
										wpsFormControlService.executeFailed_classAttribute = '';
									}
									
									/*
									 * call $apply manually to modify service references
									 */
									$scope.$apply();
								}
							} ]
				});
angular.module('wpsExecuteSetupInputs', ['wpsExecuteInput', 'wpsProperties', 'wpsFormControl', 'wpsGeometricOutput']);
angular
        .module('wpsExecuteSetupInputs')
        .component(
                'wpsExecuteSetupInputs',
                {
                    templateUrl: "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteSetupRequest/wpsExecuteSetupInputs/wps-execute-setup-inputs.template.html",
                    controller: [
                        '$rootScope',
                        '$scope',
                        'wpsExecuteInputService',
                        'wpsPropertiesService',
                        'wpsFormControlService',
                        'wpsMapService',
                        'wpsGeometricOutputService',
                        function WpsExecuteSetupInputsController(
                                $rootScope, $scope,
                                wpsExecuteInputService,
                                wpsPropertiesService,
                                wpsFormControlService, 
                                wpsMapService, 
                                wpsGeometricOutputService) {
                            /*
                             * reference to wpsPropertiesService instances
                             */
                            this.wpsExecuteInputServiceInstance = wpsExecuteInputService;
                            this.wpsPropertiesServiceInstance = wpsPropertiesService;
                            this.wpsFormControlServiceInstance = wpsFormControlService;
                            this.wpsGeometricOutputServiceInstance = wpsGeometricOutputService;
                            this.wpsMapServiceInstance = wpsMapService;
                            
                            // controller layout items;
                            this.formData = {};
                            this.formData.complexDataInput = "drawing"; // start drawing option by default
                            this.formData.bboxDataInput = "drawing"; // start corners option by default
                            this.mimeTypeSelection = "";
                            $scope.geoJsonSelected = false;

                            this.onChangeExecuteInput = function (input) {
                                this.wpsExecuteInputServiceInstance.selectedExecuteInput = input;
                                this.wpsFormControlServiceInstance.isRemoveInputButtonDisabled = true;
                                
                                resetAllInputForms();
                            };

                            this.addLiteralInput = function () {
                                var selectedInput = this.wpsExecuteInputServiceInstance.selectedExecuteInput;
                                this.wpsPropertiesServiceInstance.addLiteralInput(selectedInput);

                                this.wpsExecuteInputServiceInstance.markInputAsConfigured(selectedInput);

                                resetLiteralInputForm();
                            };

                            var resetLiteralInputForm = function () {

                                wpsExecuteInputService.literalInputValue = undefined;
                            };

                            this.addComplexInput = function () {
                                var selectedInput = this.wpsExecuteInputServiceInstance.selectedExecuteInput;
                                this.wpsPropertiesServiceInstance.addComplexInput(selectedInput);

                                this.wpsExecuteInputServiceInstance.markInputAsConfigured(selectedInput);

                                resetComplexInputForm();
                            };

                            var resetComplexInputForm = function () {
                                wpsExecuteInputService.selectedExecuteInputFormat = undefined;
                                wpsExecuteInputService.asReference = false;
                                wpsExecuteInputService.complexPayload = undefined;
                                
                                $scope.geoJsonSelected = false;
                                //disable drawing tools
                                $rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': false});
                                
                                try {
                                	//clear draw layers if available
                                    $rootScope.$broadcast('clear-draw-layers', {});
								} catch (e) {
									console.log(e);
								}
                            };

                            this.addBoundingBoxInput = function () {
                                var selectedInput = this.wpsExecuteInputServiceInstance.selectedExecuteInput;
                                this.wpsPropertiesServiceInstance.addBoundingBoxInput(selectedInput);

                                this.wpsExecuteInputServiceInstance.markInputAsConfigured(selectedInput);

                                resetBoundingBoxInputForm();
                            };

                            var resetBoundingBoxInputForm = function () {
                                wpsExecuteInputService.selectedExecuteInputCrs = undefined;
                                wpsExecuteInputService.bboxLowerCorner = undefined;
                                wpsExecuteInputService.bboxUpperCorner = undefined;
                                
                                //disable drawing tools
                                $rootScope.$broadcast('set-bbox-data-map-input-enabled', {'enabled': false});
                                
                                try {
                                	//clear draw layers if available
                                    $rootScope.$broadcast('clear-draw-layers', {});
								} catch (e) {
									console.log(e);
								}
                            };

                            var resetAllInputForms = function () {
                                resetLiteralInputForm();
                                resetComplexInputForm();
                                resetBoundingBoxInputForm();
                            };
                            
                            /**
        					 * delete a specific overlay for specific input identifier
        					 */
        					$scope.$on('reset-all-input-forms', function (event, args) {
                                console.log("reset-all-input-forms has been called.");

                                resetAllInputForms();
                            });

                            this.onChangeAlreadyDefinedExecuteInput = function () {
                                /*
                                 * user selected an already defined input
                                 * 
                                 * now identify it, show the corresponding form 
                                 * and fill the form elements with the defined values!
                                 */
                                var selectedInput = this.wpsExecuteInputServiceInstance.selectedExecuteInput;

                                var definedInput = this.getDefinedInput(selectedInput, this.wpsPropertiesServiceInstance.executeRequest.inputs);

                                /*
                                 * depending on the type of the definedInput 
                                 * we have to fill in a different form
                                 * 
                                 * type may be "literal", "complex", "bbox" 
                                 * according to InputGenerator-class from wps-js-lib library
                                 */
                                var type = definedInput.type;

                                switch (type) {

                                    case "literal":
                                        this.fillLiteralInputForm(definedInput);
                                        break;

                                    case "complex":
                                        this.fillComplexInputForm(definedInput);
                                        break;

                                    case "bbox":
                                        this.fillBoundingBoxInputForm(definedInput);

                                }

                                /*
                                 * enable removeButton
                                 */
                                this.wpsFormControlServiceInstance.isRemoveInputButtonDisabled = false;
                                
                                /*
                                 * if it is a GeoJSON input, then extract the geometry and place it on map!
                                 */
                                
                                var inputMimeType = definedInput.mimeType;
                                
                                if(wpsGeometricOutputService.isGeoJSON_mimeType(inputMimeType)){
                                	console.log("GeoJSON geometry will be added to leaflet-draw layer");
                                	
                                	var geoJSON_asString = definedInput.complexPayload;
                                	
                                	var geoJSON_asObject = JSON.parse(geoJSON_asString);
                                	$rootScope.$broadcast('add-geometry-to-leaflet-draw-from-geojson-input', {'geoJSON': geoJSON_asObject});
                                }
                            };


                            this.fillLiteralInputForm = function (literalInput) {
                                this.wpsExecuteInputServiceInstance.literalInputValue = literalInput.value;
                            };

                            this.fillBoundingBoxInputForm = function (bboxInput) {
                                this.wpsExecuteInputServiceInstance.selectedExecuteInputCrs = bboxInput.crs;
                                this.wpsExecuteInputServiceInstance.bboxLowerCorner = bboxInput.lowerCorner;
                                this.wpsExecuteInputServiceInstance.bboxUpperCorner = bboxInput.upperCorner;
                            };

                            this.fillComplexInputForm = function (complexInput) {

                                this.wpsExecuteInputServiceInstance.asReference = complexInput.asReference;

                                this.wpsExecuteInputServiceInstance.complexPayload = complexInput.complexPayload;

                                this.wpsExecuteInputServiceInstance.selectedExecuteInputFormat = this.getSelectedExecuteInputFormatcomplexInput(complexInput.mimeType, this.wpsExecuteInputServiceInstance.selectedExecuteInput.complexData.formats);

                                if (this.wpsExecuteInputServiceInstance.selectedExecuteInputFormat.mimeType === 'application/vnd.geo+json'){
                                	$scope.geoJsonSelected = true;
                                	$rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': true});
                                }
                                
                            };

                            this.getSelectedExecuteInputFormatcomplexInput = function (mimeType, formatsList) {
                                var index;

                                for (var i = 0; i < formatsList.length; i++) {
                                    var currentFormat = formatsList[i];

                                    /*
                                     * some element must have the same identifier
                                     */
                                    if (mimeType === currentFormat.mimeType) {
                                        index = i;
                                        break;
                                    }
                                }

                                return formatsList[index];
                            };

                            this.getDefinedInput = function (selectedInput, definedInputsList) {
                                var id = selectedInput.identifier;
                                var index;

                                for (var i = 0; i < definedInputsList.length; i++) {
                                    var currentDefinedInput = definedInputsList[i];

                                    /*
                                     * some element must have the same identifier
                                     */
                                    if (id === currentDefinedInput.identifier) {
                                        index = i;
                                        break;
                                    }
                                }

                                return definedInputsList[index];
                            };

                            this.removeAlreadyDefinedInput = function () {
                                /*
                                 * current input from list of already
                                 * defined inputs as well as from execute
                                 * request object 
                                 * 
                                 * and add it to list of not
                                 * defined inputs
                                 */
                                var currentInput = this.wpsExecuteInputServiceInstance.selectedExecuteInput;

                                this.wpsPropertiesServiceInstance.removeAlreadyExistingInputWithSameIdentifier(currentInput);

                                this.wpsExecuteInputServiceInstance.removeInputFromAlreadyDefinedInputs(currentInput);

                                this.wpsExecuteInputServiceInstance.addInputToUnconfiguredExecuteInputs(currentInput);
                                
                                //remove drawn input layer from map
                                $rootScope.$broadcast('delete-overlay-for-input', {'inputIdentifier': currentInput.identifier});

                                /*
                                 * disable removeButton
                                 */
                                this.wpsFormControlServiceInstance.isRemoveInputButtonDisabled = true;

                                resetAllInputForms();

                                /*
                                 * set selection to undefined as visual feedback (and prevent that the same 
                                 * input view is still shown)
                                 */
                                this.wpsExecuteInputServiceInstance.selectedExecuteInput = undefined;

                            };

                            this.complexInputChanged = function(inputSelection){
                                switch (inputSelection){
                                    case 'reference':
                                        console.log('reference selected');
                                        this.wpsExecuteInputServiceInstance.asReference = true;
                                        this.wpsExecuteInputServiceInstance.asComplex = false;
                                        $rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': false});
                                        break;
                                    case 'file':
                                        this.wpsExecuteInputServiceInstance.asReference = false;
                                        this.wpsExecuteInputServiceInstance.asComplex = false;
                                        $rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': false});
                                        console.log('file selected');
                                        break;
                                    case 'drawing':
                                        this.wpsExecuteInputServiceInstance.asReference = false;
                                        this.wpsExecuteInputServiceInstance.asComplex = false;
                                        $rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': true});
                                        console.log('drawing selected');
                                        // if complexDataInput, enable leaflet plugin.
                                        // if boundingBoxDataInput, enable leaflet bb plugin
                                        break;
                                    case 'complex':
                                        this.wpsExecuteInputServiceInstance.asReference = false;
                                        this.wpsExecuteInputServiceInstance.asComplex = true;
                                        $rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': false});
                                        console.log('complex selected');
                                        break;
                                }
                            };
                            
                            this.onCrsChanged = function(){
                            	/*
                            	 * check if create input via "drawing" on map is selected (which is selected by default)
                            	 * 
                            	 * if so then show input draw tools
                            	 */
                            	if (this.formData.bboxDataInput === "drawing")
                            		$rootScope.$broadcast('set-bbox-data-map-input-enabled', {'enabled': true});
                            };
                            
                            this.bboxInputChanged = function(inputSelection){
                                switch (inputSelection){
                                    case 'drawing':
                                        $rootScope.$broadcast('set-bbox-data-map-input-enabled', {'enabled': true});
                                        console.log('drawing selected');
                                        break;
                                    case 'corners':
                                        $rootScope.$broadcast('set-bbox-data-map-input-enabled', {'enabled': false});
                                        console.log('corners selected');
                                        break;
                                }
                            };
                            
                            this.complexDataOptionSelected = function () {
                                console.log("Format selected:");
                                console.log(this.wpsExecuteInputServiceInstance.selectedExecuteInputFormat);
                                mimeTypeSelection = this.wpsExecuteInputServiceInstance.selectedExecuteInputFormat.mimeType;
                                console.log(mimeTypeSelection);
                                if (mimeTypeSelection === "application/vnd.geo+json") {
                                    console.log("geojson selected.");
                                    $scope.geoJsonSelected = true;
                                    this.formData.complexDataInput = "drawing";
                                    $rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': true});
                                } else {
                                    console.log("no geojson selected.");
                                    $scope.geoJsonSelected = false;
                                    $rootScope.$broadcast('set-complex-data-map-input-enabled', {'enabled': false});
                                }
                            };

                        }]
                });
angular.module('wpsExecuteSetupOutputs', ['wpsExecuteOutput', 'wpsProperties', 'wpsFormControl']);
angular
		.module('wpsExecuteSetupOutputs')
		.component(
				'wpsExecuteSetupOutputs',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteSetupRequest/wpsExecuteSetupOutputs/wps-execute-setup-outputs.template.html",

					controller : [
							'wpsExecuteOutputService', 'wpsPropertiesService', 'wpsFormControlService',
							function WpsExecuteSetupOutputsController(
									wpsExecuteOutputService, wpsPropertiesService, wpsFormControlService) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsExecuteOutputServiceInstance = wpsExecuteOutputService;
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								this.wpsFormControlServiceInstance = wpsFormControlService;

								this.onChangeExecuteOutput = function(output){
									this.wpsExecuteOutputServiceInstance.selectedExecuteOutput = output;
									
									this.wpsFormControlServiceInstance.isRemoveOutputButtonDisabled = true;
								};
								
								this.addLiteralOutput = function(){
									var selectedOutput = this.wpsExecuteOutputServiceInstance.selectedExecuteOutput;
									this.wpsPropertiesServiceInstance.addLiteralOutput(selectedOutput);
								
									this.wpsExecuteOutputServiceInstance.markOutputAsConfigured(selectedOutput);

								};
								
								this.addComplexOutput = function(){
									var selectedOutput = this.wpsExecuteOutputServiceInstance.selectedExecuteOutput;
									this.wpsPropertiesServiceInstance.addComplexOutput(selectedOutput);
								
									this.wpsExecuteOutputServiceInstance.markOutputAsConfigured(selectedOutput);
									
								};
								
								this.addBoundingBoxOutput = function(){
									var selectedOutput = this.wpsExecuteOutputServiceInstance.selectedExecuteOutput;
									this.wpsPropertiesServiceInstance.addBoundingBoxOutput(selectedOutput);
								
									this.wpsExecuteOutputServiceInstance.markOutputAsConfigured(selectedOutput);
								};
								
								this.onChangeAlreadyDefinedExecuteOutput = function(){
									/*
									 * user selected an already defined output
									 * 
									 * now identify it, show the corresponding form 
									 * and fill the form elements with the defined values!
									 */
									var selectedOutput = this.wpsExecuteOutputServiceInstance.selectedExecuteOutput;
									
									var definedOutput = this.getDefinedOutput(selectedOutput, this.wpsPropertiesServiceInstance.executeRequest.outputs);
									
									/*
									 * depending on the type of the definedOutput 
									 * we have to fill in a different form
									 * 
									 * type may be "literal", "complex", "bbox" 
									 * according to OutputGenerator-class from wps-js-lib library
									 */
									var type = definedOutput.type;
									
									switch (type) {
									
									case "literal":
										this.fillLiteralOutputForm(definedOutput);
										break;
									
									case "complex":
										this.fillComplexOutputForm(definedOutput);
										break;
										
									case "bbox":
										this.fillBoundingBoxOutputForm(definedOutput);
									
									}
									
									this.wpsFormControlServiceInstance.isRemoveOutputButtonDisabled = false;
								};
								
								
								this.fillLiteralOutputForm = function(literalOutput){
									/*
									 * for WPS 2.0 output has a property named "transmission"
									 */
									if(literalOutput.transmission)
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = this.getSelectedTransmissionMode(literalOutput.transmission, this.wpsPropertiesServiceInstance.processDescription.outputTransmissionModes);
									
									/*
									 * WPS 1.0 outputs store information as property "asReference"
									 */
									else if (literalOutput.asReference)
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = "reference";
									
									else
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = "value";
								};
								
								this.fillBoundingBoxOutputForm = function(bboxOutput){
									/*
									 * for WPS 2.0 output has a property named "transmission"
									 */
									if(bboxOutput.transmission)
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = this.getSelectedTransmissionMode(bboxOutput.transmission, this.wpsPropertiesServiceInstance.processDescription.outputTransmissionModes);
									
									/*
									 * WPS 1.0 outputs store information as property "asReference"
									 */
									else if (bboxOutput.asReference)
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = "reference";
									
									else
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = "value";
								};
								
								this.fillComplexOutputForm = function(complexOutput){
									
									this.wpsExecuteOutputServiceInstance.selectedExecuteOutputFormat = this.getSelectedExecuteOutputFormat(complexOutput.mimeType, this.wpsExecuteOutputServiceInstance.selectedExecuteOutput.complexData.formats);
									
									/*
									 * for WPS 2.0 output has a property named "transmission"
									 */
									if(complexOutput.transmission)
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = this.getSelectedTransmissionMode(complexOutput.transmission, this.wpsPropertiesServiceInstance.processDescription.outputTransmissionModes);
									
									/*
									 * WPS 1.0 outputs store information as property "asReference"
									 */
									else if (complexOutput.asReference)
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = "reference";
									
									else
										this.wpsExecuteOutputServiceInstance.selectedTransmissionMode = "value";
								};
								
								this.getSelectedExecuteOutputFormat = function(mimeType, formatsList){
									var index;
									
									for(var i=0; i<formatsList.length; i++){
										var currentFormat = formatsList[i];
										
										/*
										 * some element must have the same identifier
										 */
										if(mimeType === currentFormat.mimeType){
											index = i;
											break;
										}		
									}
									
									return formatsList[index];
								};
								
								this.getSelectedTransmissionMode = function(transmissionMode, availableTransmissionModes){
									var index;
									
									for(var i=0; i<availableTransmissionModes.length; i++){
										var currentTransmissionMode = availableTransmissionModes[i];
										
										/*
										 * some element must have the same identifier
										 */
										if(transmissionMode === currentTransmissionMode){
											index = i;
											break;
										}		
									}
									
									return availableTransmissionModes[index];
								};
								
								this.getDefinedOutput = function(selectedOutput, definedOutputsList){
									var id = selectedOutput.identifier;
									var index;
									
									for(var i=0; i<definedOutputsList.length; i++){
										var currentDefinedOutput = definedOutputsList[i];
										
										/*
										 * some element must have the same identifier
										 */
										if(id === currentDefinedOutput.identifier){
											index = i;
											break;
										}		
									}
									
									return definedOutputsList[index];
								};
								
								this.removeAlreadyDefinedOutput = function(){
									/*
									 * current output from list of already
									 * defined outputs as well as from execute
									 * request object 
									 * 
									 * and add it to list of not
									 * defined outputs
									 */
									var currentOutput = this.wpsExecuteOutputServiceInstance.selectedExecuteOutput;
									
									this.wpsPropertiesServiceInstance.removeAlreadyExistingOutputWithSameIdentifier(currentOutput);
									
									this.wpsExecuteOutputServiceInstance.removeOutputFromAlreadyDefinedOutputs(currentOutput);
									
									this.wpsExecuteOutputServiceInstance.addOutputToUnconfiguredExecuteOutputs(currentOutput);
									
									/*
									 * disable removeButton
									 */
									this.wpsFormControlServiceInstance.isRemoveOutputButtonDisabled = true;
									
									//this.resetAllOutputForms();
									
									/*
									 * set selection to undefined as visual feedback (and prevent that the same 
									 * output view is still shown)
									 */
									this.wpsExecuteOutputServiceInstance.selectedExecuteOutput = undefined;
									
								}

							} ]
				});
angular.module('wpsExecuteSetupParameters', ['wpsProperties']);
angular
        .module('wpsExecuteSetupParameters')
        .component(
                'wpsExecuteSetupParameters',
                {
                    templateUrl: "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteSetupRequest/wpsExecuteSetupParameters/wps-execute-setup-parameters.template.html",
                    controller: [
                        'wpsPropertiesService',
                        function WpsExecuteSetupParametersController(
                                wpsPropertiesService) {
                            /*
                             * reference to wpsPropertiesService instances
                             */
                            this.wpsPropertiesServiceInstance = wpsPropertiesService;

                        }]
                });
angular.module('wpsExecuteResultDocumentWps2', [ 'wpsProperties']);
angular
		.module('wpsExecuteResultDocumentWps2')
		.component(
				'wpsExecuteResultDocumentWps2',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteResultDocument_WPS_2_0/wps-execute-result-document-wps-2-0.template.html",

					controller : [
							'wpsPropertiesService', 'wpsFormControlService', 'wpsMapService', 'wpsGeometricOutputService', '$scope', '$http',
							function WpsExecuteResultDocumentWps2Controller(
									wpsPropertiesService, wpsFormControlService, wpsMapService, wpsGeometricOutputService, $scope, $http) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								this.wpsFormControlServiceInstance = wpsFormControlService;
								this.wpsMapServiceInstance = wpsMapService;
								this.wpsGeometricOutputServiceInstance = wpsGeometricOutputService;
								
								this.isGeometricFormat = function(output){
									// delegate to wpsGeometricOutputService 
									return this.wpsGeometricOutputServiceInstance.isGeometricFormat(output);
								};
								
								this.fetchAndVisualizeReferenceOutput = function(referenceOutput){
									
									var url = referenceOutput.reference.href;
									
									if (this.wpsGeometricOutputServiceInstance.isGeoJSON(referenceOutput)){
										$http({
											  method: 'GET',
											  url: url
											}).then(function successCallback(response) {
											    // this callback will be called asynchronously
											    // when the response is available
												
												wpsFormControlService.fetchingReferenceOutputSuccess = true;
												wpsFormControlService.fetchingReferenceOutputFailed = false;
												
												/*
												 * make output a complexOutput
												 * and store the retrieved GeoJSON value 
												 */
												
												if(response.data){
													referenceOutput.data = {};
													referenceOutput.data.complexData = {};
													referenceOutput.data.complexData.value = response.data;
													
													
													wpsMapService.addComplexOutputToMap(referenceOutput, wpsMapService.generateUniqueOutputLayerPropertyName());	
												}

											  }, function errorCallback(response) {
											    // called asynchronously if an error occurs
											    // or server returns response with an error status.
												  wpsFormControlService.fetchingReferenceOutputSuccess = false;
												  wpsFormControlService.fetchingReferenceOutputFailed = true;
											  });
									}
									else{
										/*
										 * TODO transform to GeoJSON 
										 * 
										 */
									}

								};

							} ]
				});
angular.module('wpsExecuteStatusInfoDocumentWps2', [ 'wpsProperties']);
angular
		.module('wpsExecuteStatusInfoDocumentWps2')
		.component(
				'wpsExecuteStatusInfoDocumentWps2',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteStatusInfoDocument_WPS_2_0/wps-execute-status-info-document-wps-2-0.template.html",

					controller : [
							'wpsPropertiesService', '$scope',
							function WpsExecuteStatusInfoDocumentWps2Controller(
									wpsPropertiesService, $scope) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;

								this.refreshStatus = function() {
									/*
									 * TODO get job-id from current statusInfo
									 * document and trigger getStatusRequest!
									 */
									var jobId = this.wpsPropertiesServiceInstance.statusInfoDocument_wps_2_0.jobId;

									this.wpsPropertiesServiceInstance
											.getStatus(
													this.onRefreshedStatusInfoDocument,
													jobId);
								};

								this.onRefreshedStatusInfoDocument = function(
										wpsResponse) {
									/*
									 * check response for reasonable content
									 */
									if (wpsResponse.executeResponse)
										wpsPropertiesService
												.onExecuteResponseChange(wpsResponse.executeResponse);

									/**
									 * TODO error/success messages?
									 */
									
									$scope.$apply();
								};
								
								this.getResult = function(){
									/*
									 * TODO get job-id from current statusInfo
									 * document and trigger getResultRequest!
									 */
									var jobId = this.wpsPropertiesServiceInstance.statusInfoDocument_wps_2_0.jobId;

									this.wpsPropertiesServiceInstance
											.getResult(
													this.getResultCallback,
													jobId);
								};
								
								this.getResultCallback = function(
										wpsResponse) {
									/*
									 * check response for reasonable content
									 */
									if (wpsResponse.executeResponse)
										wpsPropertiesService
												.onExecuteResponseChange(wpsResponse.executeResponse);

									/**
									 * TODO error/success messages?
									 */
									
									$scope.$apply();
								};

							} ]
				});
angular.module('wpsExecuteResponseDocumentWps1', [ 'wpsProperties']);
angular
		.module('wpsExecuteResponseDocumentWps1')
		.component(
				'wpsExecuteResponseDocumentWps1',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteResponseDocument_WPS_1_0/wps-execute-response-document-wps-1-0.template.html",

					controller : [
							'wpsPropertiesService', 'wpsFormControlService', 'wpsMapService', 'wpsGeometricOutputService', '$scope', '$http',
							function WpsExecuteResponseDocumentWps1Controller(
									wpsPropertiesService, wpsFormControlService, wpsMapService, wpsGeometricOutputService, $scope, $http) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								this.wpsFormControlServiceInstance = wpsFormControlService;
								this.wpsMapServiceInstance = wpsMapService;
								this.wpsGeometricOutputServiceInstance = wpsGeometricOutputService;
								
								this.refreshStatus = function() {
									/*
									 * TODO get statusLocation value from current response 
									 * document and trigger !
									 */
									var documentLocation = this.wpsPropertiesServiceInstance.responseDocument_wps_1_0.statusLocation;

									this.wpsPropertiesServiceInstance
											.fetchUpdatedResponseDocument_wps_1_0(
													this.onRefreshedResponseDocument, documentLocation);
								};

								this.onRefreshedResponseDocument = function(
										wpsResponse) {
									/*
									 * check response for reasonable content
									 */
									if (wpsResponse.executeResponse)
										wpsPropertiesService
												.onExecuteResponseChange(wpsResponse.executeResponse);

									/**
									 * TODO error/success messages?
									 */
									
									$scope.$apply();
								};
								
								this.isGeometricFormat = function(output){
									// delegate to wpsGeometricOutputService 
									return this.wpsGeometricOutputServiceInstance.isGeometricFormat(output);
								};
								
								this.fetchAndVisualizeReferenceOutput = function(referenceOutput){
									
									var url = referenceOutput.reference.href;
									
									if (this.wpsGeometricOutputServiceInstance.isGeoJSON(referenceOutput)){
										$http({
											  method: 'GET',
											  url: url
											}).then(function successCallback(response) {
											    // this callback will be called asynchronously
											    // when the response is available
												
												wpsFormControlService.fetchingReferenceOutputFailed = false;
												wpsFormControlService.fetchingReferenceOutputSuccess = true;
												
												/*
												 * make output a complexOutput
												 * and store the retrieved GeoJSON value 
												 */
												
												if(response.data){
													referenceOutput.data = {};
													referenceOutput.data.complexData = {};
													referenceOutput.data.complexData.value = response.data;
													
													
													wpsMapService.addComplexOutputToMap(referenceOutput, wpsMapService.generateUniqueOutputLayerPropertyName());	
												}

											  }, function errorCallback(response) {
											    // called asynchronously if an error occurs
											    // or server returns response with an error status.
												  wpsFormControlService.fetchingReferenceOutputSuccess = false;
												  wpsFormControlService.fetchingReferenceOutputFailed = true;
											  });
									}
									else{
										/*
										 * TODO transform to GeoJSON 
										 * 
										 */
									}

								};

							} ]
				});
angular.module('wpsExecuteRawOutput', [ 'wpsProperties',
		'wpsFormControl' ]);
angular
		.module('wpsExecuteRawOutput')
		.component(
				'wpsExecuteRawOutput',
				{
					templateUrl : "components/wpsUserInterface/wpsControls/wpsExecute/wpsExecuteRawOutput/wps-execute-raw-output.template.html",

					controller : [
							'wpsPropertiesService', 'wpsFormControlService',
							function WpsExecuteRawOutputController(
									wpsPropertiesService, wpsFormControlService) {
								/*
								 * reference to wpsPropertiesService instances
								 */
								this.wpsPropertiesServiceInstance = wpsPropertiesService;
								
								this.wpsFormControlServiceInstance = wpsFormControlService;

							} ]
				});
angular.module('wpsMap', []);
/**
 * a common serviceInstance that holds all needed properties and methods for
 * interacting with a map (openlayers).
 */
angular.module('wpsMap').service(
		'wpsMapService',
		[ 'leafletData', '$rootScope', '$http', function(leafletData, $rootScope, $http) {
		
					this.visualizeGeometricOutputs = function(geometricOutputs){
						
						for (var i=0; i < geometricOutputs.length; i++){
							
							var currentNameForLayerProperty = this.generateUniqueOutputLayerPropertyName();
							
							/*
							 * if output is a "reference" output, then we will not display
							 * it on the map --> hence continue!
							 */
							if(geometricOutputs[i].reference)
								continue;
							else							
								this.addGeometricOutputToMap(geometricOutputs[i], currentNameForLayerProperty);
						}
					};
					
					this.generateUniqueOutputLayerPropertyName = function(){
						
						/*
						 * basic name for layer property
						 * 
						 * important if we add multiple layers as overlays,
						 * since each layer has to be defined as unique property
						 */
						var baseNameForLayerProperty = 'Output';
						var min = 1;
						var max = 1000;
						
						var randomNameExtension = (Math.random() * (max - min)) + min;
						
						return baseNameForLayerProperty + '_' + randomNameExtension;
					};
					
					this.generateUniqueInputLayerPropertyName = function(inputIdentifier){
						
						/*
						 * basic name for layer property
						 * 
						 * important if we add multiple layers as overlays,
						 * since each layer has to be defined as unique property
						 */
						var baseNameForLayerProperty = 'Input_' + inputIdentifier;
						
						return baseNameForLayerProperty;
						
//						var min = 1;
//						var max = 1000;
//						
//						var randomNameExtension = (Math.random() * (max - min)) + min;
//						
//						return baseNameForLayerProperty + '_' + randomNameExtension;
					};
					
					this.addGeometricOutputToMap = function(geometricOutput, currentNameForLayerProperty){
						/*
						 * output may be complex output or bbox output
						 */
						
						if(geometricOutput.data.complexData)
							this.addComplexOutputToMap(geometricOutput, currentNameForLayerProperty);
						
						else if(geometricOutput.data.boundingBoxData)
							this.addBboxOutputToMap(geometricOutput, currentNameForLayerProperty);
						
						else
							// should never reach this line; error
							return null;
					};
					
					this.addComplexOutputToMap = function(complexOutput, currentNameForLayerProperty){
						/*
						 * format will be GeoJSON or a WMS. Hence we must inspect its format/mimeType.
						 */
						
						var format = complexOutput.data.complexData.mimeType;
						
						if(format === 'application/WMS'){
							/*
							 * it is a WMS data!
							 */
							this.processAsWMS(complexOutput, currentNameForLayerProperty);
						}
						else{
							/*
							 * it is GeoJSON data!
							 */
							this.processAsGeoJSON(complexOutput, currentNameForLayerProperty);
						}
					};
					
					this.processAsGeoJSON = function(complexOutput, currentNameForLayerProperty){
						var geoJSONValue = complexOutput.data.complexData.value;
						
						/*
						 * geoJSONValue can either be a String or directly a GeoJSON object!
						 * 
						 * if it it a GeoJSON object, it should have a "type" property!
						 */
						var geoJSONFeature;
						if(geoJSONValue.type){
							/*
							 * is already GeoJSON object!
							 */
							geoJSONFeature = geoJSONValue;
						}
						else{
							/*
							 * it is a String, so we parse it as JSON
							 */
							geoJSONFeature = JSON.parse(geoJSONValue);
						} 
						
						var outputIdentifier = complexOutput.identifier;
						
						/*
						 * calls the associated event/method from wps-map controller!
						 */
						$rootScope.$broadcast("addGeoJSONOutput", 
								{ geoJSONFeature: geoJSONFeature,
								  layerPropertyName: currentNameForLayerProperty,
								  outputIdentifier: outputIdentifier});
					};
					
					this.processAsWMS = function(complexOutput, currentNameForLayerProperty){
						
						/*
						 * the WMS URL is currently included as follows:
						 * 
						 * <wps:ComplexData mimeType="application/WMS"><![CDATA[http://geoprocessing.demo.52north.org:8080/geoserver/wms?Service=WMS&Request=GetMap&Version=1.1.1&layers=N52:primary3416203586858505312.tif_b5f8f00c-903c-477d-876b-86bfe1fe8788&width=252&height=185&format=image/png&bbox=385735.0,5666656.0,386214.0,5667008.0&srs=EPSG:25832]]></wps:ComplexData>
						 */
						var wmsURL_complexDataValue = complexOutput.data.complexData.value;
						
						var wmsURL_withQueryParameters = extract_WMS_URL(wmsURL_complexDataValue);
						
						/*
						 * comma-separated String of layerNames listed in WMS Capabilities
						 * 
						 * retrieved from query parameter within wmsURL:
						 * Example: "layers=N52:primary3416203586858505312.tif_b5f8f00c-903c-477d-876b-86bfe1fe8788"
						 */
						var layerNamesString = getUrlQueryParameterValueByName("layers", wmsURL_withQueryParameters);
						console.log("layers query parameter: " + layerNamesString);
						
						var outputIdentifier = complexOutput.identifier;
						
						//remove all query parameters to obtain base WMS URL
						var wmsBaseURL = wmsURL_withQueryParameters.split("?")[0];
						//re-append "?" as required by Leaflet
						wmsBaseURL = wmsBaseURL + "?";
						
						/*
						 * TODO retrieve Capabilities of WMS and get layer names
						 * 
						 * TODO maybe there is a JS library that can help us here!
						 * 
						 * TODO simply use all layers???
						 * 
						 * TODO or similar to reference outputs: Change result forms to show available
						 * WMS Layers and let user choose the layer he/she intends to visualize! 
						 */
						
						/*
						 * calls the associated event/method from wps-map controller!
						 */
						$rootScope.$broadcast("addWMSOutput", 
								{ wmsURL: wmsBaseURL,
								  layerNamesString: layerNamesString,
								  layerPropertyName: currentNameForLayerProperty,
								  outputIdentifier: outputIdentifier});
					};
					
					var extract_WMS_URL = function(wmsURL_complexDataValue){
						
						/*
						 * WMS URL value might look like this:
						 * "<![CDATA[http://geoprocessing.demo.52north.org:8080/geoserver/wms?Service=WMS&Request=GetMap&Version=1.1.1&layers=N52:primary3416203586858505312.tif_b5f8f00c-903c-477d-876b-86bfe1fe8788&width=252&height=185&format=image/png&bbox=385735.0,5666656.0,386214.0,5667008.0&srs=EPSG:25832]]>"
						 * 
						 * Hence we must remove the leading "![CDATA[" and trailing "]]"
						 */
						
						var leadingCdataString = "<![CDATA[";
						var trailingBracketsString = "]]>";
						
						console.log(wmsURL_complexDataValue);
						
						if (wmsURL_complexDataValue.startsWith(leadingCdataString)){
							/*
							 * remove leading and trailing sections
							 */
							var wmsTargetURL = wmsURL_complexDataValue.replace(leadingCdataString, "");
							
							wmsTargetURL = wmsTargetURL.replace(trailingBracketsString, "");
							
							console.log(wmsTargetURL);
							
							return wmsTargetURL;
						}
						else if (wmsURL_complexDataValue.startsWith("http")){
							/*
							 * seems to be the target URL already, no extraction needed
							 */
							return wmsURL_complexDataValue;
						}
						else{
							/*
							 * TODO can this line be reached? Then, how to handle correctly?
							 */
							return wmsURL_complexDataValue;
						}
						
					};
					
					function getUrlQueryParameterValueByName(parameterName, url) {
						
						parameterName = parameterName.replace(/[\[\]]/g, "\\$&");
					    var regex = new RegExp("[?&]" + parameterName + "(=([^&#]*)|&|#|$)"),
					        resultsArray = regex.exec(url);
					    if (!resultsArray) 
					    	return null;
					    if (!resultsArray[2]) 
					    	return '';
					    
					    return decodeURIComponent(resultsArray[2].replace(/\+/g, " "));
					}
					
					this.addBboxOutputToMap = function(bboxOutput, currentNameForLayerProperty){
						
						/*
						 * TODO check CRS --> GeoJSON requires WGS84
						 * project if necessary!
						 */
						
						var outputIdentifier = bboxOutput.identifier;
						
						var description = bboxOutput.abstractValue || bboxOutput.title;
						
						var crs = bboxOutput.data.boundingBoxData.crs;
						
						/*
						 * TODO check CRS --> WGS84 should be used for GeoJSON
						 */
						
						var lowerCorner = bboxOutput.data.boundingBoxData.lowerCorner;
						var upperCorner = bboxOutput.data.boundingBoxData.upperCorner;
						
						var coordinatesArray = this.createCoordinatesArrayFromBbox(lowerCorner, upperCorner);
						
						var geoJSONFeature = {
							    "type": "Feature",
							    "properties": {"popupContent": description},
							    "geometry": {
							        "type": "Polygon",
							        "coordinates": coordinatesArray
							    }
							};
						
						/*
						 * calls the associated event/method from wps-map controller!
						 */
						$rootScope.$broadcast("addGeoJSONOutput", 
								{ geoJSONFeature: geoJSONFeature,
								  layerPropertyName: currentNameForLayerProperty,
								  outputIdentifier: outputIdentifier});
					};
					
					/**
					 * Creates a Coordinates Array for GeoJSON feature representing the 
					 * Bounding Box
					 */
					this.createCoordinatesArrayFromBbox = function(lowerCorner, upperCorner){
						
						var lat_lowerLeft = parseFloat(lowerCorner.split(" ")[0]);
						var lat_upperRight = parseFloat(upperCorner.split(" ")[0]);
						var lon_lowerLeft = parseFloat(lowerCorner.split(" ")[1]);
						var lon_upperRight = parseFloat(upperCorner.split(" ")[1]);
						
						var coordinatesArray = [[
    										     [lon_lowerLeft, lat_lowerLeft],
     										     [lon_lowerLeft, lat_upperRight],
 										         [lon_upperRight, lat_upperRight],
 										         [lon_upperRight, lat_lowerLeft],
 										         [lon_lowerLeft, lat_lowerLeft]
    										    ]];
						
						return coordinatesArray;
					};
			
		}]);
angular.module('wpsMap').component(
        'wpsMap',
        {
            templateUrl: "components/wpsUserInterface/wpsMap/wps-map.template.html",
            controller: [
                '$rootScope',
                '$scope',
                '$timeout',
                'wpsMapService',
                'wpsExecuteInputService',
                'leafletData',
                function MapController($rootScope, $scope, $timeout, wpsMapService, wpsExecuteInputService, leafletData) {

                    this.wpsMapServiceInstance = wpsMapService;
                    this.wpsExecuteSetupInputs = wpsExecuteInputService;
                    $scope.inputLayerCounter = 0;

                    $scope.drawnItems = new L.FeatureGroup();
                    $scope.drawControl;

                    $scope.allDrawingToolsEnabled = false;

                    // add an input layer to the map:
                    $scope.$on('add-input-layer', function (event, args) {
                        console.log("add-input-layer has been called.");
                        var geojson = JSON.parse(args.geojson);
                        // TODO: error no json format feedback to user
                        $scope.addInputLayer(geojson, args.name, args.layerPropertyName);
                        // TODO: error json no geojson format feedback to user
                    });

                    // set leaflet plugins for complex data input enabled:
                    $scope.$on('set-complex-data-map-input-enabled', function (event, args) {
                        console.log("set-complex-data-map-input-enabled has been called.");
                        console.log(args);
                        // do something on this certain event, e.g.: add input layer:

                        // get params of broadcast:
                        $scope.allDrawingToolsEnabled = args.enabled;

                        if ($scope.allDrawingToolsEnabled) {
                            // enable
                            $scope.setDrawEnabled_complex(true);
                        } else {
                            // disable
                            $scope.setDrawEnabled_complex(false);
                        }
                    });
                    
                 // set leaflet plugins for bbox data input enabled:
                    $scope.$on('set-bbox-data-map-input-enabled', function (event, args) {
                        console.log("set-bbox-data-map-input-enabled has been called.");
                        console.log(args);

                        // get params of broadcast:
                        $scope.allDrawingToolsEnabled = args.enabled;

                        if ($scope.allDrawingToolsEnabled) {
                            // enable
                            $scope.setDrawEnabled_bbox(true);
                        } else {
                            // disable
                            $scope.setDrawEnabled_bbox(false);
                        }
                    });

                    angular.extend($scope, {
                        center: {
                            lat: 51.95,
                            lng: 7.63,
                            zoom: 13
                        },
                        layers: {
                            baselayers: {
                                osm: {
                                    name: 'OpenStreetMap',
                                    url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                                    type: 'xyz'
                                }
                            },
                            overlays: {
                            }
                        },
                        controls: {
                        }
                    });
                    
                    /**
					 * Resets the map, which includes:
					 *  - deletion of all overlays
					 */
					var resetMap = function(){
						deleteAllOverlays();
					};
					
					var deleteAllOverlays = function(){
						$scope.layers.overlays = {};
					};
					
					/**
					 * delete overlay for given input identifier
					 */
					$scope.$on('delete-overlay-for-input', function (event, args) {
                        console.log("delete-overlay-for-input has been called.");
                        
                        var inputIdentifier = args.inputIdentifier;
                        
                        var inputLayerPropertyName = wpsMapService.generateUniqueInputLayerPropertyName(inputIdentifier)

                        delete $scope.layers.overlays[inputLayerPropertyName];
                        
                        console.log($scope.layers.overlays);
                        
                    });
					
					/**
					 * remove all overlays from map
					 */
					$scope.$on('reset-map-overlays', function (event, args) {
                        console.log("reset-map-overlays has been called.");

                        resetMap();
                        
                    });
					
					/**
					 * clear all layers of leaflet-draw layer
					 */
					$scope.$on('clear-draw-layers', function (event, args) {
                        console.log("clear-draw-layers has been called.");

                        $scope.drawnItems.clearLayers();
                        
                    });

					/**
					 * delete a specific overlay for specific input identifier
					 */
					$scope.$on('add-geometry-to-leaflet-draw-from-geojson-input', function (event, args) {
                        console.log("add-geometry-to-leaflet-draw-from-geojson-input has been called.");
                        console.log(args);

                        var geoJSON_asObject = args.geoJSON;
                        
                        L.geoJson(geoJSON_asObject, {
                        	  onEachFeature: function (feature, layer) {
                        	    if (layer.getLayers) {
                        	      layer.getLayers().forEach(function (currentLayer) {
                        	        $scope.drawnItems.addLayer(currentLayer);
                        	      })
                        	    } else {
                        	    	$scope.drawnItems.addLayer(layer);
                        	    }
                        	  },
                        	  style: {
                                  color: '#f06eaa',
                                  fillColor: null,
                                  weight: 4.0,
                                  opacity: 0.5,
                                  fillOpacity: 0.2
                        	  }
                        	});
                        
                    });
					
					/**
					 * clear all layers of leaflet-draw layer
					 */
					$scope.$on('clear-draw-layers', function (event, args) {
                        console.log("clear-draw-layers has been called.");

                        $scope.drawnItems.clearLayers();
                        
                    });
                    
                    var customResetMapControl = L.Control.extend({
                     
                      options: {
                        position: 'topright' 
                        //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
                      },
                     
                      onAdd: function (map) {
                    	  var container = L.DomUtil.create('input', 'leaflet-bar leaflet-control leaflet-control-custom');
                    	  
                    	  	container.type = 'button';
                    	  	container.title = 'Reset Layers';
                    	  	container.value = 'Reset Layers';
                    	    container.style.backgroundColor = 'white';
                    	    container.style.width = '90px';
                    	    container.style.height = '25px';
                    	    
                    	    container.onmouseover = function(){
                    	    	  container.style.backgroundColor = '#F4F4F4'; 
                    	    	}
                    	    	container.onmouseout = function(){
                    	    	  container.style.backgroundColor = 'white'; 
                    	    	}
                    	 
                    	    container.onclick = function(){
                    	      resetMap();
                    	    }
                    	    return container;
                      },
                     
                    });

                    // called, when the map has loaded:
                    leafletData.getMap().then(function (map) {
                        // create draw layers Control:
                        $scope.drawnItems = new L.featureGroup().addTo(map);
//                        $scope.drawControl = new L.Control.Draw({
//                            position: "bottomright",
//                            edit: {
//                                featureGroup: $scope.drawnItems
//                            }
//                        });
//
//                        // called, when a single geojson feature is created via leaflet.draw:
//                        map.on('draw:created', function (e) {
//                            var layer = e.layer;
//                            $scope.drawnItems.addLayer(layer);
//                            console.log(JSON.stringify($scope.drawnItems.toGeoJSON()));
//                        });

                        
                        // add resetMap button
                        map.addControl(new customResetMapControl());

//                        // add drawItems-layer to mapcontrols and enable 'edit'-feature on it:
//                        map.addControl(new L.Control.Draw({
//                            position: "bottomright",
//                            edit: {featureGroup: drawnItems}
//                        }));

                        // drawControl.addTo(map);

                        // add drawControls to map:
//                        $scope.setDrawEnabled_complex(false);

                        console.log(map);

                    });

                    $scope.drawctrlEnabled = true;
                    
                    /**
                     * enables/disables the Leaflet-Draw tools
                     * @param {type} enabled - true to enable the draw controls/ false to disable the draw controls
                     * @returns {undefined}
                     */
                    $scope.setDrawEnabled_complex = function (enabled) {

                        leafletData.getMap().then(function (map) {
                            
                            console.log(map);

                            if (enabled) {
                            	
	                                $scope.drawControl = new L.Control.Draw({
	                                    position: "bottomright",
	                                    edit: {
	                                        featureGroup: $scope.drawnItems
	                                    }
	                                });

                                // called, when a single geojson feature is created via leaflet.draw:
                                map.on('draw:created', function (e) {
                                    var layer = e.layer;
                                    $scope.drawnItems.addLayer(layer);
                                    console.log(JSON.stringify($scope.drawnItems.toGeoJSON()));
                                    // update geojson-selection in service:
                                    wpsExecuteInputService.complexPayload = JSON.stringify($scope.drawnItems.toGeoJSON());
                                });
                                
                             // called, when a single geojson feature is created via leaflet.draw:
                                map.on('draw:edited', function (e) {
                                    var layer = e.layer;
//                                    $scope.drawnItems.addLayer(layer);
                                    console.log(JSON.stringify($scope.drawnItems.toGeoJSON()));
                                    // update geojson-selection in service:
                                    wpsExecuteInputService.complexPayload = JSON.stringify($scope.drawnItems.toGeoJSON());
                                });

                                // called, when a single geojson feature is created via leaflet.draw:
                                map.on('draw:deleted', function (e) {
                                    var layer = e.layer;
                                    //drawnItems.addLayer(layer);
                                    console.log(JSON.stringify($scope.drawnItems.toGeoJSON()));
                                    // update geojson-selection in service:
                                    wpsExecuteInputService.complexPayload = JSON.stringify($scope.drawnItems.toGeoJSON());
                                });

                                // add drawItems-layer to mapcontrols and enable 'edit'-feature on it:
                                //drawControl.addTo(map);
                                map.addControl($scope.drawControl);
                                $scope.allDrawingToolsEnabled = true;
                            } else {
                                console.log(map);
                                
                                try {
                                	map.removeControl($scope.drawControl);
								} catch (e) {
									console.log(e);
								}
                                
                            }
                        });

                    };

                    /**
                     * enables/disables the Leaflet-Draw tools for BoundingBoxInputs
                     * @param {type} enabled - true to enable the draw controls/ false to disable the draw controls
                     * @returns {undefined}
                     */
                    $scope.setDrawEnabled_bbox = function (enabled) {

                        leafletData.getMap().then(function (map) {
                            
                            console.log(map);

                            if (enabled) {

                            		$scope.drawControl = new L.Control.Draw({
	                                    position: "bottomright",
	                                    draw:{
	                                    	polygon: false,
	                                    	marker: false,
	                                    	circle:false,
	                                    	polyline: false
	                                    },
	                                    edit: {
	                                        featureGroup: $scope.drawnItems
	                                    }
	                                });

                                // called, when a single geojson feature is created via leaflet.draw:
                                map.on('draw:created', function (e) {
                                    var layer = e.layer;
                                    $scope.drawnItems.addLayer(layer);
                                    
                                    var geoJson_bbox = $scope.drawnItems.toGeoJSON();
                                    
                                    console.log(JSON.stringify(geoJson_bbox));
                                    // update geojson-selection in service:
                                    
                                    wpsExecuteInputService.bboxAsGeoJSON = geoJson_bbox;
                                    
                                    var corners = extractBboxCornersFromGeoJSON(geoJson_bbox);
                                    
                                    wpsExecuteInputService.bboxLowerCorner = corners.lowerCorner;
                                    wpsExecuteInputService.bboxUpperCorner = corners.upperCorner;
                                });
                                
                             // called, when a single geojson feature is created via leaflet.draw:
                                map.on('draw:edited', function (e) {
                                    var layer = e.layer;
                                    var geoJson_bbox = $scope.drawnItems.toGeoJSON();
                                    
                                    console.log(JSON.stringify(geoJson_bbox));
                                    
                                    wpsExecuteInputService.bboxAsGeoJSON = geoJson_bbox;
                                    
                                    var corners = extractBboxCornersFromGeoJSON(geoJson_bbox);
                                    
                                    wpsExecuteInputService.bboxLowerCorner = corners.lowerCorner;
                                    wpsExecuteInputService.bboxUpperCorner = corners.upperCorner;
                                });

                                // called, when a single geojson feature is created via leaflet.draw:
                                map.on('draw:deleted', function (e) {
                                    var layer = e.layer;
                                    var geoJson_bbox = $scope.drawnItems.toGeoJSON();
                                    
                                    console.log(JSON.stringify(geoJson_bbox));
                                    
                                    wpsExecuteInputService.bboxAsGeoJSON = geoJson_bbox;

                                    var corners = extractBboxCornersFromGeoJSON(geoJson_bbox);
                                    
                                    wpsExecuteInputService.bboxLowerCorner = corners.lowerCorner;
                                    wpsExecuteInputService.bboxUpperCorner = corners.upperCorner;
                                });

                                // add drawItems-layer to mapcontrols and enable 'edit'-feature on it:
                                map.addControl($scope.drawControl);
                                $scope.allDrawingToolsEnabled = true;
                            } else {
                                console.log(map);
                                
                                try {
                                	map.removeControl($scope.drawControl);
								} catch (e) {
									console.log(e);
								}
                                
                            }
                        });

                    };
                    
                    var extractBboxCornersFromGeoJSON = function(geoJson_bbox){
                    	
                    	var corners = {};
                    	
                    	var lonMin;
                    	var lonMax;
                    	var latMin;
                    	var latMax;
                    	
                    	/*
                    	 * BBOX is encoded as GeoJSON FeatureCollection
                    	 * 
                    	 * hence geometry is available via object.features[0].geometry.coordinates[0]
                    	 */
                    	
                    	var coordinatesArray = geoJson_bbox.features[0].geometry.coordinates;
                    	
                    	/*
                    	 * coordinates array may look like: [[lon,lat],[lon,lat]]
                    	 */
                    	var points = coordinatesArray[0];
                    	
                    	/*
                    	 * initialize variables with first point
                    	 */
                    	var firstPoint = points[0];
                    	lonMax = firstPoint[0];
                    	lonMin = firstPoint[0];
                    	latMax = firstPoint[1];
                    	latMin = firstPoint[1];
                    	
                    	// remaining points
                    	for (var index = 1; index <points.length; index++){
                    		var currentPoint = points[index];
                    		
                    		var currentLat = currentPoint[1];
                    		var currentLon = currentPoint[0];
                    		
                    		if (currentLat > latMax)
                    			latMax = currentLat;
                    		
                    		else if (currentLat < latMin)
                    			latMin = currentLat;
                    		
                    		if (currentLon > lonMax)
                    			lonMax = currentLon;
                    		
                    		else if (currentLon < lonMin)
                    			lonMin = currentLon;
                    	}
                    	
                    	var lowerLeftCornerString = latMin + " " + lonMin;
                    	var upperRightCornerString = latMax + " " + lonMax;
                    	
                    	corners.lowerCorner = lowerLeftCornerString;
                    	corners.upperCorner = upperRightCornerString;
                    	
                    	return corners;
                    };

                    /**
                     * adds a geojson featurecollection as a layer onto the leaflet map
                     * @param {type} geojson
                     * @returns {undefined}
                     */
                    $scope.addInputLayer = function (geojson, identifier, layerPropertyName) {
                        
                        console.log(geojson);
                        
                        if($scope.layers.overlays[layerPropertyName]){
                        	delete $scope.layers.overlays[layerPropertyName];

                        	console.log($scope.layers.overlays);
                        }
                        
                        var geoJSONLayer = {
                        		name: "Input: " + identifier,
                                type: "geoJSONShape",
                                data: geojson,
                                visible:true,
                                layerOptions: {
                                    style: {
                                            color: '#1B4F72',
                                            fillColor: 'blue',
                                            weight: 2.0,
                                            opacity: 0.6,
                                            fillOpacity: 0.2
                                    },
                                    onEachFeature: onEachFeature
                                }
                            };
                        
                        checkPopupContentProperty(geojson, identifier);
                        
                        $scope.layers.overlays[layerPropertyName] = geoJSONLayer;
                        
                        // refresh the layer!!! Otherwise display is not updated properly in case
                        // an existing overlay is updated! 
                        $scope.layers.overlays[layerPropertyName].doRefresh = true;
                    };
                    
                    /*
                     * event/method to add a GeoJSON output to the map 
                     */
                    $scope.$on("addGeoJSONOutput", function(event, args) {
                    	
                        var geoJsonOutput = args.geoJSONFeature;
                        var layerPropertyName = args.layerPropertyName;
                        var outputIdentifier = args.outputIdentifier;
                        
                        checkPopupContentProperty(geoJsonOutput, outputIdentifier);
                        
                        var geoJSONLayer = {
                                name: 'Output: ' + outputIdentifier,
                                type: 'geoJSONShape',
                                data: geoJsonOutput,
                                visible: true,
                                layerOptions: {
                                    style: {
                                            color: '#922B21',
                                            fillColor: 'red',
                                            weight: 2.0,
                                            opacity: 0.6,
                                            fillOpacity: 0.2
                                    },
                                    onEachFeature: onEachFeature
                                }
                            };
                        
                        $scope.layers.overlays[layerPropertyName] = geoJSONLayer;
                        
                        // center map to new output
                        $scope.centerGeoJSONOutput(layerPropertyName);
                        
                    });
                    
//                    var addWMSOutput = function() {
//                    	
//                        var url = 'http://demo.opengeo.org/geoserver/ows?';
//                        var layerPropertyName = 'testWMS';
//                        var outputIdentifier = 'testWMS';
//                        
//                        var wmsLayer = {
//                                name: 'Output: ' + outputIdentifier,
//                                type: 'wms',
//                                visible: true,
//                                url: url,
//                                layerParams: {
//                                	layers: 'ne:ne',
//                                	format: 'image/png',
//                                    transparent: true
//                                }
//                            };
//                        
//                        $scope.layers.overlays[layerPropertyName] = wmsLayer;
//                        
//                        console.log("Test WMS");
//                        
//                    };
                    
                    /*
                     * event/method to add a WMS output to the map 
                     */
                    $scope.$on("addWMSOutput", function(event, args) {
                    	
                        var wmsURL = args.wmsURL;
                        var layerPropertyName = args.layerPropertyName;
                        var outputIdentifier = args.outputIdentifier;
                        var layerNamesString = args.layerNamesString;
//                        var testLayerNames = layerNamesString + ",topp:tasmania_state_boundaries";
//                        console.log(testLayerNames);
                        
                        var wmsLayer = {
                                name: 'Output: ' + outputIdentifier,
                                type: 'wms',
                                visible: true,
                                url: wmsURL,
                                layerParams: {
                                	layers: layerNamesString,
                                	format: 'image/png',
                                    transparent: true
                                }
                            };
                        
                        $scope.layers.overlays[layerPropertyName] = wmsLayer;    
                    });
                    
                    var checkPopupContentProperty = function(geoJson, identifier){
                    	/*
                         * check if geoJsonOutput has a .property.popupContent attribute
                         * (important for click interaction with displayed output,
                         * as it will be displayed in a popup)
                         * 
                         * if not, then set it with the identifier
                         */
                        if(geoJson.properties){
                        	if(geoJson.properties.popupContent){
                        		/*
                        		 * here we have to do nothing, as the desired property is already set
                        		 */
                        	}
                        	else
                        		geoJson.properties.popupContent = identifier;
                        }
                        else{
                        	geoJson.properties = {};
                        	geoJson.properties.popupContent = identifier;
                        }
                        
                        /*
                         * here we check the .properties.popupContent property for each feature of the output!
                         */
                        if(geoJson.features){
                        	var features = geoJson.features;
                        	
                        	for (var i in features){
                        		var currentFeature = features[i];
                        		
                        		if(currentFeature.properties){
                                	if(currentFeature.properties.popupContent){
                                		/*
                                		 * here we have to do nothing, as the desired property is already set
                                		 */
                                	}
                                	else
                                		currentFeature.properties.popupContent = identifier;
                                }
                                else{
                                	currentFeature.properties = {};
                                	currentFeature.properties.popupContent = identifier;
                                }
                        		
                        		features[i] = currentFeature;
                        	}
                        }
                    };
                    
                    /**
                     * Centers the map according to the given overlay
                     * 
                     */
                    $scope.centerGeoJSONOutput = function(layerPropertyName) {
                    	
                    	var latlngs = [];
                        
                    	/*
                    	 * TODO how to detect the array depth of coordinates???
                    	 * 
                    	 * FIXME how to detect the array depth of coordinates???
                    	 * 
                    	 * maybe use geoJSON type property to gues the array depth 
                    	 * (e.g. multiPolygon has different depth than simple Polygon)
                    	 */
                    	
                        var coordinates;
                        
                        if($scope.layers.overlays[layerPropertyName].data.geometry){
                        	coordinates = $scope.layers.overlays[layerPropertyName].data.geometry.coordinates;
                        	
                        	for (var i in coordinates) {
                                var points = coordinates[i];
                                for (var k in points) {
                                        latlngs.push(L.GeoJSON.coordsToLatLng(points[k]));
                                }
                            }
                        }
                        else if ($scope.layers.overlays[layerPropertyName].data.features){
                        	coordinates = $scope.layers.overlays[layerPropertyName].data.features[0].geometry.coordinates;
                        	
                        	 for (var i in coordinates) {
                                 var coord = coordinates[i];
                                 for (var j in coord) {
                                     var points = coord[j];
                                     for (var k in points) {
                                         latlngs.push(L.GeoJSON.coordsToLatLng(points[k]));
                                     }
                                 }
                             }
                        }
                        	
                        else
                        	return;

                        leafletData.getMap().then(function(map) {
                            map.fitBounds(latlngs);
                        });
                    };
                    
                    /**
                     * binds the popup of a clicked output 
                     * to layer.feature.properties.popupContent
                     */
                    function onEachFeature(feature, layer) {
					    // does this feature have a property named popupContent?
                    	layer.on({
                            click: function() {	
                            	
                              var popupContent = layer.feature.properties.popupContent;
                              
                              if(popupContent)
                            	  layer.bindPopup(popupContent);
                            }
                          })
					};
                    
                }]
        });

angular.module('wpsUserInterface', ['wpsMap', 'wpsControls']);
angular.module('wpsUserInterface').component('wpsUserInterface', {
	templateUrl : "components/wpsUserInterface/wps-user-interface.template.html",
	controller : function UserInterfaceController() {
		
	}
});
// Declare app level module which depends on views, and components
var appModule = angular.module('wpsClient', [ 'pascalprecht.translate',
		'wpsUserInterface', 'leaflet-directive' ]);

appModule
		.config([
				'$translateProvider',
				function($translateProvider) {
					$translateProvider.useStaticFilesLoader({
						prefix : 'i18n/',
						suffix : '.json'
					});
					var suppLang = [ 'en', 'de' ];
					$translateProvider.registerAvailableLanguageKeys(suppLang);
					$translateProvider.determinePreferredLanguage();
					if ($translateProvider.preferredLanguage() === ''
							|| suppLang.indexOf($translateProvider
									.preferredLanguage()) === -1) {
						$translateProvider.preferredLanguage('en');
					}
					$translateProvider.useSanitizeValueStrategy('escape');
				} ]);
