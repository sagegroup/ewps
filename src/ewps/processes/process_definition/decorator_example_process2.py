__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
from ewps.decorators.process_decorator import processDescription, processInput, processOutput, Integer, String, \
    BoundingBox, Float
from ewps.decorators.process_decorator import processing, is_processing, get_processing_attributes, argument, output, \
    redirectOutput


@processing(name="decorator_example_process2")
@argument(identifier="coord1", type=BoundingBox('EPSG:4326'))
@output(identifier="area", type=Float())
def decorator_example_process2(coord1):
    # example process to return area


    from math import sqrt
    x1, y1 = coord1.get_lower_corner()
    x2, y2 = coord1.get_upper_corner()
    dist = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

    # any function must return a dictionary with mapped values of the result ids and result values
    # large results should be send by reference and their value should be the saved path to file
    outputs = {}
    outputs['area'] = dist

    return outputs
