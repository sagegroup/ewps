__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
from ewps.registry import ComplexData
import rasterio
from PIL import Image
import numpy as np
from skimage.io import imsave as n_imsave
import os
from ewps.decorators.process_decorator import redirectOutput,createOutputFiles
import time

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

@redirectOutput()
@createOutputFiles(jobId=None,outputDir=None)
#the process must have the same input names as in the define .yaml config file
def compute_ndvi(input_image,coord1,result_image_name):

    #time.sleep(30)
    image=Image.open(StringIO(input_image.get_value()))
    img=np.array(image)

    #maybe do something with bounding box

    start = time.time()
    ndvi_img = np.zeros((img.shape[0], img.shape[1]), dtype=np.float32)
    for i in range(0, img.shape[0]):
        for j in range(0, img.shape[1]):
            ndvi_lower = float(img[i][j][0]) + float(img[i][j][1])
            ndvi_upper = float(img[i][j][0]) - float(img[i][j][1])
            if ndvi_lower == 0:
                ndvi_img[i][j] = 0
            else:
                ndvi_img[i][j] = (ndvi_upper) / (ndvi_lower * 1.0)

    this = compute_ndvi
    output_dir = this.get_output_dir()
    path = os.path.join(output_dir, result_image_name.get_value())
    n_imsave(path , ndvi_img)
    end = time.time()

    # any function must return a dictionary with mapped values of the result ids and result values
    # large results should be send by reference and their value should be the saved path to file
    outputs={}
    outputs['output_image']=path
    outputs['processing_time']=end-start

    return outputs


