__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
from ewps.decorators.process_decorator import processDescription, processInput, processOutput, Integer, String, processOffering, ComplexData
from ewps.decorators.process_decorator import processing, is_processing, get_processing_attributes, argument, output, redirectOutput


@redirectOutput()
@processDescription(name="decoratorExampleProcess1", title="Decorator Example Process 1",
                    metadata={'href': 'http://process-metadata-ref.ro'}, executionManager="local",
                    abstract="Test local processes", keywords=["key1","key2"])
@processInput(identifier="user_name", title="Input User Name", abstract="Input abstract", type=String())
@processOutput(identifier="output", title="Output title", abstract="Output abstract",type=String())
@processOffering(jobControlOptions="async-execute sync-execute",outputTransmission="value reference", processModel='native')
def decorator_example_process1(user_name):

    #example process to return hello world

    # any function must return a dictionary with mapped values of the result ids and result values
    # large results should be send by reference and their value should be the saved path to file
    outputs = {}
    outputs['output'] = "Hello, new EWPS user {}".format(user_name.get_value())

    return outputs



