__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import logging

from flask import Flask, render_template, send_from_directory
from flask import Response
from flask import request as flask_request
from flask.views import View
from lxml import etree
import os

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from ewps.chains import ChainTaskError
from ewps.exceptions import WPSProtocolError, ExceptionReport, WPS2ExceptionReport, RuntimeError

logger = logging.getLogger(__name__)

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SERVER_NAME'] = "localhost:5000"

JOB_DIR = "processes/job_files"


def serve_outputs(filename):
    from flask import send_from_directory
    return send_from_directory(
        app.root_path + '/processes/job_files/', filename,
        mimetype='text/html', as_attachment=True)


def receive_result_output(jobid):
    logger.debug("Received result for %s" % (jobid))

    # create job directory

    import tarfile
    tar = tarfile.open(fileobj=flask_request.files['file'])
    logger.debug(type(tar))
    tar.extractall(JOB_DIR)
    tar.close()

    job_dir_name = "job_%s" % jobid
    archive_name = os.path.join(JOB_DIR, "job_%s.tar.gz" % jobid)
    tar = tarfile.open(archive_name, "w:gz")
    tar.add(os.path.join(JOB_DIR, job_dir_name), arcname=job_dir_name)
    tar.close()

    logger.debug(os.listdir(JOB_DIR))
    return "OK"


class WPSHTTPGetAdapter(object):
    def convert_get_to_request(self, http_request):
        raise NotImplementedError()


class WPSHTTPGetAdapter_2_0(WPSHTTPGetAdapter):
    log = logger.getChild("WPSHTTPGetAdapter_2_0")
    WPS_NS = "http://www.opengis.net/wps/2.0"
    NS = "http://www.opengis.net/wps/2.0"
    XSI_NS = 'http://www.w3.org/2001/XMLSchema-instance'
    OWS_NS = "http://www.opengis.net/ows/2.0"
    XML_OWS = "{%s}" % OWS_NS
    XML_WPS = "{%s}" % WPS_NS
    NSMAP = {
        'wps': NS,
        'xsi': XSI_NS,
        'ows': OWS_NS
    }

    def convert_get_to_request(self, http_request):
        wps_operation = http_request.args.get("request", flask_request.args.get("Request", None))
        if wps_operation is None:
            self.log.warning("missing WPS operation in GET request")
            raise WPSProtocolError("Missing WPS Operation")

        if wps_operation == "GetCapabilities":
            return self.wps_getcapabilities_kv_to_xml(flask_request)
        elif wps_operation == "DescribeProcess":
            return self.wps_describeprocess_kv_to_xml(flask_request)
        elif wps_operation == 'GetStatus':
            return self.wps_getstatus_kv_to_xml(flask_request)
        elif wps_operation == 'GetResult':
            return self.wps_getresult_kv_to_xml(flask_request)
        else:
            raise WPSProtocolError("Unknown operation")

    def wps_getcapabilities_kv_to_xml(self, request):
        root = etree.Element("{http://www.opengis.net/wps/2.0}GetCapabilities", nsmap=self.NSMAP)
        root.set('{%s}schemaLocation' % self.XSI_NS, 'http://www.opengis.net/wps/2.0 ../wps.xsd')
        root.set('service', "WPS")
        root.set('version', request.args.get("version"))

        return root

    def wps_getstatus_kv_to_xml(self, request):
        argm = {k.lower(): v for k, v in request.args.items()}

        root = etree.Element("{http://www.opengis.net/wps/2.0}GetStatus", nsmap=self.NSMAP)
        root.set('{%s}schemaLocation' % self.XSI_NS, 'http://www.opengis.net/wps/2.0 ../wps.xsd')
        root.set('service', "WPS")
        root.set('version', argm.get("version"))
        jobid_node = etree.Element(self.XML_WPS + "JobID", nsmap=self.NSMAP)

        jobid_node.text = argm.get("jobid")
        print (type(request.args))
        root.append(jobid_node)

        return root

    def wps_getresult_kv_to_xml(self, request):
        argm = {k.lower(): v for k, v in request.args.items()}
        root = etree.Element("{http://www.opengis.net/wps/2.0}GetResult", nsmap=self.NSMAP)
        root.set('{%s}schemaLocation' % self.XSI_NS, 'http://www.opengis.net/wps/2.0 ../wps.xsd')
        root.set('service', "WPS")
        root.set('version', argm.get("version"))
        jobid_node = etree.Element(self.XML_WPS + "JobID", nsmap=self.NSMAP)
        jobid_node.text = argm.get("jobid")

        root.append(jobid_node)

        return root

    def wps_describeprocess_kv_to_xml(self, request):
        root = etree.Element("{http://www.opengis.net/wps/2.0}DescribeProcess", nsmap=self.NSMAP)
        root.set('{%s}schemaLocation' % self.XSI_NS, 'http://www.opengis.net/wps/2.0 ../wps.xsd')
        root.set('service', "WPS")
        root.set('version', request.args.get("version"))
        identifier = request.args.get("identifier", None)
        if identifier is None:
            raise WPSProtocolError("missing process identifier field")
        if identifier == 'ALL':
            identifier_node = etree.Element(self.XML_OWS + "Identifier", nsmap=self.NSMAP)
            identifier_node.text = identifier
            root.append(identifier_node)
        else:
            identifier_list = identifier.split(',')
            for id in identifier_list:
                identifier_node = etree.Element(self.XML_OWS + "Identifier", nsmap=self.NSMAP)
                identifier_node.text = id
                root.append(identifier_node)
        return root

    def wps_execute_kv_to_xml(self, request):
        # ToDo: @teodora is this needed ? If I remeber correctly you can't call `Execute` using HTTP GET
        root = etree.Element("{http://www.opengis.net/wps/2.0}GetCapabilities", nsmap=self.NSMAP)
        root.set('{%s}schemaLocation' % self.XSI_NS, 'http://www.opengis.net/wps/2.0 ../wps.xsd')
        root.set('version', request.args.get("version"))
        root.set('service', "WPS")


class WPSHTTPGetAdapter_1_0(WPSHTTPGetAdapter):
    pass


class WPSHTTPHandler(View):
    log = logger.getChild("WPSHTTPHandler")
    methods = ['GET', 'POST']

    def __init__(self, executor, chain):
        self.log.debug("Creating WPS HTTP Handler")
        self.executor = executor
        self.chain = chain

        super(WPSHTTPHandler, self).__init__()

    def dispatch_request(self):
        if flask_request.method not in ('GET', 'POST'):
            self.log.error("Unsupported request method %s", flask_request.method)
            raise  # ToDo: this should result in a specific HTTP error being sent back

        try:
            wps_version = None  # Initialisez early so we have information in an eventual exception
            if flask_request.method == 'GET':
                wps_request_document = self.convert_get_to_request(flask_request)
            else:
                if flask_request.data:
                    wps_request_document = etree.fromstring(flask_request.data)
                else:
                    self.log.warning("Missing request data!")
                    wps_request_document = None

            if wps_request_document is None:
                raise WPS2ExceptionReport(WPSProtocolError("Missing WPS Request"))

            wps_version = wps_request_document.attrib.get("version")

            self.log.debug("Delegating request to executor")

            result = self.executor.handle_in_chain(wps_request_document, self.chain)
            wps_response = result.response
            response_string = etree.tostring(wps_response, pretty_print=True)
            return Response(response_string, mimetype="text/xml", headers=result.headers)
        except WPS2ExceptionReport as wps2_error_report:
            if wps_version is None:
                wps_version = "2.0.0"
            if wps_version != "2.0.0":  # ToDo: we should return VersionNegotiationFailed
                raise NotImplementedError()
            error_string = etree.tostring(wps2_error_report.to_xml())
            return Response(error_string, status=501, mimetype="text/xml", headers={})
        except ChainTaskError as error:
            error_document = None
            if wps_version == "2.0.0":
                error_document = WPS2ExceptionReport(RuntimeError(str(error.innetError))).to_xml()
            else:  # ToDo: we should return VersionNegotiationFailed
                raise NotImplementedError()

            if error_document is None: raise NotImplementedError()

            error_string = etree.tostring(error_document)
            return Response(error_string, status=501, mimetype="text/xml", headers={})
        except Exception as error:
            self.log.exception("unhandled exception")
            raise error

    def convert_get_to_request(self, http_request):
        wps_version = http_request.args.get("version", flask_request.args.get("Version", None))
        if wps_version is None:
            raise WPSProtocolError("Protocol version not specified")

        get_adapter = None
        if wps_version == "2.0.0":
            get_adapter = WPSHTTPGetAdapter_2_0()
        elif wps_version == "1.0.0":
            get_adapter = WPSHTTPGetAdapter_1_0()

        if get_adapter is None:
            raise WPSProtocolError("Unsupported WPS protocol version: %s" % wps_version)

        return get_adapter.convert_get_to_request(http_request)
