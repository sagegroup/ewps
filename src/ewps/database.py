__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import os
import sqlite3
import logging
from ewps.utils import expand_variables

logger = logging.getLogger(__name__)


class DbConnector():
    def prepare_connection(self):
        raise NotImplementedError


class LocalConnector(DbConnector):
    def __init__(self, path, table_name):
        self._collections = {}
        self._path = os.path.join(os.path.abspath(path))
        self._table_name = table_name
        self._log = logger.getChild("LocalConnector")
        # TODO replace with ORM instead of string concat statements

    def get_connection(self):
        self._log.debug("Openining database connection to: %s", self._path)
        return sqlite3.connect(self._path)

    def prepare_connection(self):
        self._connection = self.get_connection()

    def add_info(self, **kwargs):
        if kwargs is None:
            raise KeyError("No arguments specified")
        if 'job_uuid' not in kwargs:
            raise KeyError("The identifier for the job is not specified")

        # TODO rewrite with ORM
        # create table if it does not exist
        statement = "CREATE TABLE IF NOT EXISTS " + self._table_name + " (uuid text PRIMARY KEY, process_identifier text NOT NULL, output_transmission text NOT NULL, output_id text NOT NULL)"
        self._connection.execute(statement)

        for process_output_id in kwargs["process_output"].keys():
            # insert data
            statement = "INSERT INTO " + self._table_name + " (uuid, process_identifier, output_transmission, output_id) VALUES ("
            statement = statement + "'" + kwargs['job_uuid'] + "_" + process_output_id + "'"
            statement = statement + ", '" + kwargs['process_identifier'] + "'"
            statement = statement + ", '" + kwargs['process_output'][process_output_id]['transmission'] + "'"
            statement = statement + ", '" + process_output_id + "'"
            statement = statement + ")"
            self._connection.execute(statement)

        self._connection.commit()

    def get_data(self, condition):

        logger.debug("Get data from db " + str(condition))

        statement = "SELECT * FROM " + self._table_name + " WHERE uuid LIKE '{}%'".format(condition['job_uuid'])
        result = self._connection.execute(statement)
        result_list = []
        for row in result.fetchall():
            entry = {}
            entry[row[0]] = {}
            entry[row[0]]['process_identifier'] = row[1]
            entry[row[0]]['output_transmission'] = row[2]
            entry[row[0]]['output_id'] = row[3]
            result_list.append(entry)

        return result_list


def db_populate_constructor_from_dict(db_connector, config_dict):
    db_connector._host = config_dict.get('host', None)
    db_connector._port = config_dict.get('port', None)
