__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
from importlib import import_module

try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

from logging import getLogger
from ewps.handlers.core import RequestHandler, SafeRequestHandler

log = getLogger(__name__)


class ChainError(Exception):
    pass


class ChainLookupError(ChainError):
    pass


class ChainTaskError(ChainError):
    def __init__(self, chain=None, request=None, context=None, task=None, innerError=None):
        self.innetError = innerError
        self.chain = chain
        self.request = request
        self.context = context
        super(ChainTaskError, self).__init__("Error generated in chain: %s" % innerError)


class RequestContext(object):
    def __init__(self, request):
        self.request = request


class HandlerChain(object):
    log = log.getChild("HandlerChain")

    def __init__(self, name):
        self.name = name
        self.tasks = []

    def add_task(self, task):
        self.tasks.append(task)

    def handle(self, request, context):
        return self.__handle(request, context, self.tasks)

    def __handle(self, request, context, tasks):
        if not tasks:
            return None

        current_task, other_tasks = tasks[0], tasks[1:]
        context.chain = self

        # Initially we run the current task and obtain its result
        current_response = current_task.handle_request(request, context)
        # If the handler did not return this implies we can go to the next one
        if current_response is not None:  # we stop the whole processing chain and return
            return current_response
        # The handler did not return and now we call the next one
        next_response = self.__handle(request, context, other_tasks)
        response = current_task.handle_response(next_response, context)
        return response


class Task(object):
    log = log.getChild("Task")

    def __init__(self, name, handler, options, enabled):
        self.name = name
        self.handler = handler
        self.handler_instance = None
        self.options = options
        self.enabled = enabled

        # We do this in __init__ to avoid concurrency issues
        if issubclass(handler, SafeRequestHandler):
            self.handler_instance = handler()

    def handle_request(self, request, context):
        self.log.debug("Handling request in task `%s`", self.name)
        context.task = self

        handler = self.handler
        if issubclass(handler, RequestHandler):
            handler = self.handler() if self.handler_instance is None else self.handler_instance

        if isinstance(handler, RequestHandler):
            return handler.handle_request(request, context)
        elif callable(handler):
            return handler(request, context)
        else:
            raise NotImplementedError("Unknown type of handler: %s (%s)" % (handler, type(handler)))

    def handle_response(self, response, context):
        self.log.debug("Handling response in task `%s`", self.name)
        context.task = self

        handler = self.handler
        if issubclass(handler, RequestHandler):
            handler = self.handler() if self.handler_instance is None else self.handler_instance

        if isinstance(handler, RequestHandler):
            return handler.handle_response(response, context)
        else:  # ToDo not implemented for generic callable's
            return response


def lookup_task_from_module(scheme, netloc, path):
    assert scheme == "python"
    module_name, callable_name = path.split(":", 1)
    module_name = module_name[1:] if module_name.startswith("/") else module_name
    mod = import_module(module_name)
    hndlr = getattr(mod, callable_name, None)
    if hndlr is None:
        log.critical("module (%s) is missing expected attribute (%s)", module_name, callable_name)
        return None
    return hndlr


__lookup_handlers = {
    'python+module': lookup_task_from_module,
    'python': lookup_task_from_module
}


def lookup_task(task_uri):
    log.debug("Looking up %s", task_uri)
    parsed_task_url = urlparse(task_uri)
    if parsed_task_url.scheme not in __lookup_handlers:
        raise NotImplementedError("Scheme handler for '%s' not defined" % parsed_task_url.scheme)
    task = __lookup_handlers[parsed_task_url.scheme](parsed_task_url.scheme,
                                                     parsed_task_url.netloc,
                                                     parsed_task_url.path)
    return task


class ChainExecutor(object):
    chains = {}
    log = log.getChild("ChainExecutor")

    def __init__(self, global_config):
        self._global_config = global_config

    def add_chain(self, name, chain):
        if name is None:
            name = chain.name
        self.chains[name] = chain

    def __setitem__(self, key, value):
        self.add_chain(key, value)

    def handle_in_chain(self, request, chain, request_context=None):
        self.log.debug("Handling request in chain: %s", chain)
        start_chain = self.chains.get(chain)
        if start_chain is None:
            raise KeyError("No such chain: %s" % chain)
        if request_context is None:
            request_context = self.create_context(request_context)
            setattr(request_context, "config", self._global_config)
        try:
            return start_chain.handle(request, request_context)
        except Exception as innerError:
            if isinstance(innerError, ChainTaskError):
                raise innerError  # re-raising an error generated in a subsequent task
            else:
                log.exception("Handler generated exception")
                raise ChainTaskError(chain=chain, request=request_context, context=request_context,
                                     innerError=innerError)

    def handle(self, request):
        return self.handle_in_chain(request, "__init__")

    def create_context(self, request):
        request_context = RequestContext(request)
        request_context.executor = self
        return request_context


def populate_constructor_from_dict(executor, config_dict):
    for chain_name, chain_tasks in config_dict.items():
        log.debug("creating chain: %s", chain_name)
        if chain_tasks is None:
            log.warning("chain `%s` does not seem to have any tasks!", chain_name)
            continue
        chain_obj = HandlerChain(chain_name)
        for task in chain_tasks:
            task_name = task["name"]
            task_provider = task["provider"]
            task_options = task.get("options", {})
            task_enabled = task.get("enabled", True)
            log.debug("adding task %s (%s) to chain %s", task_name, task_provider, chain_name)
            task_handler = lookup_task(task_provider)
            if task_handler is None:
                log.critical("Could not load task `%s` from chain `%s` using provider `%s`", task_name, chain_name,
                             task_provider)
                raise ChainLookupError("Could not load task %s" % task_name)
            chain_obj.add_task(Task(task_name, handler=task_handler, options=task_options, enabled=task_enabled))
        executor[chain_name] = chain_obj


if __name__ == '__main__':
    config = {
        "__init__": [
            {
                "name": "doing-nothing",
                "provider": "python:ewps.handlers.core:noop",
                "enabled": True
            },
            {
                "name": "goto-main-chain",
                "provider": "python:ewps.handlers.core:goto",
                "enabled": True,
                "options": {
                    "destination": "main"
                }
            }

        ],
        'main': [
            {
                "name": "upper-case",
                "provider": "python:ewps.handlers.core:Error",
                "enabled": True
            }
        ],
        '__final__': [

        ]
    }

    chain_executor = ChainExecutor()
    populate_constructor_from_dict(chain_executor, config)

    result = chain_executor.handle("goo")
    print("final result: ", result)
