__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import uuid
import os
from ewps.utils import expand_variables


class ExecutionManger(object):
    def __init__(self):
        self._job_dir = None

    def start(self, process_uuid, process_inputs, process_summary, process_output):
        raise NotImplementedError()

    def generate_process_uuid(self):
        process_uuid = str(uuid.uuid4())
        return process_uuid

    def get_status(self, process_uuid):
        raise NotImplementedError()

    def is_finished(self, job_id):
        raise NotImplementedError

    def set_db_connector(self, db_connector):
        self._db_connector = db_connector

    def get_process_by_identifier(self, job_id):
        condition = {'job_uuid': job_id}
        result = self._db_connector.get_data(condition)

        if len(result) < 1:
            return None

        # assume only one job exists with the uuid
        return result[0]['process_identifier']

    def get_process_identifier(self, job_id):
        condition = {'job_uuid': job_id}
        result = self._db_connector.get_data(condition)

        if len(result) < 1:
            return None

        return list(result[0].values())[0]['process_identifier']

    def get_result(self, process_uuid):
        # get output format
        condition = {'job_uuid': process_uuid}
        result = self._db_connector.get_data(condition)

        if len(result) < 1:
            return None

        # read from the result file
        # ToDo: handle exceptions
        result_file = os.path.join(self._job_dir, "job_%s/outputs/result_file_%s.out") % (process_uuid, process_uuid)
        job_dir = os.path.join(self._job_dir, "job_%s/outputs/" % process_uuid)

        try:
            file = open(result_file, "r")
            output_string = file.read()
            from ast import literal_eval
            output = literal_eval(output_string)
            file.close()
        except:
            output = None

        for item in result:
            output_item = list(item.values())[0]
            if output_item['output_id'] in output:
                value = output[output_item['output_id']]
                output[output_item['output_id']] = {}
                output[output_item['output_id']]['value'] = value
                output[output_item['output_id']]['transmission'] = output_item['output_transmission']

        return output


class ProcessSummary(object):
    def __init__(self, process_definition, executionManager, executionManagers, jobControlOptions=None,
                 outputTransmission=None, processModel=None):
        self.definition = process_definition
        self._jobControlOptions = jobControlOptions
        self._processModel = processModel
        self._outputTransmission = outputTransmission
        self._executionManager = None

        # set the execution manager
        if self.definition.get_execution_manager_type() is not None:
            # process definition has an execution manager preference
            self._executionManager = executionManagers[self.definition.get_execution_manager_type()]
        else:
            # set the registry execution manager
            self._executionManager = executionManager

    def add_options_from_dict(self, definition, definition_source=None):

        output_transmission = definition.get("outputTransmission", ['value'])
        job_control_options = definition.get("jobControlOptions", ['async-execute'])
        process_model = definition.get("processModel", 'native')

        self._outputTransmission = " ".join(output_transmission)
        self._jobControlOptions = " ".join(job_control_options)
        self._processModel = process_model

    @property
    def identifier(self):
        return self.definition.identifier

    @property
    def title(self):
        return self.definition.title

    @property
    def abstract(self):
        return self.definition.abstract

    @property
    def keywords(self):
        return self.definition.keywords

    @property
    def metadata(self):
        return self.definition._metadata

    def __repr__(self):
        return "process " + self.definition.identifier

    def __str__(self):
        return "process " + self.definition.identifier

    def get_process_definition(self):
        return self.definition

    def get_jobControlOptions(self):
        return self._jobControlOptions

    def get_output_transmission(self):
        return self._outputTransmission

    def get_process_model(self):
        return self._processModel

    def get_execution_manager(self):
        return self._executionManager
