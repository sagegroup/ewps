__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import argparse
import os
import importlib
import requests
import urllib3
import tarfile
import time

import logging
import sys
import json
from ewps.registry import decode_json
from ewps.registry import ComplexData

JOB_FILES_DIR = "/opt/ewps/src/ewps/processes/job_files/"

log = logging.getLogger()


def main():
    parser = argparse.ArgumentParser(description='Execute processes in docker container')

    parser.add_argument('--job-info', required=True, type=str, help="Info of the job to be executed as JSON")

    # get arg values
    args = parser.parse_args()
    job_info_url = args.job_info

    r = requests.get(job_info_url)
    data = r.json()

    job_uuid = data.get('job_uuid', None)
    process_entrypoint = data.get('entrypoint', None)
    inputs = data.get('inputs', None)
    result_url = data.get('result_url', None)

    if process_entrypoint is not None:

        entities = process_entrypoint.split(':')
        module_name = entities[0]
        function_name = entities[1]

        # prepare job directory
        # make sure job_files dir exists, if not create it
        if not os.path.exists(os.path.abspath(JOB_FILES_DIR)):
            os.mkdir(os.path.abspath(JOB_FILES_DIR))

        # create a new directory for the job
        # ToDo: handle possible exceptions
        os.mkdir(os.path.join(os.path.abspath(JOB_FILES_DIR), "job_%s" % job_uuid))

        # create a new directory for the job's outputs
        # ToDo: handle possible exceptions
        job_dir_name = "job_%s/" % job_uuid
        os.mkdir(JOB_FILES_DIR + "job_%s/" % job_uuid + "outputs")
        output_file = (JOB_FILES_DIR + "job_%s/outputs/output_file_%s.out") % (job_uuid, job_uuid)
        error_file = (JOB_FILES_DIR + "job_%s/outputs/error_file_%s.out") % (job_uuid, job_uuid)
        result_file = (JOB_FILES_DIR + "job_%s/outputs/result_file_%s.out") % (job_uuid, job_uuid)

        # load module
        module = importlib.import_module(module_name)
        function = getattr(module, function_name)

        # create new process
        if output_file != None:
            function.set_output(output_file)
        if error_file != None:
            function.set_error(error_file)

        # check if functions need to create its own files
        if hasattr(function, '_createOutputFiles') and getattr(function, '_createOutputFiles'):
            os.mkdir(JOB_FILES_DIR + "job_%s/" % job_uuid + "files")
            function.set_output_info(job_uuid, (JOB_FILES_DIR + "job_%s/files/") % job_uuid)

        # decode inputs json
        function_inputs = {}
        for key in inputs:
            if inputs[key]['transmission'] == 'value':
                function_inputs[key] = json.loads(json.dumps(inputs[key]), object_hook=decode_json)
            else:
                c = ComplexData()
                c.set_transmission('reference')
                c.set_parameters_by_ref(**inputs[key]['value'])
                function_inputs[key] = c

        job_result = function(**function_inputs)

        # write function result to file
        with open(result_file, "w") as r:
            r.write(str(job_result))

        # create archive
        archive_name = JOB_FILES_DIR + "job_%s.tar.gz" % job_uuid
        tar = tarfile.open(archive_name, "w:gz")
        tar.add(os.path.join(os.path.abspath(JOB_FILES_DIR), job_dir_name), arcname=job_dir_name)
        tar.close()

        sys.stdout.flush()
        sys.stderr.flush()

        # make post request
        files = {'file': open(archive_name, 'rb')}
        result_r = requests.post(result_url, files=files)

    sys.stdout.flush()
    sys.stderr.flush()
