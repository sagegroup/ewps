__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import logging

from ewps.utils import expand_variables

from ewps.execution.core import ExecutionManger
import os
import json
from ewps.registry import DataTypeEncoder, BoundingBoxData, ComplexData, LiteralData
from flask import Flask
from flask import request
import requests

logger = logging.getLogger(__name__)

JOB_FILES_DIR = "processes/job_files/"

app = Flask(__name__)


class CookExecutionManager(ExecutionManger):
    log = logger.getChild("CookExecutionManager")

    def __init__(self, options, config):
        super(ExecutionManger, self).__init__()
        self._options = options
        self._config = config
        self._global_config = config
        self._job_dir = os.path.abspath(expand_variables(options.get("job-dir"), config))

    def set_db_connector(self, db_connector):
        self._db_connector = db_connector

    def create_cook_request_for_job(self, job_name, job_dir, job_uuid):
        cook_request = {}
        cook_request['disable-mea-culpa-retries'] = True
        cook_request['mem'] = 1000
        cook_request['application'] = {}
        cook_request['application']['name'] = job_name
        cook_request['application']['version'] = "1.0.0"
        cook_request['name'] = job_name
        # ToDo do not hardcode params
        command = []
        command.append(self._options.get("command_script", None))
        if command[0] is not None:
            command.append("--job-info")
            ewps_host = self._options.get("ewps_host", request.headers.get('Host'))
            command.append("http://" + ewps_host + "/result/job_" + job_uuid + "/job_info.json")
            # command.append("http://ewps.services.eo4see.info.uvt.ro/result/job_info.json")
            cook_request['command'] = " ".join(command)
        cook_request['max_runtime'] = 86400000
        cook_request['max-retries'] = 10
        cook_request['uuid'] = job_uuid
        cook_request['executor'] = 'cook'
        cook_request['container'] = {}
        cook_request['container']['type'] = 'DOCKER'
        cook_request['container']['docker'] = {}
        cook_request['container']['docker']['image'] = self._options.get('docker_image', None)
        cook_request['container']['docker']['network'] = 'BRIDGE'
        cook_request['container']['docker']['force-pull-image'] = True
        cook_request['container']['volumes'] = []
        hosts = {}
        hosts['container-path'] = "/etc/hosts"
        hosts['host-path'] = "/etc/hosts"
        cook_request['container']['volumes'].append(hosts)
        cook_request['cpus'] = 1

        return cook_request

    def create_job_info(self, process_uuid, process_entrypoint, process_inputs):
        job_info = {}
        job_info['job_uuid'] = process_uuid
        job_info['entrypoint'] = process_entrypoint
        job_info['inputs'] = {}
        for key in process_inputs:
            job_info['inputs'][key] = {}
            input_type = '__complexdata__'
            transmission = 'value'
            # check if input is send by reference
            # is dict only if it is send by refenrence
            if isinstance(process_inputs[key], ComplexData) and process_inputs[key].get_transmission() == 'reference':
                transmission = 'reference'
                input_type = '__complexdata__'
                job_info['inputs'][key]['value'] = process_inputs[key].get_parameters_by_ref()

            if transmission == 'value':
                if isinstance(process_inputs[key], BoundingBoxData):
                    input_type = '__boundingboxdata__'
                    job_info['inputs'][key]['value'] = {}
                    job_info['inputs'][key]['value']['lower_corner'] = process_inputs[key].get_lower_corner()
                    job_info['inputs'][key]['value']['upper_corner'] = process_inputs[key].get_upper_corner()
                    job_info['inputs'][key]['value']['dimensions'] = process_inputs[key].get_dimensions()
                    job_info['inputs'][key]['value']['crs'] = process_inputs[key].get_crs()
                if isinstance(process_inputs[key], ComplexData):
                    input_type = '__complexdata__'
                    job_info['inputs'][key]['value'] = process_inputs[key].get_value()
                if isinstance(process_inputs[key], LiteralData):
                    input_type = '__literaldata__'
                    job_info['inputs'][key]['value'] = process_inputs[key].get_value()
            job_info['inputs'][key]['type'] = input_type
            job_info['inputs'][key]['transmission'] = transmission

        result_url = self._options.get('result_url', None)
        if result_url is None:
            raise KeyError("Can not return job result. Result url is missing")
        job_info['result_url'] = result_url + process_uuid

        return job_info

    def start(self, process_uuid, process_inputs, process_summary, process_output):

        # create job dir
        # prepare job directory
        # make sure job_files dir exists, if not create it
        if not os.path.exists(self._job_dir):
            os.mkdir(self._job_dir)

        # create a new directory for the job
        # ToDo: handle possible exceptions
        job_name = "job_%s" % process_uuid
        job_dir = os.path.join(self._job_dir, "job_%s" % process_uuid)
        os.mkdir(os.path.join(self._job_dir, "job_%s" % process_uuid))

        # create json job_info file
        process_entrypoint = process_summary.get_process_definition()._entrypoint
        job_info = self.create_job_info(process_uuid, process_entrypoint, process_inputs)

        # save to job dir job_info_file
        with open(os.path.join(job_dir, 'job_info.json'), 'w') as job_info_file:
            json.dump(job_info, job_info_file, cls=DataTypeEncoder, sort_keys=True, indent=4)

        job_cook_request = self.create_cook_request_for_job(job_name, job_dir, process_uuid)
        jobs = []
        jobs.append(job_cook_request)
        cook_request = {}
        cook_request['jobs'] = jobs

        cook_request_json = json.dumps(cook_request)

        headers = {"Content-Type": "application/json", "Accept": "application/json"}
        # ToDo handle possible exception if cook_scheduler_url not provided
        result_r = requests.post(self._options['cook_scheduler_url'], headers=headers, json=cook_request)
        self.log.debug(result_r.text)

        # Todo maybe check is result_r contains "submitted"

        # add process to db
        process_identifier = process_summary.identifier
        self.add_process(process_uuid, process_output, process_identifier)

    def add_process(self, process_uuid, process_output, process_identifier):

        if self._db_connector is None:
            raise KeyError("No database connector is specified")
        self._db_connector.add_info(job_uuid=process_uuid, process_output=process_output,
                                    process_identifier=process_identifier)

    def get_status(self, process_uuid):

        # prepare cook request to get status
        headers = {'Accept': 'application/json'}
        # ToDo handle possible exception if cook_status_url not provided
        result = requests.get(self._options['cook_status_url'] + process_uuid, headers=headers,
                              auth=requests.auth.HTTPBasicAuth('eouser', 'pass'))
        data = result.json()

        instances = data['instances']
        if len(instances) > 0:
            for instance in instances:
                if instance['status'].lower() == 'running':
                    return 'Running'
                if instance['status'].lower() == 'success':
                    return 'Succeeded'
                if instance['status'].lower() == 'unknown' or instance['status'].lower() == 'waiting':
                    return 'Accepted'
            return 'Failed'

        else:
            status = data['status']
            self.log.debug("Status " + status)
            if status.lower() == 'unknown' or status.lower() == 'waiting':
                return 'Accepted'
            if status.lower() == 'running':
                return 'Running'
            if status.lower() == 'success':
                return 'Succeeded'
            if status.lower() == 'failed':
                return 'Failed'

    def is_finished(self, job_id):
        if self.get_status(job_id) == "Succeeded" or self.get_status(job_id) == "Failed":
            return True
        return False
