__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import os
import importlib
from ewps.execution.core import ExecutionManger
import multiprocessing
import logging

import urllib3

from ewps.utils import expand_variables

logger = logging.getLogger(__name__)


class LocalExecutionManager(ExecutionManger):
    log = logger.getChild("LocalExecutionManager")

    def __init__(self, options, config):
        super(ExecutionManger, self).__init__()
        self._global_config = config
        self._job_dir = os.path.abspath(expand_variables(options.get("job-dir"), config))

        self._executionRegistry = {}
        self._db_connector = None
        self._executionManager = "local"
        self._exec_mapping = {}
        self._jobs = {}
        self._val = 9

    def get_executionManager_type(self):
        return self._executionManager

    def start(self, process_uuid, process_inputs, process_summary, process_output):
        """Starts a new job running a specific WPS process based on process_id

        :param process_uuid: the uuid  of the process from the registry
        :return: the job id
        """

        process_entrypoint = process_summary.get_process_definition()._entrypoint

        if process_entrypoint is not None:
            entities = process_entrypoint.split(':')
            module_name = entities[0]
            function_name = entities[1]

            # prepare job directory
            # make sure job_files dir exists, if not create it
            if not os.path.exists(self._job_dir):
                os.mkdir(self._job_dir)

            # create a new directory for the job
            # ToDo: handle possible exceptions
            os.mkdir(os.path.join(self._job_dir, "job_%s" % process_uuid))

            # create a new directory for the job's outputs
            # ToDo: handle possible exceptions
            os.mkdir(os.path.join(self._job_dir, "job_%s/" % process_uuid, "outputs"))
            result_file = os.path.join(self._job_dir,
                                       "job_%s/outputs/result_file_%s.out" % (process_uuid, process_uuid))
            error_file = os.path.join(self._job_dir, "job_%s/outputs/error_file_%s.out" % (process_uuid, process_uuid))
            output_file = os.path.join(self._job_dir,
                                       "job_%s/outputs/output_file_%s.out" % (process_uuid, process_uuid))

            # load module
            module = importlib.import_module(module_name)
            function = getattr(module, function_name)

            # create new process output and error files
            if output_file != None:
                function.set_output(output_file)
            if error_file != None:
                function.set_error(error_file)

            # check if functions need to create its own files
            if hasattr(function, '_createOutputFiles') and getattr(function, '_createOutputFiles'):
                os.mkdir(os.path.join(self._job_dir, "job_%s/" % process_uuid, "files"))
                function.set_output_info(process_uuid, os.path.join(self._job_dir, "job_%s/files/" % process_uuid))

            def wrap_processing(func, result_file, process_inputs):
                result = func(**process_inputs)
                with open(result_file, "w") as r:
                    r.write(str(result))

            self.log.debug("Creating a new process for job %s" % process_uuid)
            func_arg = {'func': function, 'result_file': result_file, 'process_inputs': process_inputs}
            p = multiprocessing.Process(target=wrap_processing, kwargs=func_arg, name=process_uuid)

            self.log.debug("Add process %s to jobs pool")
            self._jobs[process_uuid] = p

            self.log.debug("Starting process for job with uuid %s" % process_uuid)
            p.start()

            process_identifier = process_summary.identifier

            self.add_process(process_uuid, process_output, process_identifier)

            return process_uuid

    def get_status(self, process_uuid):
        p = self._jobs[process_uuid]
        exitcode = p.exitcode
        if p.is_alive() and exitcode is None:
            return "Running"
        if exitcode != 0:
            return "Failed"
        return "Succeeded"

    def add_process(self, process_uuid, process_output, process_identifier):

        if self._db_connector is None:
            raise KeyError("No database connector is specified")
        self._db_connector.add_info(job_uuid=process_uuid, process_output=process_output,
                                    process_identifier=process_identifier)

    def is_finished(self, job_id):
        p = self._jobs[job_id]
        exitcode = p.exitcode
        if p.is_alive() and exitcode is None:
            return False
        return True
