__license__ = \
    """Copyright 2019 West University of Timisoara

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    """
import logging
import os
from glob import glob
# import urllib3
import re
import json

import urllib3

try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

import lxml.etree as etree
import yaml
from flask import request
from pkg_resources import resource_filename
from ewps.decorators.process_decorator import EWPS_PROCESS_REGISTRY, is_processing, get_processing_attributes, \
    get_process_offering
from ewps.execution.core import ProcessSummary
import importlib
import pkgutil
import inspect
import imp
import sys

WPS_NS = "http://www.opengis.net/wps/2.0"
OWS_NS = "http://www.opengis.net/ows/2.0"
XSI_NS = 'http://www.w3.org/2001/XMLSchema-instance'
XLINK = "http://www.w3.org/1999/xlink"
XML_WPS = "{%s}" % WPS_NS
XML_OWS = "{%s}" % OWS_NS
XML_XLINK = "{%s}" % XLINK
NSMAP = {
    'wps': WPS_NS,
    'xsi': XSI_NS,
    'ows': OWS_NS,

}

logger = logging.getLogger(__name__)


class WPSProcessRegistry(object):
    log = logger.getChild("WPSProcessRegistry")

    def __init__(self, executionManager):
        self._registry = {}
        self._executionManager = executionManager

    def get_processes(self):
        for process in self._registry:
            yield self._registry[process]

    def add_process(self, process_id, process):
        if process_id in self._registry:
            raise ValueError("Process identified by process_id '%s' is already defined" % process_id)
        self._registry[process_id] = process
        return process

    def get_process_by_identifier(self, process_identifier):
        if process_identifier in self._registry:
            return self._registry[process_identifier]
        return None


class LocalFSRegistry(WPSProcessRegistry):
    log = logger.getChild("LocalFSRegistry")

    def __init__(self, location, executionManager, executionManagers):
        super(LocalFSRegistry, self).__init__(executionManager=executionManager)
        self.log.info("loading task definitions from `%s`", location)
        parsed_url = urlparse(location)
        path = None

        if parsed_url.scheme == "file" or parsed_url.scheme == '':
            path = parsed_url.path
        elif parsed_url.scheme == "pkg-resources":
            if parsed_url.netloc:
                raise NotImplementedError("specifying package  name is not supported!")
            path = parsed_url.path
            path = resource_filename(__name__, path)

        self.location = path

        if path is None:
            self.log.warning("registry path could not be resolved!")
        else:
            self._load_processes_from_directory(path, executionManager, executionManagers)

    def _load_processes_from_directory(self, directory, executionManager, executionManagers):
        glob_result = glob(directory)
        for matched_file in glob_result:
            self.log.debug("loading task definitions from file `%s`", matched_file)
            loaded_definition = yaml.load(open(matched_file, "r"))
            proc_def = ProcessDefinition.from_dict(loaded_definition, definition_source=os.path.abspath(matched_file))
            proc_summ = ProcessSummary(proc_def, executionManager, executionManagers)
            proc_summ.add_options_from_dict(loaded_definition, definition_source=os.path.abspath(matched_file))
            self.add_process(proc_def.identifier, proc_summ)
            self.log.debug("loaded process `%s`", proc_def.identifier)


class LocalPythonRegistry(WPSProcessRegistry):
    log = logger.getChild("LocalPythonRegistry")

    def __init__(self, python_spec, executionManager, executionManagers):
        super(LocalPythonRegistry, self).__init__(executionManager=executionManager)
        self.log.info("loading modules based on specification: %s", python_spec)
        spec_components = python_spec.split(":")
        if len(spec_components) < 1 or len(spec_components) > 2:
            raise NotImplementedError("Provided specification is not supported: %s" % python_spec)
        module_name = spec_components[0]
        function = None

        if len(spec_components) > 1:
            function = spec_components[1]

        self._load_handlers(module_name, function, executionManager, executionManagers)

    def _load_handlers(self, module_name, function, execution_manager, execution_managers):
        impm = importlib.import_module(module_name)
        for element_name in dir(impm):
            if not function or element_name == function:
                element = getattr(impm, element_name)
                if not hasattr(element, '__call__'):  # ignore non callabels
                    continue
                if inspect.isbuiltin(element):  # ignore builtins
                    continue
                if not is_processing(element):  # ignore not decorated functions
                    self.log.debug("Ignoring `%s` from `%s` as it is not a processing", element_name, module_name)
                    continue
                self.log.debug("Loading `%s` from `%s`", element_name, module_name)
                attrs = get_processing_attributes(element)
                proces_definition = ProcessDefinition.from_dict(attrs)
                proces_offering = get_process_offering(element)
                self.add_process(proces_definition.identifier,
                                 ProcessSummary(proces_definition,
                                                execution_manager,
                                                execution_managers,
                                                **proces_offering
                                                ))
                self.log.debug("loaded process `%s`", proces_definition.identifier)


class LocalPSRegistry(WPSProcessRegistry):
    log = logger.getChild("LocalPSRegistry")

    def __init__(self, location, executionManager, executionManagers):
        super(LocalPSRegistry, self).__init__(executionManager=executionManager)
        self.log.info("loading task definitions from `%s`", location)
        parsed_url = urlparse(location)
        path = None

        if parsed_url.scheme == "file" or parsed_url.scheme == '':
            path = parsed_url.path
        elif parsed_url.scheme == "pkg-resources":
            if parsed_url.netloc:
                raise NotImplementedError("specifying package  name is not supported!")
            path = parsed_url.path
            path = resource_filename(__name__, path)

        self.location = path

        if path is None:
            self.log.warning("registry path could not be resolved!")
        else:
            self._load_processes_from_directory(path, executionManager, executionManagers)

    def _load_processes_from_directory(self, directory, executionManager, executionManagers):
        glob_result = glob(directory)

        # register all processes from processDirectory package
        for matched_file in glob_result:
            self.log.debug("loading task definitions from file `%s`", matched_file)
            loaded_definition = yaml.load(open(matched_file, "r"))
            process_directory = loaded_definition['processDirectory']
            process_package = loaded_definition['processPackage']
            for importer, package_name, _ in pkgutil.iter_modules([process_directory]):
                full_package_name = '%s.%s' % (process_package, package_name)
                module = importlib.import_module(full_package_name)

        # load all processes from the list
        for process in EWPS_PROCESS_REGISTRY:
            proc_def = ProcessDefinition.from_funct_attr(process)
            self.add_process(proc_def.identifier,
                             ProcessSummary(proc_def, executionManager, executionManagers, "async-execute sync-execute",
                                            "value"))
            self.log.debug("loaded process `%s`", proc_def.identifier)

        self.log.debug(EWPS_PROCESS_REGISTRY)


class MetaWPSProcessRegistry(object):
    def __init__(self):
        self.registries = []

    def add_registry(self, registry):
        self.registries.append(registry)

    def get_processes(self):
        for registry in self.registries:
            for process in registry.get_processes():
                yield process

    def get_process_by_identifier(self, process_identifier):
        for registry in self.registries:
            process_summary = registry.get_process_by_identifier(process_identifier)
            if process_summary is not None:
                return process_summary


class DataDefinition(object):
    pass


class DataFormat(object):
    def __init__(self, mimetype, encoding, schema, maximum_megabytes=None, default=False):
        self.mimetype = mimetype
        self.encoding = encoding
        self.schema = schema
        self.maximum_megabytes = maximum_megabytes
        self.default = default


class BoundingBoxData(object):
    def __init__(self, lowerCorner, upperCorner, crs=None, dimensions=None):
        self.lower_corner = lowerCorner
        self.upper_corner = upperCorner
        self.crs = crs
        self.dimensions = dimensions

    def get_lower_corner(self):
        return self.lower_corner

    def get_upper_corner(self):
        return self.upper_corner

    def get_crs(self):
        return self.crs

    def get_dimensions(self):
        return self.dimensions


class LiteralData(object):
    def __init__(self, value):
        self.value = value

    def get_value(self):
        return self.value


class ComplexData(object):
    def __init__(self, value=None):
        self.value = value
        self.transmission = 'value'
        self.url = None
        self.mimeType = None
        self.encoding = None
        self.schema = None

    def get_value(self):
        if self.transmission == 'reference':
            if self.url is not None and self.mimeType is not None:
                http = urllib3.PoolManager()
                r = http.request('GET', self.url)
                print (self.url)
                if r is not None and r.data is not None:
                    self.value = r.data
                    # ToDo maybe send mimetype as input
                    # return self.value.read()
        return self.value

    def set_transmission(self, transmission):
        self.transmission = transmission

    def set_parameters_by_ref(self, url, mimeType, encoding=None, schema=None):
        self.url = url
        self.mimeType = mimeType
        self.encoding = encoding
        self.schema = schema

    def get_transmission(self):
        return self.transmission

    def get_parameters_by_ref(self):
        d = {}
        d['url'] = self.url
        d['mimeType'] = self.mimeType
        d['encoding'] = self.encoding
        d['schema'] = self.schema
        return d


class SupportedCRS(object):
    def __init__(self, CRS, default=False):
        self.CRS = CRS
        self.default = default


class ComplexDataDefinition(DataDefinition):
    def __init__(self, default_format=None, formats=None, any=None):
        super(ComplexDataDefinition, self).__init__()
        if formats:
            self._formats = formats
        else:
            self._formats = []
        self._default_format = default_format
        self._any = any

    @classmethod
    def from_dict(cls, definition_dict):
        defaultFormat = definition_dict.get('default', None)
        supportedFormats = definition_dict.get('formats')
        any = definition_dict.get('any', None)
        if supportedFormats is None:
            raise KeyError('Missing supported formats')
        if defaultFormat is not None and defaultFormat not in supportedFormats:
            raise KeyError('Invalid Default Format. Should be one of "%s"' % ", ".join(supportedFormats.keys()))

        complexData = ComplexDataDefinition()

        for format_name, format_definition in supportedFormats.items():
            format = DataFormat(
                mimetype=format_definition.get('mimeType', "text/plain"),
                encoding=format_definition.get('encoding', "utf8"),
                schema=format_definition.get('schema'),
                maximum_megabytes=format_definition.get('maximum_megabytes')
            )
            if format_name == defaultFormat:
                format.default = True
            complexData.add_format(format)
        if any:
            complexData._any = any

        return complexData

    def add_format(self, format):
        self._formats.append(format)

    def to_xml(self, parent_node=None):
        if parent_node is None:
            input_node = etree.Element(self.XML_WPS + "ComplexData", nsmap=NSMAP)
        else:
            input_node = etree.SubElement(parent_node, XML_WPS + "ComplexData")

        for format in self._formats:
            node = etree.SubElement(input_node, XML_WPS + "Format")
            # ToDO mimetype, encoding and schema from Format are mandatory, treat exceptions
            if format.mimetype:
                node.attrib["mimeType"] = format.mimetype
            if format.encoding:
                node.attrib["encoding"] = format.encoding
            if format.schema:
                node.attrib["schema"] = format.schema
            if format.maximum_megabytes is not None:
                node.attrib["maximum_megabytes"] = format.maximum_megabytes
            if format.default:
                node.attrib["default"] = "true"

        if self._any:
            for any_element in self._any:
                node = etree.SubElement(input_node, XML_WPS + "Any")
                node.text = any_element

        return input_node


class BoundingBoxDataDefinition(DataDefinition):
    def __init__(self, default_format=None, formats=None):
        super(BoundingBoxDataDefinition, self).__init__()
        if formats:
            self._formats = formats
        else:
            self._formats = []
        self._default_format = default_format
        self._supportedCRS = []

    def add_format(self, format):
        self._formats.append(format)

    def add_supportedCRS(self, supportedCRS):
        self._supportedCRS.append(supportedCRS)

    @classmethod
    def from_dict(cls, definition_dict):
        defaultFormat = definition_dict.get('default', None)
        supportedFormats = definition_dict.get('formats')
        supportedCRS = definition_dict.get('supportedCRS', None)
        if supportedFormats is None:
            raise KeyError('Missing supported formats')
        if defaultFormat is not None and defaultFormat not in supportedFormats:
            raise KeyError('Invalid Default Format. Should be one of "%s"' % ", ".join(supportedFormats.keys()))
        if supportedCRS is None:
            raise KeyError("Missing supported formats")

        bounding_box_data = BoundingBoxDataDefinition()

        for format_name, format_definition in supportedFormats.items():
            format = DataFormat(
                mimetype=format_definition.get('mimeType', "text/plain"),
                encoding=format_definition.get('encoding', None),
                schema=format_definition.get('schema', None),
                maximum_megabytes=format_definition.get('maximum_megabytes')
            )
            if format_name == defaultFormat:
                format.default = True
            bounding_box_data.add_format(format)

        for value in supportedCRS:
            supportedCRS = SupportedCRS(
                CRS=value.get('CRS', None),
                default=value.get('default', False)
            )
            bounding_box_data.add_supportedCRS(supportedCRS)
            # ToDO test to have one and only one supportedCRS default=True
        return bounding_box_data

    def to_xml(self, parent_node=None):
        if parent_node is None:
            input_node = etree.Element(self.XML_WPS + "BoundingBoxData", nsmap=NSMAP)
        else:
            input_node = etree.SubElement(parent_node, XML_WPS + "BoundingBoxData")

        for format in self._formats:
            node = etree.SubElement(input_node, XML_WPS + "Format")
            if format.mimetype is not None:
                node.attrib["mimeType"] = format.mimetype
            if format.encoding is not None:
                node.attrib["encoding"] = format.encoding
            if format.schema is not None:
                node.attrib["schema"] = format.schema
            if format.maximum_megabytes is not None:
                node.attrib["maximum_megabytes"] = format.maximum_megabytes
            if format.default:
                node.attrib["default"] = "true"

        for supportedCRS in self._supportedCRS:
            node = etree.SubElement(input_node, XML_WPS + "SupportedCRS")
            if supportedCRS.default:
                node.attrib["default"] = "true"
            node.text = supportedCRS.CRS

        return input_node


class LiteralDataDomainDefinition(object):
    def __init__(self, possibleLiteralValues, dataType, UOM=None, defaultValue=None, default=False):
        self._possibleLiteralValues = possibleLiteralValues
        self._values = []
        self._ranges = []
        self._reference = None
        self._dataType = dataType
        self._UOM = UOM
        self._defaultValue = defaultValue
        self._default = default
        self._dataTypeReference = {
            'string': "http://www.w3.org/2001/XMLSchema#string",
            'integer': "http://www.w3.org/2001/XMLSchema#integer",
            'decimal': "http://www.w3.org/2001/XMLSchema#decimal",
            'boolean': "http://www.w3.org/2001/XMLSchema#boolean",
            'double': "http://www.w3.org/2001/XMLSchema#double",
            'float': "http://www.w3.org/2001/XMLSchema#float",
        }

    @classmethod
    def from_dict(cls, definition_dict):
        print (definition_dict)
        possibleLiteralValue = definition_dict.get('possibleLiteralValues')
        dataType = definition_dict.get('dataType')
        UOM = definition_dict.get('UOM', None)
        defaultValue = definition_dict.get('defaultValue', None)
        default = definition_dict.get('default', False)

        dataDomain = LiteralDataDomainDefinition(possibleLiteralValue, dataType, UOM, defaultValue, default)

        if possibleLiteralValue.lower() == "allowedvalues":
            dataDomain._values = definition_dict.get("values", [])
            dataDomain._ranges = definition_dict.get("range", [])

        if possibleLiteralValue.lower() == "valuesreference":
            dataDomain._reference = definition_dict.get("reference", None)

        return dataDomain


class LiteralDataDefinition(DataDefinition):
    def __init__(self):
        self._formats = []
        self._datadomains = []

    @classmethod
    def from_dict(cls, definition_dict):
        defaultFormat = definition_dict.get('default', None)
        supportedFormats = definition_dict.get('formats')
        dataDomains = definition_dict.get('dataDomains', None)
        if supportedFormats is None:
            raise KeyError('Missing supported formats')
        if defaultFormat is not None and defaultFormat not in supportedFormats:
            raise KeyError('Invalid Default Format. Should be one of "%s"' % ", ".join(supportedFormats.keys()))
        literal_data = cls()

        for format_name, format_definition in supportedFormats.items():
            format = DataFormat(
                mimetype=format_definition.get('mimeType', "text/plain"),
                encoding=format_definition.get('encoding', None),
                schema=format_definition.get('schema', None),
                maximum_megabytes=format_definition.get('maximum_megabytes')
            )
            if format_name == defaultFormat:
                format.default = True
            literal_data.add_format(format)
        # ToDo: we should implement the DataDomain definitions according to the standard

        for dd in dataDomains:
            datadomain = LiteralDataDomainDefinition.from_dict(dd)
            literal_data.add_datadomain(datadomain)
        return literal_data

    def to_xml(self, parent_node=None):
        if parent_node is None:
            input_node = etree.Element(self.XML_WPS + "LiteralData", nsmap=NSMAP)
        else:
            input_node = etree.SubElement(parent_node, XML_WPS + "LiteralData")
        for format in self._formats:
            node = etree.SubElement(input_node, XML_WPS + "Format")
            if format.mimetype is not None:
                node.attrib["mimeType"] = format.mimetype
            if format.encoding is not None:
                node.attrib["encoding"] = format.encoding
            if format.schema is not None:
                node.attrib["schema"] = format.schema
            if format.maximum_megabytes is not None:
                node.attrib["maximum_megabytes"] = format.maximum_megabytes
            if format.default:
                node.attrib["default"] = "true"

        for data_domain in self._datadomains:
            literal_data_domain = etree.SubElement(input_node, "LiteralDataDomain")
            if data_domain._default:
                literal_data_domain.set("default", "true")
            if data_domain._possibleLiteralValues.lower() == 'anyvalue':
                any_value = etree.SubElement(literal_data_domain, XML_OWS + "AnyValue")
            if data_domain._possibleLiteralValues.lower() == "allowedvalues":
                allowed_values = etree.SubElement(literal_data_domain, XML_OWS + "AllowedValues")
                for v in data_domain._values:
                    value = etree.SubElement(allowed_values, XML_OWS + "Value")
                    value.text = v
                for r in data_domain._ranges:
                    range = etree.SubElement(allowed_values, XML_OWS + "Range")
                    minRange = etree.SubElement(range, XML_OWS + "MinimumValue")
                    maxRange = etree.SubElement(range, XML_OWS + "MaximumValue")
                    minRange.text = str(r.get('minimumValue', None))
                    maxRange.text = str(r.get('maximumValue', None))
            if data_domain._possibleLiteralValues.lower() == "valuesreference":
                value_ref = etree.SubElement(literal_data_domain, XML_OWS + "ValuesReference")
                value_ref.set("{%s}reference" % OWS_NS, data_domain._reference)
            if data_domain._dataType is not None:
                data_type = etree.SubElement(literal_data_domain, XML_OWS + "DataType")
                data_type.set("{%s}reference" % OWS_NS,
                              data_domain._dataTypeReference.get(data_domain._dataType.lower(), None))
                data_type.text = data_domain._dataType
            if data_domain._UOM is not None:
                uom = etree.SubElement(literal_data_domain, XML_OWS + "UOM")
                uom.text = data_domain._UOM
            if data_domain._defaultValue is not None:
                default_value = etree.SubElement(literal_data_domain, XML_OWS + "DefaultValue")
                default_value.text = str(data_domain._defaultValue)


                # ToDo test for none at the above
                # TODo implement for AllowedValues and ValuesReference
                # allowed_value = etree.SubElement(literal_data_domain, XML_OWS + "AnyValue")

        return input_node

    def add_format(self, format):
        self._formats.append(format)

    def add_datadomain(self, datadomain):
        self._datadomains.append(datadomain)


class CommonIOStructure(object):
    # ToDo: We have support nested inputs as mandated by the standard!
    def __init__(self, identifier,
                 title=None, minOccurs=None, maxOccurs=None, abstract=None, complexData=None, literalData=None,
                 boundingBoxData=None, metadata=None, keywords=None):
        self.identifier = identifier
        self.title = title
        self.minOccurs = minOccurs
        self.maxOccurs = maxOccurs
        self.abstract = abstract
        self.metadata = metadata
        self.keywords = keywords
        self.complexData = complexData
        self.literalData = literalData
        self.boundingBoxData = boundingBoxData
        self._tagname = None

    @classmethod
    def from_dict(cls, definition_dict):
        title = definition_dict.get('title')
        identifier = definition_dict.get('identifier')
        minOccurs = definition_dict.get('minOccurs')
        maxOccurs = definition_dict.get('maxOccurs')
        abstract = definition_dict.get('abstract', None)
        metadata = definition_dict.get('metadata', None)
        keywords = definition_dict.get('keywords', None)

        complexDataDefinition = definition_dict.get('complexData', None)
        literalDataDefinition = definition_dict.get('literalData', None)
        boundingBoxDataDefinition = definition_dict.get('boundingBoxData', None)
        complexData = None
        literalData = None
        boundingBoxData = None

        if complexDataDefinition is not None and literalDataDefinition is not None and boundingBoxDataDefinition is not None:
            raise KeyError("Simultaneous complexData and literalData and boundingBoxData not allowed!")
        if complexDataDefinition:
            complexData = ComplexDataDefinition.from_dict(complexDataDefinition)
        if literalDataDefinition:
            literalData = LiteralDataDefinition.from_dict(literalDataDefinition)
        if boundingBoxDataDefinition:
            boundingBoxData = BoundingBoxDataDefinition.from_dict(boundingBoxDataDefinition)

        input_def = cls(identifier, title=title, minOccurs=minOccurs, maxOccurs=maxOccurs,
                        abstract=abstract, literalData=literalData, complexData=complexData,
                        boundingBoxData=boundingBoxData, metadata=metadata, keywords=keywords)

        return input_def

    @classmethod
    def from_funct_attr(cls, definition_dict):
        return cls.from_dict(definition_dict)

    def to_xml(self, parent_node=None):
        if parent_node is None:
            input_node = etree.Element(XML_WPS + self._tagname, nsmap=NSMAP)
        else:
            input_node = etree.SubElement(parent_node, XML_WPS + self._tagname)

        if self.minOccurs is not None:
            input_node.attrib["minOccurs"] = str(self.minOccurs)
        if self.maxOccurs is not None:
            input_node.attrib["maxOccurs"] = str(self.maxOccurs)

        # Title and Identifier are mandatory attributes
        etree.SubElement(input_node, XML_OWS + "Title").text = self.title
        etree.SubElement(input_node, XML_OWS + "Identifier").text = self.identifier

        if self.abstract:
            etree.SubElement(input_node, XML_OWS + "Abstract").text = self.abstract

        # TODO add more metadata properties http://docs.opengeospatial.org/is/14-065/14-065.html#31 Table 5
        if self.metadata and self.metadata.get('href', None) is not None:
            etree.SubElement(input_node, XML_OWS + "Metadata").set(XML_XLINK + "href", self.metadata['href'])

        # ToDo add Language Tag

        # add keywords zero or more (optional)
        if self.keywords is not None and len(self.keywords) > 0:
            node = etree.SubElement(input_node, XML_OWS + "Keywords")
            for keyword in self.keywords:
                keyword_node = etree.Element(XML_OWS + "Keyword", nsmap=NSMAP)
                keyword_node.text = keyword
                node.append(keyword_node)

        if self.complexData:
            self.complexData.to_xml(input_node)
        if self.literalData:
            self.literalData.to_xml(input_node)
        if self.boundingBoxData:
            self.boundingBoxData.to_xml(input_node)
        return input_node


# TODo handle datatype encoder for complex and literal
class DataTypeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, BoundingBoxData):
            bb_data = {}
            bb_data['lower_corner'] = obj.get_lower_corner()
            bb_data['upper_corner'] = obj.get_upper_corner()
            bb_data['crs'] = obj.get_crs()
            bb_data['dimensions'] = obj.get_dimensions()
            return bb_data
        return json.JSONEncoder.default(self, obj)


def decode_json(dct):
    if '__boundingboxdata__' in str(dct):
        return BoundingBoxData(dct['value']['lower_corner'], dct['value']['upper_corner'],
                               dct['value']['dimensions'], dct['value']['crs'])
    if '__literaldata__' in str(dct):
        return LiteralData(dct['value'])
    return dct


class InputDefinition(CommonIOStructure):
    def __init__(self, *args, **kw):
        super(InputDefinition, self).__init__(*args, **kw)
        self._tagname = "Input"


class OutputDefinition(CommonIOStructure):
    def __init__(self, *args, **kw):
        super(OutputDefinition, self).__init__(*args, **kw)
        self._tagname = "Output"
        if kw.get('minOccurs', None) is not None: raise KeyError('minOccurs not allowed in output')
        if kw.get('maxOccurs', None) is not None: raise KeyError('maxOccurs not allowed in output')


class ProcessDefinition(object):
    def __init__(self, title,
                 identifier, metadata=None,
                 abstract=None, keywords=None, definition_source=None, entrypoint=None, execution_manager_type=None):
        self.identifier = identifier
        self.title = title
        self.abstract = abstract
        self.keywords = keywords
        self._inputs = {}
        self._outputs = {}
        self._metadata = metadata
        self._definition_source = None
        self._entrypoint = entrypoint
        self._status = None
        # adaugare instanta
        self._executionManagerType = execution_manager_type

    def get_execution_manager_type(self):
        return self._executionManagerType

    @classmethod
    def from_dict(cls, definition, definition_source=None):
        process_identifier = definition["name"]
        process_title = definition["title"]

        process_metadata = definition["metadata"]

        process_entrypoint = definition["entrypoint"]
        # ToDo maybe add validation for executionManager type here
        process_execution_manager_type = definition.get("executionManager", None)
        process_abstract = definition.get("abstract", None)
        process_keywords = definition.get("keywords", [])
        input_definitions = definition["inputs"]
        output_definitions = definition["outputs"]

        proc_def = ProcessDefinition(process_title, process_identifier, process_metadata, abstract=process_abstract,
                                     keywords=process_keywords,
                                     definition_source=definition_source, entrypoint=process_entrypoint,
                                     execution_manager_type=process_execution_manager_type)

        for input_identifier, input_definition in input_definitions.items():
            input_definition["identifier"] = input_identifier
            proc_def.add_input(input_identifier, InputDefinition.from_funct_attr(input_definition))

        for output_identifier, output_definition in output_definitions.items():
            output_definition["identifier"] = output_identifier
            proc_def.add_output(output_identifier, OutputDefinition.from_funct_attr(output_definition))

        return proc_def

    def add_input(self, input_name, input_definition):
        if input_name in self._inputs:
            raise KeyError("Input already registered")
        self._inputs[input_name] = input_definition

    def add_output(self, output_name, output_definition):
        if output_name in self._outputs:
            raise KeyError("Output already registered")
        self._outputs[output_name] = output_definition

    def to_xml(self, parent_node=None):
        if parent_node is None:
            process_node = etree.Element(XML_WPS + "Process", nsmap=NSMAP)
        else:
            process_node = etree.SubElement(parent_node, XML_WPS + "Process")

        etree.SubElement(process_node, XML_OWS + "Title").text = self.title
        etree.SubElement(process_node, XML_OWS + "Identifier").text = self.identifier

        if self.abstract:
            etree.SubElement(process_node, XML_OWS + "Abstract").text = self.abstract
        # TODO add more metadata properties http://docs.opengeospatial.org/is/14-065/14-065.html#31 Table 5
        if self._metadata and self._metadata.get('href', None) is not None:
            etree.SubElement(process_node, XML_OWS + "Metadata").set(XML_XLINK + "href", self._metadata['href'])

        # ToDo add Language Tag

        # add keywords zero or more (optional)
        if self.keywords is not None and len(self.keywords) > 0:
            node = etree.SubElement(process_node, XML_OWS + "Keywords")
            for keyword in self.keywords:
                keyword_node = etree.Element(XML_OWS + "Keyword", nsmap=NSMAP)
                keyword_node.text = keyword
                node.append(keyword_node)

        for input_name, input_def in self._inputs.items():
            input_def.to_xml(process_node)
        for output_name, output_def in self._outputs.items():
            output_def.to_xml(process_node)
        return process_node

    def get_output_result_to_xml(self, output_result, job_id=None):

        output_nodes = []
        for output in output_result:
            # create output node
            output_node = etree.Element(XML_WPS + "Output", nsmap=NSMAP)
            output_node.set('id', output)

            if output_result[output]['transmission'] == 'value':
                data_node = etree.Element(XML_WPS + "Data", nsmap=NSMAP)
                # get type of the output value
                if self._outputs[output].literalData is not None:
                    literal_node = etree.Element(XML_WPS + "LiteralValue", nsmap=NSMAP)
                    literal_node.text = str(output_result[output]['value'])
                    data_node.append(literal_node)
                output_node.append(data_node)
            elif output_result[output]['transmission'] == 'reference':
                # prepare link
                base_path = request.headers.get('Host')
                # get path from value if result from process is a file
                p = output_result[output]['value'][output_result[output]['value'].index("job_" + job_id):]
                # ToDo maybe not hardcode /result/ path and job dir
                # full_path="http://"+base_path+"/result/"+"job_"+job_id+".tar.gz"
                full_path = "http://" + base_path + "/result/" + p
                data_node = etree.Element(XML_WPS + "Reference", nsmap=NSMAP)
                data_node.set("{%s}href" % XLINK, full_path)
                output_node.append(data_node)
            # ToDo maybe expection if different output_format
            output_nodes.append(output_node)
        return output_nodes

    def extract_input_values(self, doc):
        input_values = {}

        expr = "//*[local-name() = $name]"
        process_inputs = doc.xpath(expr, name="Input")
        for process_input in process_inputs:
            process_input_id = process_input.get('id')
            expr_data = "//*[@id=$id]/*[local-name() = $name]"
            data_inputs = doc.xpath(expr_data, id=process_input_id, name='Data')
            reference_inputs = doc.xpath(expr_data, id=process_input_id, name='Reference')

            # TODO maybe LiteralData by reference
            if data_inputs:
                literalData = data_inputs[0].xpath("./*[local-name() = $name]", name="LiteralValue")
                # check if Data attribute exists and content is LiteralData
                if self._inputs[process_input_id].literalData is not None and len(literalData) > 0:
                    # get value for LiteralValue
                    literal_data_values = list(data_inputs[0])
                    if len(literal_data_values) > 0:
                        value = literal_data_values[0].text
                        data_type = None
                        for dd in self._inputs[process_input_id].literalData._datadomains:
                            if dd._default:
                                data_type = dd._dataType
                        # ToDo handle if data_type is None
                        # ToDo handle decimal type
                        if data_type.lower() == 'integer':
                            value = int(value)
                        if data_type.lower() == 'double' or data_type.lower() == 'float':
                            value = float(value)
                        if data_type.lower() == 'false':
                            value = False
                        if data_type.lower() == 'true':
                            value = True
                        input_values[process_input_id] = LiteralData(value)
                # check if Data attribute exists and content is BoundingBoxData
                # TODO maybe BoundingBox by reference
                else:
                    boundingBoxData = data_inputs[0].xpath("./*[local-name() = $name]", name="BoundingBox")
                    if self._inputs[process_input_id].boundingBoxData is not None and len(boundingBoxData) > 0:
                        # get attributes
                        bounding_box_crs = boundingBoxData[0].get("crs")
                        bounding_box_dimensions = boundingBoxData[0].get("dimensions")

                        # get corners
                        lower_corner = boundingBoxData[0].xpath("./*[local-name() = $name]", name="LowerCorner")
                        upper_corner = boundingBoxData[0].xpath("./*[local-name() = $name]", name="UpperCorner")
                        # ToDO handle if it does not contain lower and upper corners, mandatory!
                        if len(lower_corner) > 0 and len(upper_corner) > 0:
                            lower_corner_values = tuple([float(i) for i in re.split("\s+", lower_corner[0].text[1:-1])])
                            upper_corner_values = tuple([float(i) for i in re.split("\s+", upper_corner[0].text[1:-1])])
                            # Todo check if current crs is in the initial list
                            input_values[process_input_id] = BoundingBoxData(lower_corner_values, upper_corner_values,
                                                                             bounding_box_crs, bounding_box_dimensions)

                    # check if Data attribute exists and content is ComplexData
                    else:
                        children = list(data_inputs[0].getchildren())
                        if len(children) == 0:
                            value = data_inputs[0].text
                            input_values[process_input_id] = ComplexData(value.strip())
                        else:
                            value = data_inputs[0].xpath("./*")
                            data_content = ""
                            for element in value:
                                data_content = data_content + etree.tostring(element)
                            input_values[process_input_id] = ComplexData(data_content.strip())
                            # ToDO: reference and nested input type
                            # ToDO: link to _inputs

            # check if Reference attribute exists
            elif len(reference_inputs) > 0:
                if self._inputs[process_input_id].complexData is not None:
                    complex_data_href = reference_inputs[0].get("{%s}href" % XLINK)
                    complex_data_mimeType = reference_inputs[0].get("mimeType")
                    if complex_data_href is not None and complex_data_mimeType is not None:
                        c = ComplexData(None)
                        c.set_transmission("reference")
                        complex_data_encoding = reference_inputs[0].get("encoding", None)
                        complex_data_schema = reference_inputs[0].get("schema", None)
                        c.set_parameters_by_ref(complex_data_href, complex_data_mimeType, complex_data_encoding,
                                                complex_data_schema)
                        input_values[process_input_id] = c
                        # ToDo: check if Input attribute exists

        return input_values

    def extract_output_values(self, doc, output_transmission=['value']):

        output_values = {}

        expr = "//*[local-name() = $name]"
        process_outputs = doc.xpath(expr, name="Output")

        # ToDO: handle default Outputs
        for process_output in process_outputs:
            process_output_id = process_output.get('id')
            process_output_transmission = process_output.get('transmission')
            process_output_encoding = process_output.get('encoding', None)
            process_output_schema = process_output.get('schema', None)
            output_values[process_output_id] = {}
            output_values[process_output_id]['transmission'] = process_output_transmission
            if process_output_encoding:
                output_values[process_output_id]['encoding'] = process_output_encoding
            if process_output_schema:
                output_values[process_output_id]['schema'] = process_output_schema
                # ToDO: reference and nested output type
                # ToDO: link to _outputs

        return output_values
